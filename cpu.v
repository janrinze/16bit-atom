/*
    16bit RISC cpu
    (c) 2018 Jan Rinze Peterzon.
*/

`define  BPL   4'h0
`define  BMI   4'h1
`define  BEQ   4'h2
`define  BNE   4'h3
`define  LDR   4'h4
`define  STR   4'h5
`define  JSR   4'h6
`define  LIT   4'h7
`define  ADD   4'h8
`define  SUB   4'h9
`define  SHL   4'ha
`define  SHR   4'hb
`define  AND   4'hc
`define  ORR   4'hd
`define  INV   4'he
`define  XOR   4'hf

`define LOADRD 2'b00
`define WRITE  2'b01
`define DECODE 2'b10
`define PSTALL 2'b11

`define PC 4'hf
/*
`define  opLIT   constant
`define  opADD   register[arg1] + register[arg2]
`define  opSUB   register[arg1] - register[arg2]
`define  opSHL   register[arg1] << arg2
`define  opSHR   register[arg1] >> arg2
`define  opAND   register[arg1] & register[arg2]
`define  opORR   register[arg1] | register[arg2]
`define  opINV   (dest==`PC)?register[14]:~register[arg1]
`define  opXOR   register[arg1] ^ register[arg2]
*/
`define  opLIT   constant
`define  opADD   Ra + Rb
`define  opSUB   Ra - Rb
`define  opSHL   Ra << arg2
`define  opSHR   Ra >> arg2
`define  opAND   Ra & Rb
`define  opORR   Ra | Rb
`define  opINV   (dest==`PC)?register[14]:~Ra
`define  opXOR   Ra ^ Rb

module cpu (
    input clk,                // clock
    input rst,                // reset
    input delay,              // Hold off cpu from bus
    output reg write,         // CPU write request
    output reg [15:0]address, // read/write address
    output reg [15:0]dout,    // write data
    input [15:0]din           // read data
  );

  reg [15:0] result = 16'h0000;
  reg [15:0] register[15:0];
  reg [3:0]  read_dest;
  reg [1:0]  cpu_state = `PSTALL;
  reg [15:0] inst ;//= din;

  // clock instr

  
  reg [15:0] nextpc;
  `define to_pc (dest==`PC)
  
  
  
  wire [3:0] op = inst[15:12];
  wire [3:0] dest = inst[11:8];
  wire [3:0] arg1 = inst[7:4];
  wire [3:0] arg2 = inst[3:0];
  wire [15:0] Ra = register[inst[7:4]];
  wire [15:0] Rb = register[inst[3:0]];
  wire [15:0] constant = { 8'h00,inst[7:0]};
  wire [15:0] branch_target = register[`PC] + {{5{inst[10]}},inst[10:0]};
  
  // prepare for single branch instruction.
  wire branchbita = inst[13];
  wire branchbitb = inst[12];

  reg branch_taken;
  

    
  always@(negedge clk)
  begin
    nextpc <= register[`PC] + 16'h0001;
    branch_taken <= branchbita ?  ((result==16'h0000) ? ~branchbitb : branchbitb) : (result[15]? branchbitb:~branchbitb);
  end
  


  
  reg [15:0] ALU=0;
  always@*
    case (op)
      `LIT: ALU = `opLIT ;
      `ADD: ALU = `opADD ;
      `SUB: ALU = `opSUB ;
      `SHL: ALU = `opSHL ;
      `SHR: ALU = `opSHR ;
      `AND: ALU = `opAND ;
      `ORR: ALU = `opORR ;
      `INV: ALU = `opINV ;
      `XOR: ALU = `opXOR ;
      default: ALU = 16'h0000;
    endcase
  
  reg imm_read;
  always@(posedge clk) begin
    if (rst) begin
      // jump to boot vector.
      cpu_state <= `LOADRD;
      address   <= 16'hffff;
      write     <= 0;
      read_dest <= `PC;
      // sane defaults.
      result <= 16'h0000;
      register[0] <= 16'h8080;
      register[1] <= 16'h0000;
      register[2] <= 16'h8181;
      register[3] <= 16'h0000;
      register[4] <= 16'h0000;
      register[5] <= 16'h0000;
      imm_read<=0;
    end
    else if (!delay) // bus busy
    begin
      
      // pipeline next instruction.
      if (cpu_state[1]) inst <= din;
      
      case(cpu_state)
      `DECODE: begin
                case (op)
                  `BPL,
                  `BMI,
                  `BEQ,
                  `BNE: if (branch_taken)
                          begin
                            read_dest <= `PC;
                            // special case where the indirect address is our next instruction
                            if (inst[11:0]==12'h800)
                             begin
                                register[`PC] <= din;
                                address <= din;
                                cpu_state <= `PSTALL;
                              end
                            else
                              begin
                                register[`PC] <= branch_target;
                                address <= branch_target;
                                if (inst[11]) // indirect target
                                  cpu_state <= `LOADRD;
                                else
                                  cpu_state <= `PSTALL;
                              end
                          end
                        else
                          begin
                            register[`PC] <= nextpc;
                            address <= nextpc;
                            if (inst[11:0]==12'h800) cpu_state <= `PSTALL; // skip next word if intended as branch target
                          end
                  `LDR: begin
                            // ldr pc,pc,pc is JMP IMM16
                            // ldr rd,pc,pc is LDR rd[IMM16]
                            // ldr rd,rs,pc is POP rd,rs
                            
                            register[`PC] <= nextpc;
                            read_dest <= dest;
                            cpu_state <= `LOADRD;
                            if (arg2==`PC)
                              begin
                                if (arg1==`PC)
                                  begin
                                    address<=din;
                                    imm_read<=1;
                                  end
                                else
                                  begin
                                    register[arg1] <= Ra + 1; // stack operation 'POP' ascending.
                                    address <= Ra + 1;
                                  end
                              end
                            else
                              begin                              
                                address <= Ra + Rb;
                              end
                         end
                  `STR: begin
                            // str rd,pc,pc is str rd,[IMM16]
                            // str rd,rs,pc is PUSH rd,rs
                            register[`PC] <= nextpc;
                            cpu_state <= `WRITE;
                            write <=1;
                            if (arg2==`PC)
                              begin
                                if (arg1==`PC)
                                  begin // STR Rd,imm16
                                    address <= din;
                                    imm_read<=1;
                                  end
                                else
                                  begin // PUSH Rd,Ra
                                    address <= Ra;
                                    register[arg1] <= Ra-1;
                                  end
                              end
                            else // STR Rd,[Ra,Rb]
                              address <= Ra + Rb;
                            
                            if (dest==`PC)
                              dout <= nextpc;
                            else
                              dout <= register[dest];
                         end
                  `JSR: begin
                            read_dest <= `PC;
                            // special case where the indirect address is our next instruction
                            if (inst[11:0]==12'h800)
                             begin
                                register[`PC] <= din;
                                address <= din;
                                cpu_state <= `PSTALL;
                                register[14]<=nextpc; // skip one because that contains our target address
                              end
                            else
                              begin
                                register[`PC] <=  branch_target;
                                address <=  branch_target;
                                if (inst[11]) // indirect target
                                  cpu_state <= `LOADRD;
                                else
                                  cpu_state <= `PSTALL;
                                register[14]<=register[`PC];
                              end
                         end
                  default: begin
                            if (`to_pc) begin
                                address <= ALU;
                                register[`PC] <= ALU;
                                cpu_state<=`PSTALL;
                              end
                            else begin
                              register[dest] <= ALU;
                              result <= ALU;
                              register[`PC] <= nextpc;
                              address <= nextpc;
                            end
                         end
                endcase
               end  
      `LOADRD: begin
                  register[read_dest] <= din;
                  imm_read<=0;
                  if (read_dest==`PC) 
                    begin
                      address <= din;
                      cpu_state<=`PSTALL; // stall for pipeline
                    end
                  else
                    begin
                      address <= register[`PC];
                      result <= din;
                      cpu_state <= imm_read ?`PSTALL:`DECODE;
                  end
                end
      `WRITE:  begin
                  address <= register[`PC];
                  write<=0;
                  cpu_state<=imm_read ?`PSTALL:`DECODE;
                  imm_read<=0;
               end
      `PSTALL:  begin
                  // pipeline stall for new PC.
                  address <= nextpc;
                  write<=0;
                  register[`PC]<=nextpc;
                  cpu_state<=`DECODE;
               end
      endcase
    end
  end
endmodule
