# 16bit-atom

Hardware platform based on the Acorn Atom
With a twist: the data bus is 16 bit.
By replacing the 6502 with a 16 bit risc cpu we have 128Kb memory space.
Initial steps were:
  - design a 16 bit risc cpu (still evolving)
  - write an assembler parser in python (for testing the cpu)
  - reuse the Acorn Atom as target platform (easiest way to have I/O)
  - write a compiler that can output assembler


