{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "icoboard",
    "graph": {
      "blocks": [
        {
          "id": "eba9b66b-ca33-4173-8279-fd4b7cc1c52a",
          "type": "basic.output",
          "data": {
            "name": "red",
            "range": "[3:0]",
            "pins": [
              {
                "index": "3",
                "name": "red[0]",
                "value": "A5"
              },
              {
                "index": "2",
                "name": "red[1]",
                "value": "A2"
              },
              {
                "index": "1",
                "name": "red[2]",
                "value": "C3"
              },
              {
                "index": "0",
                "name": "red[3]",
                "value": "B4"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2344,
            "y": 88
          }
        },
        {
          "id": "4493217c-7c14-4ced-a590-5ecef3665c72",
          "type": "basic.output",
          "data": {
            "name": "green",
            "range": "[3:0]",
            "pins": [
              {
                "index": "3",
                "name": "green[0]",
                "value": "D8"
              },
              {
                "index": "2",
                "name": "green[1]",
                "value": "B9"
              },
              {
                "index": "1",
                "name": "green[2]",
                "value": "B10"
              },
              {
                "index": "0",
                "name": "green[3]",
                "value": "B11"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2216,
            "y": 88
          }
        },
        {
          "id": "57bcdc53-3ac5-4caf-9db7-fff9bebf0842",
          "type": "basic.output",
          "data": {
            "name": "blue",
            "range": "[3:0]",
            "pins": [
              {
                "index": "3",
                "name": "blue[0]",
                "value": "B7"
              },
              {
                "index": "2",
                "name": "blue[1]",
                "value": "B6"
              },
              {
                "index": "1",
                "name": "blue[2]",
                "value": "B3"
              },
              {
                "index": "0",
                "name": "blue[3]",
                "value": "B5"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2088,
            "y": 88
          }
        },
        {
          "id": "fa456c9a-39c8-4cf9-a83c-e3a8197549c0",
          "type": "basic.input",
          "data": {
            "name": "clk",
            "pins": [
              {
                "index": "0",
                "name": "pclk",
                "value": "R9"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": 944,
            "y": 120
          }
        },
        {
          "id": "81d60cc2-db8c-414c-8893-26dec2eada02",
          "type": "basic.output",
          "data": {
            "name": "sram_oe",
            "pins": [
              {
                "index": "0",
                "name": "SRAM_nOE",
                "value": "L5"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 1560,
            "y": 136
          }
        },
        {
          "id": "395b3908-59b1-4199-aa90-c5560828d9bc",
          "type": "basic.output",
          "data": {
            "name": "sram_wr",
            "pins": [
              {
                "index": "0",
                "name": "SRAM_nWE",
                "value": "T7"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 1712,
            "y": 152
          }
        },
        {
          "id": "c434e0da-3b59-40d3-9e4b-36808f351f19",
          "type": "basic.output",
          "data": {
            "name": "hsync",
            "pins": [
              {
                "index": "0",
                "name": "hsync",
                "value": "B8"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2216,
            "y": 464
          }
        },
        {
          "id": "fec98832-d0a6-4479-a41d-ec351a356ee1",
          "type": "basic.output",
          "data": {
            "name": "sram_addr",
            "range": "[18:0]",
            "pins": [
              {
                "index": "18",
                "name": "SRAM_A[0]",
                "value": "N2"
              },
              {
                "index": "17",
                "name": "SRAM_A[1]",
                "value": "K5"
              },
              {
                "index": "16",
                "name": "SRAM_A[2]",
                "value": "J5"
              },
              {
                "index": "15",
                "name": "SRAM_A[3]",
                "value": "M5"
              },
              {
                "index": "14",
                "name": "SRAM_A[4]",
                "value": "P4"
              },
              {
                "index": "13",
                "name": "SRAM_A[5]",
                "value": "N5"
              },
              {
                "index": "12",
                "name": "SRAM_A[6]",
                "value": "P5"
              },
              {
                "index": "11",
                "name": "SRAM_A[7]",
                "value": "P7"
              },
              {
                "index": "10",
                "name": "SRAM_A[8]",
                "value": "M6"
              },
              {
                "index": "9",
                "name": "SRAM_A[9]",
                "value": "P6"
              },
              {
                "index": "8",
                "name": "SRAM_A[10]",
                "value": "T8"
              },
              {
                "index": "7",
                "name": "SRAM_A[11]",
                "value": "T1"
              },
              {
                "index": "6",
                "name": "SRAM_A[12]",
                "value": "P2"
              },
              {
                "index": "5",
                "name": "SRAM_A[13]",
                "value": "R1"
              },
              {
                "index": "4",
                "name": "SRAM_A[14]",
                "value": "N3"
              },
              {
                "index": "3",
                "name": "SRAM_A[15]",
                "value": "P1"
              },
              {
                "index": "2",
                "name": "SRAM_A[16]",
                "value": "M11"
              },
              {
                "index": "1",
                "name": "SRAM_A[17]",
                "value": "P10"
              },
              {
                "index": "0",
                "name": "SRAM_A[18]",
                "value": "P8"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2064,
            "y": 552
          }
        },
        {
          "id": "d78f5357-314b-47fb-b9bb-a31dc1bade97",
          "type": "basic.output",
          "data": {
            "name": "vsync",
            "pins": [
              {
                "index": "0",
                "name": "vsync",
                "value": "A9"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 2208,
            "y": 552
          }
        },
        {
          "id": "273c119b-c29a-4613-b77b-b6336d1a6b1c",
          "type": "basic.input",
          "data": {
            "name": "spi_miso",
            "pins": [
              {
                "index": "0",
                "name": "SPI_FLASH_MISO",
                "value": "P11"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": 480,
            "y": 584
          }
        },
        {
          "id": "8588fa15-3d1a-4384-8880-951261a93084",
          "type": "basic.output",
          "data": {
            "name": "spi_cs",
            "pins": [
              {
                "index": "0",
                "name": "SPI_FLASH_CS",
                "value": "R12"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 832,
            "y": 688
          }
        },
        {
          "id": "754bd4dd-8e9b-4483-b83d-d4b8a26951a9",
          "type": "basic.input",
          "data": {
            "name": "SRAMD",
            "range": "[15:0]",
            "pins": [
              {
                "index": "15",
                "name": "SRAM_D[0]",
                "value": "T2"
              },
              {
                "index": "14",
                "name": "SRAM_D[1]",
                "value": "R3"
              },
              {
                "index": "13",
                "name": "SRAM_D[2]",
                "value": "T3"
              },
              {
                "index": "12",
                "name": "SRAM_D[3]",
                "value": "R4"
              },
              {
                "index": "11",
                "name": "SRAM_D[4]",
                "value": "R5"
              },
              {
                "index": "10",
                "name": "SRAM_D[5]",
                "value": "T5"
              },
              {
                "index": "9",
                "name": "SRAM_D[6]",
                "value": "R6"
              },
              {
                "index": "8",
                "name": "SRAM_D[7]",
                "value": "T6"
              },
              {
                "index": "7",
                "name": "SRAM_D[8]",
                "value": "N4"
              },
              {
                "index": "6",
                "name": "SRAM_D[9]",
                "value": "M4"
              },
              {
                "index": "5",
                "name": "SRAM_D[10]",
                "value": "L6"
              },
              {
                "index": "4",
                "name": "SRAM_D[11]",
                "value": "M3"
              },
              {
                "index": "3",
                "name": "SRAM_D[12]",
                "value": "L4"
              },
              {
                "index": "2",
                "name": "SRAM_D[13]",
                "value": "L3"
              },
              {
                "index": "1",
                "name": "SRAM_D[14]",
                "value": "K4"
              },
              {
                "index": "0",
                "name": "SRAM_D[15]",
                "value": "K3"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": 592,
            "y": 712
          }
        },
        {
          "id": "5865b18c-a836-49b2-a043-13bd8368c4d4",
          "type": "basic.output",
          "data": {
            "name": "spi_sclk",
            "pins": [
              {
                "index": "0",
                "name": "SPI_FLASH_SCLK",
                "value": "R11"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 824,
            "y": 760
          }
        },
        {
          "id": "6974689b-3210-4d35-9f94-0c09c58c2e1c",
          "type": "basic.output",
          "data": {
            "name": "spi_mosi",
            "pins": [
              {
                "index": "0",
                "name": "SPI_FLASH_MOSI",
                "value": "P12"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 832,
            "y": 824
          }
        },
        {
          "id": "17d844a1-0fc8-4508-a30b-f0eb486d7253",
          "type": "basic.input",
          "data": {
            "name": "rkey",
            "pins": [
              {
                "index": "0",
                "name": "C09",
                "value": "E10"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": 872,
            "y": 1048
          }
        },
        {
          "id": "729aea52-6b86-4faf-9b67-3b5b7a76c15a",
          "type": "basic.output",
          "data": {
            "name": "sram_ub",
            "pins": [
              {
                "index": "0",
                "name": "SRAM_nUB",
                "value": "J3"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 1872,
            "y": 1096
          }
        },
        {
          "id": "73b411ef-118a-41d3-bc4c-8a234f014173",
          "type": "basic.input",
          "data": {
            "name": "rkey",
            "pins": [
              {
                "index": "0",
                "name": "C13",
                "value": "F9"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": 872,
            "y": 1160
          }
        },
        {
          "id": "4585f67d-0c31-4374-8e41-44e58aaa0cf5",
          "type": "basic.output",
          "data": {
            "name": "sram_lb",
            "pins": [
              {
                "index": "0",
                "name": "SRAM_nLB",
                "value": "J4"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 1840,
            "y": 1176
          }
        },
        {
          "id": "0f6b431b-b7d5-4fa2-8d7a-2eea8ad9e7a9",
          "type": "basic.output",
          "data": {
            "name": "sram_cs",
            "pins": [
              {
                "index": "0",
                "name": "SRAM_nCE",
                "value": "M7"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 1792,
            "y": 1264
          }
        },
        {
          "id": "635aa00a-17c4-491e-b1b9-4281163b2663",
          "type": "b85fb4668bf6e12ebf9734a4553a81f5f00816ed",
          "position": {
            "x": 1680,
            "y": 328
          },
          "size": {
            "width": 96,
            "height": 256
          }
        },
        {
          "id": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
          "type": "92f57de3dcf7dfd6a52dd678f003892f171dd1af",
          "position": {
            "x": 1096,
            "y": 520
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
          "type": "c1cc110a79f581fa615029abb6271e0a96de1e56",
          "position": {
            "x": 1104,
            "y": 232
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "f8be3aab-229d-410e-bc77-492d0189ee2e",
          "type": "7fd4895b34279e884b3ea58c4868f1fadb88efbf",
          "position": {
            "x": 1880,
            "y": 224
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
          "type": "dfbc5cfbbe7b48e499bf3a3cbaa1d8c277972cd7",
          "position": {
            "x": 1312,
            "y": 232
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
          "type": "e70bbd0254872fd86a6ce2e7fe68c9e55eb0e1c1",
          "position": {
            "x": 776,
            "y": 472
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
          "type": "8dc147848d751861ad658de6a0e3960b89b9719a",
          "position": {
            "x": 1472,
            "y": 752
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
          "type": "b47ef1a4e00a8193e22fa1dd2f49d389c9b6acf3",
          "position": {
            "x": 1112,
            "y": 720
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "6d79108f-6813-419c-a463-6cbcf0d82970",
          "type": "f7dd2ac89ad4bef413a6e6b89faac40bbebd61ec",
          "position": {
            "x": 1808,
            "y": 728
          },
          "size": {
            "width": 96,
            "height": 160
          }
        },
        {
          "id": "15b643da-1e3a-44c5-a585-4dd1fa03ed23",
          "type": "b42fe4886af45d655453bb36e28f4c5838b905dd",
          "position": {
            "x": 1896,
            "y": 544
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "7bac3ca1-0e24-4bbe-a9d4-5bff894ce33f",
          "type": "c4dd08263a85a91ba53e2ae2b38de344c5efcb52",
          "position": {
            "x": 1600,
            "y": 1152
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "1a8bcabf-d72c-418f-8d47-8cfde60b0a83",
          "type": "e04e1531ece65bcd7b91c6ea1e3dbcd590811245",
          "position": {
            "x": 1408,
            "y": 416
          },
          "size": {
            "width": 96,
            "height": 128
          }
        },
        {
          "id": "5d7b00c6-722b-4b3c-9bce-d1c59563ed27",
          "type": "cfd9babc26edba88e2152493023c4bef7c47f247",
          "position": {
            "x": 1208,
            "y": 1032
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "2ee9aa4f-77b7-48e6-aa3b-d7af68f12d63",
          "type": "8026abbda6bfa79c6f4c6debd4e97ae7b51ee8d3",
          "position": {
            "x": 1024,
            "y": 1048
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "88a42157-98e2-493e-8e2b-02bf464afcfd",
          "type": "96f0988f8164f7c1b216c8ee122d6ce3cf6bc139",
          "position": {
            "x": 1352,
            "y": 1024
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "c4c87503-1764-4328-90e6-0de46fb972fb",
          "type": "cfd9babc26edba88e2152493023c4bef7c47f247",
          "position": {
            "x": 1216,
            "y": 1144
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "488b1a60-f241-4d7d-8f31-de160c5ec0f5",
          "type": "8026abbda6bfa79c6f4c6debd4e97ae7b51ee8d3",
          "position": {
            "x": 1024,
            "y": 1160
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "d1fbe7b3-2acb-4146-a4ea-54d50b141674",
          "type": "96f0988f8164f7c1b216c8ee122d6ce3cf6bc139",
          "position": {
            "x": 1360,
            "y": 1136
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "51a310db-c61f-486d-aed2-14790ff9843a",
          "type": "7ebc902cbb1c4db116741533a86182485900ecda",
          "position": {
            "x": 1512,
            "y": 1064
          },
          "size": {
            "width": 96,
            "height": 64
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "4a9307be-3533-489d-b933-688a7b43f329"
          },
          "target": {
            "block": "c434e0da-3b59-40d3-9e4b-36808f351f19",
            "port": "in"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "3a55afe9-7931-4bd6-90f4-d75b2137a6bf"
          },
          "target": {
            "block": "d78f5357-314b-47fb-b9bb-a31dc1bade97",
            "port": "in"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "fa456c9a-39c8-4cf9-a83c-e3a8197549c0",
            "port": "out"
          },
          "target": {
            "block": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
            "port": "f734ac12-d7b6-4d18-9c78-adbd27498648"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "f8be3aab-229d-410e-bc77-492d0189ee2e",
            "port": "90b1046b-23df-47e1-a7a8-ab3d22c6cf6e"
          },
          "target": {
            "block": "4493217c-7c14-4ced-a590-5ecef3665c72",
            "port": "in"
          },
          "vertices": [],
          "size": 4
        },
        {
          "source": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "68b9832c-9503-40b8-8318-5b325eaed782"
          },
          "target": {
            "block": "f8be3aab-229d-410e-bc77-492d0189ee2e",
            "port": "db2be3e8-0887-4ae2-ac0a-816452d4a989"
          },
          "vertices": [],
          "size": 6
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "5f4865a7-9a94-46c5-9397-e89a1dc59d14"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "0df97bf9-ae9e-4751-bc88-aab707309556"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "e3c470eb-7a70-4f65-95e5-2bacd85d04a3"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "b8f132c5-4ead-4715-b9b5-b9b81d30aed8"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "4f9f537e-abc0-413f-9093-12c57ac39a8b"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "dc5fb33c-1ad9-4006-9a5d-d59146a0d449"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "e3c470eb-7a70-4f65-95e5-2bacd85d04a3"
          },
          "target": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "cee734d9-a354-4316-8a71-f7bf9156e4c0"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
            "port": "e7f83624-5d24-4c4a-af7f-48c65ed3f295"
          },
          "target": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "b973cba3-f432-41c4-a390-c4e34a90380d"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
            "port": "c0b47dda-91ac-4276-9501-9b8df345009d"
          },
          "target": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "8b64dff7-cfa8-43f6-a518-14ef9be98cc0"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "428fbb9c-247a-4963-a406-c5c59b66879f"
          },
          "target": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "d1244797-3c77-4c65-9150-2afd651c6a7f"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "267ebed8-9d5e-4251-b772-954a2aa7dbbb"
          },
          "target": {
            "block": "8588fa15-3d1a-4384-8880-951261a93084",
            "port": "in"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "b921e9d4-e843-44b5-8ded-aecdd1420c1d"
          },
          "target": {
            "block": "5865b18c-a836-49b2-a043-13bd8368c4d4",
            "port": "in"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "8e6aca50-d184-4e78-8ac9-26bfdcb89114"
          },
          "target": {
            "block": "6974689b-3210-4d35-9f94-0c09c58c2e1c",
            "port": "in"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
            "port": "c0b47dda-91ac-4276-9501-9b8df345009d"
          },
          "target": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "ee686b12-725d-407a-90c1-ff3b4f45b4bf"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "273c119b-c29a-4613-b77b-b6336d1a6b1c",
            "port": "out"
          },
          "target": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "0c81e9d1-795f-4ba3-8d5a-f198d9861711"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "19c97c8a-5cf8-4572-a2c9-eab140c6940d"
          },
          "target": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "db776527-97b2-41ff-b2b8-5bbf9995c0df"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "e661e10c-9de9-405d-bf14-54d7e6ea3ec9"
          },
          "target": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "dd2c60bf-436d-4666-82d3-f92420b1ac60"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "71681ce6-3708-4689-b268-3c11db8cf05b"
          },
          "target": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "9a275c1b-55c4-4d4a-a74b-1490d8caeb28"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "a24bf319-9d41-44ee-ad60-9a1cd90f9290"
          },
          "target": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "f6d61dc3-924c-4d46-b9bd-1559c3931224"
          },
          "vertices": [],
          "size": 24
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "ff6579bd-a2da-4ee7-83a4-be5a5cc9b3e2"
          },
          "target": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "84946bb2-4e77-4cc2-b380-0efabfb91feb"
          },
          "vertices": [],
          "size": 17
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "05bb2d1d-4775-45bd-9dab-9b66fbb3ec82"
          },
          "target": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "19d99d7f-c246-4255-acb9-8d6fdcd942ec"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "05bb2d1d-4775-45bd-9dab-9b66fbb3ec82"
          },
          "target": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "65cc1da3-fbf4-4b2d-883c-70c6d2ce991d"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "2eab01ec-bd16-422f-baaa-f0a86bd58d9f"
          },
          "target": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "aab334cc-fd1b-465e-8067-0d125a07e77c"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "2668129f-cd4f-44ea-bdd7-14f005281a66"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "3191f25b-8ae7-4dff-8822-0e656ca7ffbd",
            "port": "c0b47dda-91ac-4276-9501-9b8df345009d"
          },
          "target": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "f22d717f-017a-4562-97ec-7a0bbf61f87d"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "6cb24b51-0007-413d-9c63-5e9010b32000"
          },
          "target": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "700fe0e1-e94a-44f7-b76b-973299dcc2d5"
          },
          "vertices": [],
          "size": 32
        },
        {
          "source": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "e2fe99e9-92cb-43ea-b9c2-6a04a17e6ae1"
          },
          "target": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "446f8401-0bb5-42b2-95e1-a73c15fe25b4"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "05d0ea77-6648-48c2-9eed-c858bb980db1"
          },
          "target": {
            "block": "ffd4f87b-006d-4899-a1e5-328cc60cb99d",
            "port": "2665fabd-8bb4-417c-8160-85aac0689807"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "71628cdc-a383-435f-a4ee-42d1cdf5739f"
          },
          "target": {
            "block": "fec98832-d0a6-4479-a41d-ec351a356ee1",
            "port": "in"
          },
          "vertices": [],
          "size": 19
        },
        {
          "source": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "0eddc281-cf93-4b65-a14a-ba8ad56541d5"
          },
          "target": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "1c9faf8b-efcc-458a-96a7-9afecc3d531a"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "b705a660-684d-4a88-88b7-2672ba772512"
          },
          "target": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "f460f4f5-8fa9-49b9-8e68-3f8831f701ae"
          },
          "vertices": [],
          "size": 19
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "91f05048-ed41-46d1-b3be-15804686c90b"
          },
          "target": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "cfe7b36a-1475-45a6-b728-a0ba2a26e6b7"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "4f9f537e-abc0-413f-9093-12c57ac39a8b"
          },
          "target": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "5801c2c6-9af6-42fc-8b3a-8e8ae81e09cd"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "754bd4dd-8e9b-4483-b83d-d4b8a26951a9",
            "port": "out"
          },
          "target": {
            "block": "6d79108f-6813-419c-a463-6cbcf0d82970",
            "port": "2c316daa-b031-4239-a23d-ffa14f005cb6"
          },
          "vertices": [],
          "size": 16
        },
        {
          "source": {
            "block": "15b643da-1e3a-44c5-a585-4dd1fa03ed23",
            "port": "8dc18642-c2fc-497b-828a-ac24d0a902c0"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "dac676b0-f72a-452e-9ba8-a6ac57269458"
          },
          "vertices": [],
          "size": 8
        },
        {
          "source": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "162c9912-27d8-4221-bbb9-359c1ebcc605"
          },
          "target": {
            "block": "15b643da-1e3a-44c5-a585-4dd1fa03ed23",
            "port": "4bc9adae-f51c-4add-9e17-e0ced1ebc37e"
          },
          "vertices": [],
          "size": 10
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "91f05048-ed41-46d1-b3be-15804686c90b"
          },
          "target": {
            "block": "81d60cc2-db8c-414c-8893-26dec2eada02",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "7bac3ca1-0e24-4bbe-a9d4-5bff894ce33f",
            "port": "19c8f68d-5022-487f-9ab0-f0a3cd58bead"
          },
          "target": {
            "block": "0f6b431b-b7d5-4fa2-8d7a-2eea8ad9e7a9",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "7bac3ca1-0e24-4bbe-a9d4-5bff894ce33f",
            "port": "19c8f68d-5022-487f-9ab0-f0a3cd58bead"
          },
          "target": {
            "block": "4585f67d-0c31-4374-8e41-44e58aaa0cf5",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "7bac3ca1-0e24-4bbe-a9d4-5bff894ce33f",
            "port": "19c8f68d-5022-487f-9ab0-f0a3cd58bead"
          },
          "target": {
            "block": "729aea52-6b86-4faf-9b67-3b5b7a76c15a",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "4f9f537e-abc0-413f-9093-12c57ac39a8b"
          },
          "target": {
            "block": "395b3908-59b1-4199-aa90-c5560828d9bc",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "f8be3aab-229d-410e-bc77-492d0189ee2e",
            "port": "a649eb46-0097-47ad-a4b1-8d586ac37441"
          },
          "target": {
            "block": "eba9b66b-ca33-4173-8279-fd4b7cc1c52a",
            "port": "in"
          },
          "size": 4
        },
        {
          "source": {
            "block": "f8be3aab-229d-410e-bc77-492d0189ee2e",
            "port": "5b1eae4b-7d27-4937-95ca-04edb5100e6a"
          },
          "target": {
            "block": "57bcdc53-3ac5-4caf-9db7-fff9bebf0842",
            "port": "in"
          },
          "size": 4
        },
        {
          "source": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "0eddc281-cf93-4b65-a14a-ba8ad56541d5"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "5eb58447-969f-4c0d-84ff-8cd3d9398650"
          },
          "size": 16
        },
        {
          "source": {
            "block": "8500908b-636a-4f2f-bb28-6ef58ee61e90",
            "port": "b705a660-684d-4a88-88b7-2672ba772512"
          },
          "target": {
            "block": "1a8bcabf-d72c-418f-8d47-8cfde60b0a83",
            "port": "a97b5b0f-cf20-4b68-9225-f957fec63f38"
          },
          "size": 19
        },
        {
          "source": {
            "block": "1a8bcabf-d72c-418f-8d47-8cfde60b0a83",
            "port": "494fac32-c5b7-4a98-80c3-1b115c47558a"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "f2c4b715-9c8c-4a3c-9aa0-4e095c1d63f8"
          }
        },
        {
          "source": {
            "block": "1a8bcabf-d72c-418f-8d47-8cfde60b0a83",
            "port": "78c719c3-46c3-4534-9a9f-52dcdadbc033"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "426df4c6-7fd0-4193-b7e4-143fcd4f94d0"
          }
        },
        {
          "source": {
            "block": "1a8bcabf-d72c-418f-8d47-8cfde60b0a83",
            "port": "9d18b5ff-b31a-4aa8-97f9-d21293038d66"
          },
          "target": {
            "block": "635aa00a-17c4-491e-b1b9-4281163b2663",
            "port": "9d0eabfc-0543-46d9-9103-86c3546ee73c"
          },
          "size": 13
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "5d7b00c6-722b-4b3c-9bce-d1c59563ed27",
            "port": "4bf41c17-a2da-4140-95f7-2a80d51b1e1a"
          }
        },
        {
          "source": {
            "block": "17d844a1-0fc8-4508-a30b-f0eb486d7253",
            "port": "out"
          },
          "target": {
            "block": "2ee9aa4f-77b7-48e6-aa3b-d7af68f12d63",
            "port": "bb4a1ca9-1b30-471e-92ca-ca7ff2fc1150"
          }
        },
        {
          "source": {
            "block": "2ee9aa4f-77b7-48e6-aa3b-d7af68f12d63",
            "port": "a139fa0d-9b45-4480-a251-f4a66b49aa23"
          },
          "target": {
            "block": "5d7b00c6-722b-4b3c-9bce-d1c59563ed27",
            "port": "c9e1af2a-6f09-4cf6-a5b3-fdf7ec2c6530"
          }
        },
        {
          "source": {
            "block": "5d7b00c6-722b-4b3c-9bce-d1c59563ed27",
            "port": "22ff3fa1-943b-4d1a-bd89-36e1c054d077"
          },
          "target": {
            "block": "88a42157-98e2-493e-8e2b-02bf464afcfd",
            "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
          }
        },
        {
          "source": {
            "block": "488b1a60-f241-4d7d-8f31-de160c5ec0f5",
            "port": "a139fa0d-9b45-4480-a251-f4a66b49aa23"
          },
          "target": {
            "block": "c4c87503-1764-4328-90e6-0de46fb972fb",
            "port": "c9e1af2a-6f09-4cf6-a5b3-fdf7ec2c6530"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "73b411ef-118a-41d3-bc4c-8a234f014173",
            "port": "out"
          },
          "target": {
            "block": "488b1a60-f241-4d7d-8f31-de160c5ec0f5",
            "port": "bb4a1ca9-1b30-471e-92ca-ca7ff2fc1150"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "c4c87503-1764-4328-90e6-0de46fb972fb",
            "port": "22ff3fa1-943b-4d1a-bd89-36e1c054d077"
          },
          "target": {
            "block": "d1fbe7b3-2acb-4146-a4ea-54d50b141674",
            "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
          },
          "vertices": []
        },
        {
          "source": {
            "block": "88a42157-98e2-493e-8e2b-02bf464afcfd",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "51a310db-c61f-486d-aed2-14790ff9843a",
            "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
          }
        },
        {
          "source": {
            "block": "d1fbe7b3-2acb-4146-a4ea-54d50b141674",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "51a310db-c61f-486d-aed2-14790ff9843a",
            "port": "97b51945-d716-4b6c-9db9-970d08541249"
          }
        },
        {
          "source": {
            "block": "51a310db-c61f-486d-aed2-14790ff9843a",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "bae336e4-b1b4-4cdf-82b5-3a898144e203",
            "port": "c361180d-4621-4620-a200-af168eb92a27"
          }
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "c4c87503-1764-4328-90e6-0de46fb972fb",
            "port": "4bf41c17-a2da-4140-95f7-2a80d51b1e1a"
          }
        },
        {
          "source": {
            "block": "88a42157-98e2-493e-8e2b-02bf464afcfd",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "213f7d87-3d22-401d-b022-6c673ef2d8cd"
          }
        },
        {
          "source": {
            "block": "1b18c401-7ae9-40b5-9b9e-bb121bbed5fd",
            "port": "ed174b25-c643-46c1-8c39-be754bb62003"
          },
          "target": {
            "block": "7a708786-2f0a-41ff-89a8-7acb28a2db77",
            "port": "78f75945-cea9-4b5a-b092-0c1c8c0ed3a3"
          }
        }
      ]
    }
  },
  "dependencies": {
    "b85fb4668bf6e12ebf9734a4553a81f5f00816ed": {
      "package": {
        "name": "VGAgen",
        "version": "0.1",
        "description": "VGA Graphics generator",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "5f4865a7-9a94-46c5-9397-e89a1dc59d14",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": 80,
                "y": -64
              }
            },
            {
              "id": "0917dc61-579d-43ef-9a90-d08987040a61",
              "type": "basic.output",
              "data": {
                "name": "settings",
                "range": "[5:0]",
                "size": 6
              },
              "position": {
                "x": 1040,
                "y": -40
              }
            },
            {
              "id": "b8f132c5-4ead-4715-b9b5-b9b81d30aed8",
              "type": "basic.input",
              "data": {
                "name": "reset",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 16
              }
            },
            {
              "id": "68b9832c-9503-40b8-8318-5b325eaed782",
              "type": "basic.output",
              "data": {
                "name": "rgb",
                "range": "[5:0]",
                "size": 6
              },
              "position": {
                "x": 1040,
                "y": 88
              }
            },
            {
              "id": "f2c4b715-9c8c-4a3c-9aa0-4e095c1d63f8",
              "type": "basic.input",
              "data": {
                "name": "cs",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 96
              }
            },
            {
              "id": "426df4c6-7fd0-4193-b7e4-143fcd4f94d0",
              "type": "basic.input",
              "data": {
                "name": "ms",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 176
              }
            },
            {
              "id": "4a9307be-3533-489d-b933-688a7b43f329",
              "type": "basic.output",
              "data": {
                "name": "hsync"
              },
              "position": {
                "x": 1040,
                "y": 216
              }
            },
            {
              "id": "dc5fb33c-1ad9-4006-9a5d-d59146a0d449",
              "type": "basic.input",
              "data": {
                "name": "we",
                "clock": false
              },
              "position": {
                "x": 80,
                "y": 256
              }
            },
            {
              "id": "9d0eabfc-0543-46d9-9103-86c3546ee73c",
              "type": "basic.input",
              "data": {
                "name": "cpu_address",
                "range": "[12:0]",
                "clock": false,
                "size": 13
              },
              "position": {
                "x": 80,
                "y": 336
              }
            },
            {
              "id": "3a55afe9-7931-4bd6-90f4-d75b2137a6bf",
              "type": "basic.output",
              "data": {
                "name": "vsync"
              },
              "position": {
                "x": 1040,
                "y": 344
              }
            },
            {
              "id": "5eb58447-969f-4c0d-84ff-8cd3d9398650",
              "type": "basic.input",
              "data": {
                "name": "Din",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 80,
                "y": 416
              }
            },
            {
              "id": "162c9912-27d8-4221-bbb9-359c1ebcc605",
              "type": "basic.output",
              "data": {
                "name": "charaddr",
                "range": "[9:0]",
                "size": 10
              },
              "position": {
                "x": 1040,
                "y": 472
              }
            },
            {
              "id": "dac676b0-f72a-452e-9ba8-a6ac57269458",
              "type": "basic.input",
              "data": {
                "name": "textchar",
                "range": "[7:0]",
                "clock": false,
                "size": 8
              },
              "position": {
                "x": 80,
                "y": 496
              }
            },
            {
              "id": "226f690c-1f4b-4257-9664-351f6a5539c4",
              "type": "basic.code",
              "data": {
                "code": "\t// ------------------------------------------------------------------------------------\n\t// VGA registers:\n\t// #BC00 - #BFFF\n\t//\n\t//   T.B.D.\n\t// 4 bit RGB output.\n\t// \n\t// ------------------------------------------------------------------------------------\n\n\t// ATOM can do up to 4 colors so we map them to writeable RGB 2:2:2 registers.\n\treg [5:0] color0=0;\n\treg [5:0] color1=0;\n\treg [5:0] color2=0;\n\treg [5:0] color3=0;\n  reg [5:0] settings=0;\n  reg [15:0] vid_data;\n  \n  \n always@(negedge we or posedge reset) begin\n  if (reset) begin\n    color0 <= 6'b000011; \n    color1 <= 6'b001001;\n    color2 <= 6'b110000;\n    color3 <= 6'b111111;\n    settings <= 6'b000000; // startup in text mode.\n    end \n  else\n    begin  // latch writes to color regs\n      if (cs) begin\n        case (cpu_address[2:0])\n          3'b100: color0 <= Din[5:0];\n          3'b101: color1 <= Din[5:0];\n          3'b110: color2 <= Din[5:0];\n          3'b111: color3 <= Din[5:0];\n          default: settings <= Din[5:0];//;{2'b00,Din[3:0]};//\n        endcase\n       end\n    end\n  end\n\n\nreg [9:0] hor_counter=0;\nreg [9:0] vert_counter=0;\nreg [15:0] curpixeldat=0;\nreg [3:0] char_line=0;\nreg [4:0] hor_pos=0;\nreg [7:0] vert_pos=0;\nreg [3:0] tvert_pos=0;\n\nwire [7:0] data = vid_data[7:0];\nwire [7:0] altdata = vid_data[15:8];\n\nwire hor_valid    = ~hor_counter[9];\nwire vert_valid   = (vert_counter[9:8]==3) ? 0 : 1;\n// 60 Hz \nwire hor_restart  = hor_counter == 511+12+68+80;\nwire hs_start     = hor_counter == 511+12;\nwire hs_stop\t    = hor_counter == 511+12+68;\n\nwire vert_restart = vert_counter == 767+3+6+29;\nwire vs_start     = vert_counter == 767+3;\nwire vs_stop\t    = vert_counter == 767+3+6;\nwire [12:0] address;\n/* 75 Hz\nwire hor_restart  = hor_counter == 511+8+48+88;\nwire hs_start     = hor_counter == 511+8;\nwire hs_stop      = hor_counter == 511+8+48;\n\nwire vert_restart = vert_counter == 767+1+3+28;\nwire vs_start     = vert_counter == 767+1;\nwire vs_stop      = vert_counter == 767+1+3;\n*/\nwire textmode\t  = settings[3]==1'b0;\nreg invert=0;\t      \nwire c_restart    = char_line==4'b1011;\nwire next_byte    = hor_counter[3:0] == 4'b0000;\nwire next_line    = vert_counter[1:0] == 2'b11;\nwire next_data    = (hor_counter[3:0]==4'b1111);\nreg h_sync=0,v_sync=0,bg=0,invs=0;\nreg [5:0] pixel=0;\n\nwire [7:0] textchar  ;// = charmap[{data[5:0],char_line }];\nreg [15:0] Dtextchar;\nreg [15:0] Dgraph=0;\nwire [15:0] Ddata = vid_data;\nwire highres;\ninteger myiter=0;\nalways@* begin\nfor (myiter=0;myiter<8;myiter=myiter+1) begin\n    Dtextchar[myiter*2]=textchar[myiter];\n    Dtextchar[myiter*2+1]=textchar[myiter];\n    end\nend\n//duplic txt [7:0] (textchar,Dtextchar);\n//duplic dta [7:0] (data[7:0],Ddata);\n//duplic dta [7:0] (data,Ddata);\nassign highres = (settings[3:0]==4'hf)|(settings[3:0]==4'h0);\n//always@* begin\nassign    charaddr={data[5:0],char_line};\n\n// special mode using blocks\nwire [1:0] p1,p2,p3,p4,p5,p6;\n\nassign p1 = {data[5]&~data[7],data[5]};\nassign p2 = {data[4]&~data[7],data[4]};\nassign p3 = {data[3]&~data[7],data[3]};\nassign p4 = {data[2]&~data[7],data[2]};\nassign p5 = {data[1]&~data[7],data[1]};\nassign p6 = {data[0]&~data[7],data[0]};\n\n\nalways @* begin\n  case (char_line[3:2])\n  0 : Dgraph = {{4{p1}},{4{p2}}};\n  1 : Dgraph = {{4{p3}},{4{p4}}};\n  default : Dgraph = {{4{p5}},{4{p6}}};\n  endcase\nend\n  \nreg req;// = (hor_counter[3:0]==4'b1100)||hor_restart;\nalways @(negedge clk) begin\n  req <= (hor_counter[3:0]==4'b1100)||hor_restart;\nend\nreg [15:0] vidmem [0:8191];\n\nalways@(posedge clk) begin\n\tif (req) vid_data <= vidmem[address];\n\tif (ms) vidmem[cpu_address]<= Din;\nend\n\nalways@(posedge clk) begin\n\tif (reset) begin\n\t\thor_counter <= 0;\n\t\tvert_counter <= 0;\n\t\th_sync <= 1;\n\t\tv_sync <= 1;\n\t\tpixel <= 0;\n\t\tcurpixeldat <=0;\n\t\tchar_line <=0;\n\t\thor_pos<=0;\n\t\tvert_pos<=0;\n\t\ttvert_pos<=0;\n    invert <=0;\n    bg<=0;\n\tend else begin\n    \n\t\tif (hor_restart) begin\n\t\t    hor_counter <= 0;\n        // multi pix here\n\t\t    if (vert_restart) begin\n\t\t        vert_counter <= 0;\n            // multiline here\n          end\n\t\t    else \n          begin\n            vert_counter <= vert_counter + 1;\n            // multiline here\n          end\n\t\tend else begin\n\t\t    hor_counter <= hor_counter + 1;\n        // multi pix here\n\t\tend\t\n\t\t\n\t\t\n\t\tif (hs_start)\n\t\t\thor_pos <=0;\n\t\telse if (next_byte & hor_valid)\n\t\t\thor_pos <= hor_pos+1;\n\t\t\n\t\tif (vs_start) begin\n\t\t\tvert_pos <= 0;\n\t\t\tchar_line<=0;\n\t\t\ttvert_pos <= 0;\n\t\tend \n\t\telse if ( next_line & vert_valid & hs_start) begin\n\t\t\tvert_pos <= vert_pos +1;\t\n\t\t    if (c_restart) begin\n\t\t\t\tchar_line <=0;\n\t\t\t\ttvert_pos<=tvert_pos+1;\n\t\t\tend\telse\n\t\t\t\tchar_line <= char_line +1;\n\t\tend\n\n\t    if (next_data)\n          case ({data[7:6],settings[3:0]})\n            6'b00_0000 : curpixeldat <= Dtextchar; // text mode\n            6'b10_0000 : curpixeldat <= ~Dtextchar; // text mode\n            6'b01_0000 : curpixeldat <= Dgraph; // text mode blocks\n            6'b11_0000 : curpixeldat <= Dgraph; // text mode blocks\n\t        default:  curpixeldat <= highres ?Ddata:{data[7:0],8'h00};\n\t      endcase\n\t    else\n        if (highres) begin\n          if (hor_counter[0]==1'b1) \n            curpixeldat <= {curpixeldat[13:0],2'b00}; //shift_left\t\n        end\n        else\n          if (hor_counter[1:0]==2'b11)\n            curpixeldat <= {curpixeldat[13:0],2'b00}; //shift_left\n\n    case(curpixeldat[15:14])\n      0:  pixel <= color0;\n      1:  pixel <= color1;\n      2:  pixel <= color2;\n      3:  pixel <= color3;\n    endcase\n\n\t\tbg <= hor_valid & vert_valid; \n\t\t\n\t\t// generate sync pulses  \n\t\tif (hs_start)\n\t\t   h_sync <=0;\n\t\telse if (hs_stop)\n\t\t   h_sync <=1;\n\t\tif (vs_start)\n\t\t   v_sync <=0;\n\t\telse if (vs_stop)\n\t\t   v_sync <=1;\n\t\t\t\n\tend\t\nend\n\n// hor_pos 5 \n// tvert_pos 4 \n// address 13\n\nassign address = (textmode) ? {4'b0000,tvert_pos,hor_pos}:{ vert_pos,hor_pos};\nassign hsync = h_sync;\nassign vsync = v_sync;\n\nassign rgb  =  bg ? pixel : 6'b000000;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "reset"
                    },
                    {
                      "name": "cs"
                    },
                    {
                      "name": "ms"
                    },
                    {
                      "name": "we"
                    },
                    {
                      "name": "cpu_address",
                      "range": "[12:0]",
                      "size": 13
                    },
                    {
                      "name": "Din",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "textchar",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ],
                  "out": [
                    {
                      "name": "settings",
                      "range": "[5:0]",
                      "size": 6
                    },
                    {
                      "name": "rgb",
                      "range": "[5:0]",
                      "size": 6
                    },
                    {
                      "name": "hsync"
                    },
                    {
                      "name": "vsync"
                    },
                    {
                      "name": "charaddr",
                      "range": "[9:0]",
                      "size": 10
                    }
                  ]
                }
              },
              "position": {
                "x": 288,
                "y": -72
              },
              "size": {
                "width": 624,
                "height": 640
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "5f4865a7-9a94-46c5-9397-e89a1dc59d14",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "b8f132c5-4ead-4715-b9b5-b9b81d30aed8",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "reset"
              }
            },
            {
              "source": {
                "block": "f2c4b715-9c8c-4a3c-9aa0-4e095c1d63f8",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "cs"
              }
            },
            {
              "source": {
                "block": "426df4c6-7fd0-4193-b7e4-143fcd4f94d0",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "ms"
              }
            },
            {
              "source": {
                "block": "dc5fb33c-1ad9-4006-9a5d-d59146a0d449",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "we"
              }
            },
            {
              "source": {
                "block": "9d0eabfc-0543-46d9-9103-86c3546ee73c",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "cpu_address"
              },
              "size": 13
            },
            {
              "source": {
                "block": "5eb58447-969f-4c0d-84ff-8cd3d9398650",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "Din"
              },
              "size": 16
            },
            {
              "source": {
                "block": "dac676b0-f72a-452e-9ba8-a6ac57269458",
                "port": "out"
              },
              "target": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "textchar"
              },
              "size": 8
            },
            {
              "source": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "settings"
              },
              "target": {
                "block": "0917dc61-579d-43ef-9a90-d08987040a61",
                "port": "in"
              },
              "size": 6
            },
            {
              "source": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "rgb"
              },
              "target": {
                "block": "68b9832c-9503-40b8-8318-5b325eaed782",
                "port": "in"
              },
              "size": 6
            },
            {
              "source": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "hsync"
              },
              "target": {
                "block": "4a9307be-3533-489d-b933-688a7b43f329",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "vsync"
              },
              "target": {
                "block": "3a55afe9-7931-4bd6-90f4-d75b2137a6bf",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "226f690c-1f4b-4257-9664-351f6a5539c4",
                "port": "charaddr"
              },
              "target": {
                "block": "162c9912-27d8-4221-bbb9-359c1ebcc605",
                "port": "in"
              },
              "size": 10
            }
          ]
        }
      }
    },
    "92f57de3dcf7dfd6a52dd678f003892f171dd1af": {
      "package": {
        "name": "CPU16",
        "version": "0.1",
        "description": "16 bit RISC processor",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "0df97bf9-ae9e-4751-bc88-aab707309556",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": 176,
                "y": 160
              }
            },
            {
              "id": "428fbb9c-247a-4963-a406-c5c59b66879f",
              "type": "basic.output",
              "data": {
                "name": "write"
              },
              "position": {
                "x": 832,
                "y": 160
              }
            },
            {
              "id": "cee734d9-a354-4316-8a71-f7bf9156e4c0",
              "type": "basic.input",
              "data": {
                "name": "reset",
                "clock": false
              },
              "position": {
                "x": 176,
                "y": 264
              }
            },
            {
              "id": "19c97c8a-5cf8-4572-a2c9-eab140c6940d",
              "type": "basic.output",
              "data": {
                "name": "address",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 832,
                "y": 264
              }
            },
            {
              "id": "2665fabd-8bb4-417c-8160-85aac0689807",
              "type": "basic.input",
              "data": {
                "name": "din",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 176,
                "y": 368
              }
            },
            {
              "id": "e661e10c-9de9-405d-bf14-54d7e6ea3ec9",
              "type": "basic.output",
              "data": {
                "name": "dout",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 832,
                "y": 368
              }
            },
            {
              "id": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
              "type": "basic.code",
              "data": {
                "code": "/*\n    16bit RISC cpu\n    (c) 2018 Jan Rinze Peterzon.\n*/\n\n`define  BRA   4'h0\n`define  JSR   4'h1\n`define  LDR   4'h2\n`define  STR   4'h3\n`define  LDI   4'h4\n`define  STI   4'h5\n`define  ADI   4'h6\n`define  LIT   4'h7\n\n`define  ADD   4'h8\n`define  SUB   4'h9\n`define  AND   4'ha\n`define  ORR   4'hb\n`define  EOR   4'hc\n`define  SHL   4'hd\n`define  SHR   4'he\n`define  MUL   4'hf\n\n//`define LOADRD 4'b0001\n//`define WRITE  4'b0010\n//`define DECODE 4'b0100\n//`define PSTALL 4'b1000\n\n`define PC 4'hf\n\n`define  opLIT   constant\n`define  opADI   (arg2==4'h0)?{15'h0000,(Ra==0)}:Ra + sim7\n\n\n`define  opADD   Ra + Rb\n`define  opSUB   Ra - Rb\n`define  opSHL   Ra << Rb[3:0]\n`define  opSHR   Ra >> Rb[3:0]\n`define  opAND   Ra & Rb\n`define  opORR   Ra | Rb\n`define  opXOR   Ra ^ Rb\n`define  opMUL   Ra * Rb\n\n`define  DopADD   RiA + RiB\n`define  DopSUB   RiA - RiB\n`define  DopSHL   RiA << RiB[3:0]\n`define  DopSHR   RiA >> RiB[3:0]\n`define  DopAND   RiA & RiB\n`define  DopORR   RiA | RiB\n`define  DopXOR   RiA ^ RiB\n`define  DopMUL   RiA * RiB\n\n\n/*\nmodule cpu (\n    input clk,                // clock\n    input rst,                // reset\n    input delay,              // Hold off cpu from bus\n    output reg write,         // CPU write request\n    output reg [15:0]address, // read/write address\n    output reg [15:0]dout,    // write data\n    input [15:0]din           // read data\n  );\n*/\n  reg [15:0] result = 16'h0000;\n  reg [15:0] register[0:15];\n  reg [3:0]  read_dest=0;\n  reg [3:0]  cpu_state;\n  reg [15:0] inst =0;\n  reg [15:0] address,dout;\n  reg write;\n\n  // clock instr\n\n  \n  reg [15:0] nextpc=0;\n  //reg [15:0] dnextpc=0;\n  `define to_pc (dest==`PC)\n  \n \n  \n  wire [3:0] op = inst[15:12];\n  wire [3:0] dest = inst[11:8];\n  wire [3:0] arg1 = inst[7:4];\n  wire [3:0] arg2 = inst[3:0];\n  \n  \n  \n  wire imm_alu = inst[15] & inst[0]& inst[1]& inst[2]& inst[3];\n  \n  reg [15:0] RiB=0;\n  reg [15:0] RiA=0;\n  reg [3:0] d_op=0;\n  \n  reg delayed_alu;\n  \n  wire [15:0] Ra = register[inst[7:4]];\n  wire [15:0] Rb = register[inst[3:0]];\n  \n  //wire [15:0] RB = delayed_alu ? RiB:Rb;\n  wire [15:0] constant = { 8'h00,inst[7:0]};\n  //wire [15:0] branch_target = register[`PC] + {{7{inst[8]}},inst[8:0]};\n  //wire [15:0] jump_target = register[`PC] + {{5{inst[10]}},inst[10:0]};\n  wire [15:0] sim7 =  {{12{inst[3]}},inst[3:0]};\n  \n  // prepare for single branch instruction.\n  wire branchbita = inst[10];\n  wire branchbitb = inst[9];\n\n  reg branch_taken=0;\n  reg branch_imm=0;\n\n  reg [15:0] branch_target;\n   \n  always@(negedge clk)\n  begin\n    nextpc <= register[`PC] + 16'h0001;\n    branch_target <= register[`PC] + {{7{inst[8]}},inst[8:0]};\n    //dnextpc <= register[`PC] + 16'h0002;\n    branch_taken <= branchbita ?  ((result==16'h0000) ? ~branchbitb : branchbitb) : (result[15]? branchbitb:~branchbitb);\n    branch_imm <= inst[11] ? (inst[8:0]==9'h00) : 0;\n  end\n\n  //always @*\n  //  branch_taken = branchbita ?  ((result==16'h0000) ? ~branchbitb : branchbitb) : (result[15]? branchbitb:~branchbitb);\n\n  wire [15:0] braddr = branch_taken ? (branch_imm ? din : branch_target) : nextpc;\n  \n`define S_LOADRD 4'b0001\n`define S_WRITE 4'b0010\n`define S_DECODE 4'b0100\n`define S_PSTALL 4'b1000\n  \n  wire [3:0]  br_cpu_state = (branch_taken) ? ((inst[11]&&(inst[8:0]!=9'h00))?`S_LOADRD:`S_PSTALL):(inst[11]?`S_PSTALL:`S_DECODE);\n\n  //wire [15:0] ldraddr = (arg2==`PC) ? ((arg1==`PC)? din : Ra + 1):Ra + Rb;\n  \n  reg [15:0] ALU=0;\n  always@* begin\n    case (op)\n      `LIT: ALU = `opLIT ;\n      `ADD: ALU = `opADD ;\n      `SUB: ALU = `opSUB ;\n      `SHL: ALU = `opSHL ;\n      `SHR: ALU = `opSHR ;\n      `AND: ALU = `opAND ;\n      `ORR: ALU = `opORR ;\n      `EOR: ALU = `opXOR ;\n      `ADI: ALU = `opADI ;\n      //`MUL: ALU = `opMUL ;\n      default: ALU = 16'h0000;\n    endcase\n  end\n\nreg [15:0] delayedALU=0;\n  always@* begin\n    case (d_op)\n      `ADD: delayedALU = `DopADD ;\n      `SUB: delayedALU = `DopSUB ;\n      `SHL: delayedALU = `DopSHL ;\n      `SHR: delayedALU = `DopSHR ;\n      `AND: delayedALU = `DopAND ;\n      `ORR: delayedALU = `DopORR ;\n      `EOR: delayedALU = `DopXOR ;\n      //`MUL: delayedALU = `DopMUL ;\n      default: delayedALU = 16'h0000;\n    endcase\n  end\n\n  \n  //reg [15:0] co_pro_div;\n  //reg [15:0] co_pro_rem;\n  reg [15:0] co_pro_A;\n  reg [15:0] co_pro_B;\n\n  wire [15:0] co_pro_mul = co_pro_A * co_pro_B;\n  \n  \n  reg imm_read;\n  parameter LOADRD=0,WRITE=1,DECODE=2,PSTALL=3;\n\n\n  always@(posedge clk) begin\n    if (rst) begin\n      // jump to boot vector.\n      cpu_state <= 4'b0001; // start with load address\n      address   <= 16'hffff;\n      write     <= 0;\n      read_dest <= `PC;\n      // sane defaults.\n      result <= 16'h0000;\n      imm_read<=0;\n      delayed_alu<=0;\n      RiA <=16'h0000;\n      RiB <=16'h0000;\n      dout <=16'h0000;\n      d_op <=4'h0;\n      inst <=16'h0000;\n    end\n    else //if (!delay) // bus busy\n    begin\n      \n      // pipeline next instruction.\n      if (cpu_state[PSTALL] || (cpu_state[DECODE]& ~imm_alu))\n            inst <= din;\n      cpu_state<=4'b0000;\n      case(1'b1)// synthesis parallel_case full_case\n      cpu_state[DECODE]: begin\n                case (op)\n                  `BRA: //if (branch_taken)\n                          begin\n                            read_dest <= `PC;\n                            address   <= braddr;\n                            register[`PC] <=braddr;\n                            cpu_state <= br_cpu_state;\n                            /*\n                            // special case where the indirect branch address is our next instruction\n                            if (branch_imm)\n                             begin\n                                register[`PC] <= din;\n                                address <= din;\n                                cpu_state <= `PSTALL;\n                              end\n                            else\n                              begin\n                                register[`PC] <= branch_target;\n                                address <= branch_target;\n                                if (inst[11]) // indirect target\n                                  cpu_state <= `LOADRD;\n                                else\n                                  cpu_state <= `PSTALL;\n                              end\n                          end\n                        else\n                          begin\n                            register[`PC] <= nextpc;\n                            address <= nextpc;\n                            if (inst[11]) cpu_state <= `PSTALL; // skip next word if intended as branch target\n                          end*/\n                        end\n                  `LDR: begin\n                            // ldr pc,pc,pc is JMP [IMM16]\n                            // ldr rd,pc,pc is LDR rd[IMM16]\n                            // ldr rd,rs,pc is POP rd,rs\n                            \n                            register[`PC] <= nextpc;\n                            read_dest <= dest;\n                            cpu_state <= `S_LOADRD;//cpu_state[LOADRD] <= 1;\n                            /*\n                            address <= ldraddr;\n                            if (arg2==`PC)\n                              begin\n                                if (arg1==`PC)\n                                    imm_read<=1;\n                                else\n                                    register[arg1] <= ldraddr; // stack operation 'POP' ascending.\n                              end\n                            */\n                            if (arg2==`PC)\n                              begin\n                                if (arg1==`PC)\n                                  begin\n                                    address<=din;\n                                    imm_read<=1;\n                                  end\n                                else\n                                  begin\n                                    register[arg1] <= Ra + 1; // stack operation 'POP' ascending.\n                                    address <= Ra + 1;\n                                  end\n                              end\n                            else\n                              begin                              \n                                address <= Ra + Rb;\n                              end\n                         end\n                  `STR: begin\n                            // str rd,pc,pc is str rd,[IMM16]\n                            // str rd,rs,pc is PUSH rd,rs\n                            register[`PC] <= nextpc;\n                            cpu_state <= `S_WRITE;//cpu_state[WRITE] <= 1;\n                            write <=1;\n                            if (arg2==`PC)\n                              begin\n                                if (arg1==`PC)\n                                  begin // STR Rd,imm16\n                                    address <= din;\n                                    imm_read<=1;\n                                  end\n                                else\n                                  begin // PUSH Rd,Ra\n                                    address <= Ra;\n                                    register[arg1] <= Ra-1;\n                                  end\n                              end\n                            else // STR Rd,[Ra,Rb]\n                              address <= Ra + Rb;\n                            \n                            if (dest==`PC)\n                              dout <= nextpc;\n                            else\n                              dout <= register[dest];\n                         end\n                  `LDI: begin\n                            // ldr pc,pc,pc is JMP IMM16\n                            // ldr rd,pc,pc is LDR rd[IMM16]\n                            // ldr rd,rs,pc is POP rd,rs\n                            \n                            register[`PC] <= nextpc;\n                            read_dest <= dest;\n                            cpu_state <= `S_LOADRD;\n                            if (arg1==`PC)\n                              begin\n                                if (arg2==`PC)\n                                  begin\n                                    register[dest]<=din ;\n                                    result <= din;\n                                    cpu_state <= `S_PSTALL;\n                                    if (dest==`PC)\n                                      address <= din ;\n                                    else\n                                      address <= nextpc;\n                                  end\n                                else\n                                  begin\n                                    imm_read<=1;\n                                    address <= din + Rb;\n                                  end\n                              end\n                            else\n                              begin                              \n                                address <= Ra + sim7;\n                              end\n                         end\n                  `STI: begin\n                            // str rd,pc,pc is str rd,[IMM16]\n                            // str rd,rs,pc is PUSH rd,rs\n                            register[`PC] <= nextpc;\n                            cpu_state <= `S_WRITE;\n                            write <=1;\n                            if (arg1==`PC)\n                              begin\n                                imm_read<=1;\n                                if (arg2==`PC)\n                                  begin // STR Rd,imm16\n                                    address <= din;\n                                  end\n                                else\n                                  begin // STR Rd,[imm16,Rb]\n                                    address <= din + Rb;\n                                  end\n                              end\n                            else // STR Rd,[Ra,Rb]\n                              address <= Ra + sim7;\n                            \n                            if (dest==`PC)\n                              dout <= nextpc;\n                            else\n                              dout <= register[dest];\n                         end\n                         \n                  `JSR: begin\n                            read_dest <= `PC;\n                            // special case where the indirect address is our next instruction\n                            if (inst[11:0]==12'h800)\n                             begin\n                                register[`PC] <= din;\n                                address <= din;\n                                cpu_state <= `S_PSTALL;\n                                register[14]<=nextpc; // skip one because that contains our target address\n                                //if (inst[11]) // indirect target\n                                //  cpu_state <= `LOADRD;\n                                //else\n                                //  cpu_state <= `PSTALL;\n                              end\n                            else\n                              begin\n                                register[14]<=register[`PC];\n                                register[`PC] <=  branch_target;\n                                address <=  branch_target;\n                                if (inst[11]) // indirect target\n                                  cpu_state <= `S_LOADRD;\n                                else\n                                  cpu_state <= `S_PSTALL;\n                              end\n                         end\n                  `MUL:  begin\n                           register[`PC] <= nextpc;\n                           address <= nextpc;\n                           if(dest==`PC) begin\n                              co_pro_A <= register[arg1];\n                              co_pro_B <= (imm_alu) ? din :register[arg2];\n                              cpu_state <= (imm_alu) ? `S_PSTALL:`S_DECODE;\n                           end else begin\n                              register[dest]<=co_pro_mul;\n                              cpu_state <= `S_DECODE;\n                           end\n                         end\n                  default: begin\n                              if (imm_alu)\n                                begin\n                                  register[`PC] <= nextpc;\n                                  address <= nextpc;\n                                  delayed_alu<=1;\n                                  RiB <= din;\n                                  RiA <= Ra;\n                                  d_op <= op;\n                                  read_dest<=dest;\n                                  cpu_state<=`S_PSTALL;\n                                end\n                              else\n                               begin\n                                 if (`to_pc)\n                                   begin\n                                    address <= ALU;\n                                    register[`PC] <= ALU;\n                                    cpu_state<=`S_PSTALL;\n                                   end\n                                 else\n                                   begin\n\t\t\t\t\t\t\t\t\t cpu_state<=`S_DECODE;\n                                     register[dest] <= ALU;\n                                     result <= ALU;\n                                     register[`PC] <= nextpc;\n                                     address <= nextpc;\n                                   end\n                               end  \n                           end\n                endcase\n               end  \n      cpu_state[LOADRD]: begin\n                  imm_read<=0;\n                  if (read_dest==`PC) \n                    begin\n                      register[`PC]<= din;\n                      address <= din;\n                      cpu_state<=`S_PSTALL; // stall for pipeline\n                    end\n                  else\n                    begin\n                      address <= register[`PC];\n                      result <= din;\n                      register[read_dest] <= din;\n                      cpu_state <= imm_read ?`S_PSTALL:`S_DECODE;\n                    end\n                end\n      cpu_state[WRITE]:  begin\n                  address <= register[`PC];\n                  write<=0;\n                  cpu_state<=imm_read ?`S_PSTALL:`S_DECODE;\n                  imm_read<=0;\n               end\n      cpu_state[PSTALL]:  begin\n                  // pipeline stall for new PC.\n                  delayed_alu<=0;\n                  write<=0;\n                  if (delayed_alu)\n                    begin\n                      if (read_dest==`PC)\n                        begin\n                          cpu_state<=`S_PSTALL;\n                          address <= delayedALU;\n                          register[`PC]<=delayedALU;\n                        end\n                      else\n                        begin\n                         register[read_dest] <= delayedALU;\n                         result <= delayedALU;\n                         register[`PC]<=nextpc;\n                         address <= nextpc;\n                         cpu_state<=`S_DECODE;\n                        end\n                    end\n                  else\n                    begin\n                      cpu_state<=`S_DECODE;\n                      register[`PC]<=nextpc;\n                      address <= nextpc;\n                    end\n               end\n      endcase\n    end\n  end\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "rst"
                    },
                    {
                      "name": "din",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ],
                  "out": [
                    {
                      "name": "write"
                    },
                    {
                      "name": "address",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "dout",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 352,
                "y": 136
              },
              "size": {
                "width": 408,
                "height": 320
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "2665fabd-8bb4-417c-8160-85aac0689807",
                "port": "out"
              },
              "target": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "din"
              },
              "size": 16
            },
            {
              "source": {
                "block": "cee734d9-a354-4316-8a71-f7bf9156e4c0",
                "port": "out"
              },
              "target": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "rst"
              }
            },
            {
              "source": {
                "block": "0df97bf9-ae9e-4751-bc88-aab707309556",
                "port": "out"
              },
              "target": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "address"
              },
              "target": {
                "block": "19c97c8a-5cf8-4572-a2c9-eab140c6940d",
                "port": "in"
              },
              "size": 16
            },
            {
              "source": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "write"
              },
              "target": {
                "block": "428fbb9c-247a-4963-a406-c5c59b66879f",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "ca7d7a8f-392b-43dc-9a79-77240ed5fc9d",
                "port": "dout"
              },
              "target": {
                "block": "e661e10c-9de9-405d-bf14-54d7e6ea3ec9",
                "port": "in"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "c1cc110a79f581fa615029abb6271e0a96de1e56": {
      "package": {
        "name": "pll120",
        "version": "0.1",
        "description": "120 MHz PLL",
        "author": "generated with icetime.",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "e7f83624-5d24-4c4a-af7f-48c65ed3f295",
              "type": "basic.output",
              "data": {
                "name": "clk_120"
              },
              "position": {
                "x": 760,
                "y": 184
              }
            },
            {
              "id": "f734ac12-d7b6-4d18-9c78-adbd27498648",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": 96,
                "y": 264
              }
            },
            {
              "id": "c0b47dda-91ac-4276-9501-9b8df345009d",
              "type": "basic.output",
              "data": {
                "name": "locked"
              },
              "position": {
                "x": 760,
                "y": 336
              }
            },
            {
              "id": "a9b7c223-73d9-4745-865d-276fc830163c",
              "type": "basic.code",
              "data": {
                "code": "\nSB_PLL40_CORE #(\n\t\t.FEEDBACK_PATH(\"SIMPLE\"),\n\t\t.DIVR(4'b0100),\t\t// DIVR =  4\n\t\t.DIVF(7'b0101111),\t// DIVF = 47\n\t\t.DIVQ(3'b011),\t\t// DIVQ =  3\n\t\t.FILTER_RANGE(3'b010)\t// FILTER_RANGE = 2\n\t) uut (\n\t\t.LOCK(locked),\n\t\t.RESETB(1'b1),\n\t\t.BYPASS(1'b0),\n\t\t.REFERENCECLK(clock_in),\n\t\t.PLLOUTCORE(clock_out)\n\t\t);",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clock_in"
                    }
                  ],
                  "out": [
                    {
                      "name": "clock_out"
                    },
                    {
                      "name": "locked"
                    }
                  ]
                }
              },
              "position": {
                "x": 264,
                "y": 136
              },
              "size": {
                "width": 424,
                "height": 312
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "f734ac12-d7b6-4d18-9c78-adbd27498648",
                "port": "out"
              },
              "target": {
                "block": "a9b7c223-73d9-4745-865d-276fc830163c",
                "port": "clock_in"
              }
            },
            {
              "source": {
                "block": "a9b7c223-73d9-4745-865d-276fc830163c",
                "port": "clock_out"
              },
              "target": {
                "block": "e7f83624-5d24-4c4a-af7f-48c65ed3f295",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "a9b7c223-73d9-4745-865d-276fc830163c",
                "port": "locked"
              },
              "target": {
                "block": "c0b47dda-91ac-4276-9501-9b8df345009d",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "7fd4895b34279e884b3ea58c4868f1fadb88efbf": {
      "package": {
        "name": "RGB222to444",
        "version": "0.1",
        "description": "RGB 2:2:2 to component R[4] G[4] B[4] for PMOD VGA",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "5b1eae4b-7d27-4937-95ca-04edb5100e6a",
              "type": "basic.output",
              "data": {
                "name": "red",
                "range": "[3:0]",
                "size": 4
              },
              "position": {
                "x": 856,
                "y": 128
              }
            },
            {
              "id": "db2be3e8-0887-4ae2-ac0a-816452d4a989",
              "type": "basic.input",
              "data": {
                "name": "rgb",
                "range": "[5:0]",
                "clock": false,
                "size": 6
              },
              "position": {
                "x": 120,
                "y": 208
              }
            },
            {
              "id": "90b1046b-23df-47e1-a7a8-ab3d22c6cf6e",
              "type": "basic.output",
              "data": {
                "name": "green",
                "range": "[3:0]",
                "size": 4
              },
              "position": {
                "x": 856,
                "y": 208
              }
            },
            {
              "id": "a649eb46-0097-47ad-a4b1-8d586ac37441",
              "type": "basic.output",
              "data": {
                "name": "blue",
                "range": "[3:0]",
                "size": 4
              },
              "position": {
                "x": 856,
                "y": 288
              }
            },
            {
              "id": "3ee86b20-5e81-49ff-93e8-df3301d23bc8",
              "type": "basic.code",
              "data": {
                "code": "assign red = {rgb[1:0],rgb[1:0]};\nassign green = {rgb[3:2],rgb[3:2]};\nassign blue = {rgb[5:4],rgb[5:4]};\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "rgb",
                      "range": "[5:0]",
                      "size": 6
                    }
                  ],
                  "out": [
                    {
                      "name": "red",
                      "range": "[3:0]",
                      "size": 4
                    },
                    {
                      "name": "green",
                      "range": "[3:0]",
                      "size": 4
                    },
                    {
                      "name": "blue",
                      "range": "[3:0]",
                      "size": 4
                    }
                  ]
                }
              },
              "position": {
                "x": 320,
                "y": 120
              },
              "size": {
                "width": 456,
                "height": 240
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "db2be3e8-0887-4ae2-ac0a-816452d4a989",
                "port": "out"
              },
              "target": {
                "block": "3ee86b20-5e81-49ff-93e8-df3301d23bc8",
                "port": "rgb"
              },
              "size": 6
            },
            {
              "source": {
                "block": "3ee86b20-5e81-49ff-93e8-df3301d23bc8",
                "port": "red"
              },
              "target": {
                "block": "5b1eae4b-7d27-4937-95ca-04edb5100e6a",
                "port": "in"
              },
              "size": 4
            },
            {
              "source": {
                "block": "3ee86b20-5e81-49ff-93e8-df3301d23bc8",
                "port": "green"
              },
              "target": {
                "block": "90b1046b-23df-47e1-a7a8-ab3d22c6cf6e",
                "port": "in"
              },
              "size": 4
            },
            {
              "source": {
                "block": "3ee86b20-5e81-49ff-93e8-df3301d23bc8",
                "port": "blue"
              },
              "target": {
                "block": "a649eb46-0097-47ad-a4b1-8d586ac37441",
                "port": "in"
              },
              "size": 4
            }
          ]
        }
      }
    },
    "dfbc5cfbbe7b48e499bf3a3cbaa1d8c277972cd7": {
      "package": {
        "name": "clkgen",
        "version": "0.1",
        "description": "Clock generator",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "ed174b25-c643-46c1-8c39-be754bb62003",
              "type": "basic.output",
              "data": {
                "name": "clk"
              },
              "position": {
                "x": 920,
                "y": 88
              }
            },
            {
              "id": "b973cba3-f432-41c4-a390-c4e34a90380d",
              "type": "basic.input",
              "data": {
                "name": "fclk",
                "clock": true
              },
              "position": {
                "x": 32,
                "y": 88
              }
            },
            {
              "id": "4f9f537e-abc0-413f-9093-12c57ac39a8b",
              "type": "basic.output",
              "data": {
                "name": "wg"
              },
              "position": {
                "x": 920,
                "y": 184
              }
            },
            {
              "id": "8b64dff7-cfa8-43f6-a518-14ef9be98cc0",
              "type": "basic.input",
              "data": {
                "name": "locked",
                "clock": false
              },
              "position": {
                "x": 32,
                "y": 184
              }
            },
            {
              "id": "d1244797-3c77-4c65-9150-2afd651c6a7f",
              "type": "basic.input",
              "data": {
                "name": "cpu_write",
                "clock": false
              },
              "position": {
                "x": 32,
                "y": 288
              }
            },
            {
              "id": "ea0e6a9a-8ec4-4043-83f2-43261b13c98d",
              "type": "basic.output",
              "data": {
                "name": "spiclk"
              },
              "position": {
                "x": 920,
                "y": 288
              }
            },
            {
              "id": "65cc1da3-fbf4-4b2d-883c-70c6d2ce991d",
              "type": "basic.input",
              "data": {
                "name": "boot",
                "clock": false
              },
              "position": {
                "x": 32,
                "y": 384
              }
            },
            {
              "id": "91f05048-ed41-46d1-b3be-15804686c90b",
              "type": "basic.output",
              "data": {
                "name": "phi2_we"
              },
              "position": {
                "x": 920,
                "y": 384
              }
            },
            {
              "id": "e3c470eb-7a70-4f65-95e5-2bacd85d04a3",
              "type": "basic.output",
              "data": {
                "name": "cpu_reset"
              },
              "position": {
                "x": 920,
                "y": 480
              }
            },
            {
              "id": "213f7d87-3d22-401d-b022-6c673ef2d8cd",
              "type": "basic.input",
              "data": {
                "name": "reset_key",
                "clock": false
              },
              "position": {
                "x": 32,
                "y": 480
              }
            },
            {
              "id": "b2579481-434a-46d3-8aa6-b80c6117cccd",
              "type": "basic.code",
              "data": {
                "code": "reg wg,clk,spiclk;\nreg [1:0] cnt=0;\nwire [1:0] nxtcnt;\nassign nxtcnt = cnt +1;\n\nassign phi2_we = cpu_write|boot;\n\nalways@(posedge fclk) begin\n    spiclk <= ~nxtcnt[0];\n\tclk <= ~nxtcnt[1];\n    cnt <= nxtcnt;\n    wg <= ~(phi2_we & nxtcnt[1]);\nend\n\nreg cpu_reset;\nalways@*\n    cpu_reset= ~locked | boot | reset_key;\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "fclk"
                    },
                    {
                      "name": "locked"
                    },
                    {
                      "name": "cpu_write"
                    },
                    {
                      "name": "boot"
                    },
                    {
                      "name": "reset_key"
                    }
                  ],
                  "out": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "wg"
                    },
                    {
                      "name": "spiclk"
                    },
                    {
                      "name": "phi2_we"
                    },
                    {
                      "name": "cpu_reset"
                    }
                  ]
                }
              },
              "position": {
                "x": 200,
                "y": 72
              },
              "size": {
                "width": 592,
                "height": 488
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "d1244797-3c77-4c65-9150-2afd651c6a7f",
                "port": "out"
              },
              "target": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "cpu_write"
              }
            },
            {
              "source": {
                "block": "65cc1da3-fbf4-4b2d-883c-70c6d2ce991d",
                "port": "out"
              },
              "target": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "boot"
              }
            },
            {
              "source": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "clk"
              },
              "target": {
                "block": "ed174b25-c643-46c1-8c39-be754bb62003",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "wg"
              },
              "target": {
                "block": "4f9f537e-abc0-413f-9093-12c57ac39a8b",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "spiclk"
              },
              "target": {
                "block": "ea0e6a9a-8ec4-4043-83f2-43261b13c98d",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "phi2_we"
              },
              "target": {
                "block": "91f05048-ed41-46d1-b3be-15804686c90b",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "b973cba3-f432-41c4-a390-c4e34a90380d",
                "port": "out"
              },
              "target": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "fclk"
              }
            },
            {
              "source": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "cpu_reset"
              },
              "target": {
                "block": "e3c470eb-7a70-4f65-95e5-2bacd85d04a3",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "213f7d87-3d22-401d-b022-6c673ef2d8cd",
                "port": "out"
              },
              "target": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "reset_key"
              }
            },
            {
              "source": {
                "block": "8b64dff7-cfa8-43f6-a518-14ef9be98cc0",
                "port": "out"
              },
              "target": {
                "block": "b2579481-434a-46d3-8aa6-b80c6117cccd",
                "port": "locked"
              }
            }
          ]
        }
      }
    },
    "e70bbd0254872fd86a6ce2e7fe68c9e55eb0e1c1": {
      "package": {
        "name": "SPIFlash",
        "version": "0.1",
        "description": "Flash reading over SPI",
        "author": "Clifford",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "e2fe99e9-92cb-43ea-b9c2-6a04a17e6ae1",
              "type": "basic.output",
              "data": {
                "name": "ready"
              },
              "position": {
                "x": 864,
                "y": 96
              }
            },
            {
              "id": "78f75945-cea9-4b5a-b092-0c1c8c0ed3a3",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": false
              },
              "position": {
                "x": 64,
                "y": 104
              }
            },
            {
              "id": "6cb24b51-0007-413d-9c63-5e9010b32000",
              "type": "basic.output",
              "data": {
                "name": "rdata",
                "range": "[31:0]",
                "size": 32
              },
              "position": {
                "x": 864,
                "y": 176
              }
            },
            {
              "id": "ee686b12-725d-407a-90c1-ff3b4f45b4bf",
              "type": "basic.input",
              "data": {
                "name": "resetn",
                "clock": false
              },
              "position": {
                "x": 64,
                "y": 184
              }
            },
            {
              "id": "267ebed8-9d5e-4251-b772-954a2aa7dbbb",
              "type": "basic.output",
              "data": {
                "name": "spi_cs"
              },
              "position": {
                "x": 864,
                "y": 256
              }
            },
            {
              "id": "9a275c1b-55c4-4d4a-a74b-1490d8caeb28",
              "type": "basic.input",
              "data": {
                "name": "valid",
                "clock": false
              },
              "position": {
                "x": 64,
                "y": 264
              }
            },
            {
              "id": "b921e9d4-e843-44b5-8ded-aecdd1420c1d",
              "type": "basic.output",
              "data": {
                "name": "spi_sclk"
              },
              "position": {
                "x": 864,
                "y": 336
              }
            },
            {
              "id": "f6d61dc3-924c-4d46-b9bd-1559c3931224",
              "type": "basic.input",
              "data": {
                "name": "addr",
                "range": "[23:0]",
                "clock": false,
                "size": 24
              },
              "position": {
                "x": 64,
                "y": 344
              }
            },
            {
              "id": "8e6aca50-d184-4e78-8ac9-26bfdcb89114",
              "type": "basic.output",
              "data": {
                "name": "spi_mosi"
              },
              "position": {
                "x": 864,
                "y": 416
              }
            },
            {
              "id": "0c81e9d1-795f-4ba3-8d5a-f198d9861711",
              "type": "basic.input",
              "data": {
                "name": "spi_miso",
                "clock": false
              },
              "position": {
                "x": 64,
                "y": 424
              }
            },
            {
              "id": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
              "type": "basic.code",
              "data": {
                "code": "reg ready;\nreg [31:0] rdata;\nreg spi_cs;\nreg spi_sclk;\nreg spi_mosi;\n\n\treg [7:0] buffer=0;\n\treg [3:0] xfer_cnt=0;\n\treg [3:0] state=0;\n\n\talways @(posedge clk) begin\n\t\tready <= 0;\n\t\tif (!resetn || !valid || ready) begin\n\t\t\tspi_cs <= 1;\n\t\t\tspi_sclk <= 1;\n\t\t\txfer_cnt <= 0;\n\t\t\tstate <= 0;\n\t\tend else begin\n\t\t\tspi_cs <= 0;\n\t\t\tif (xfer_cnt>0) begin\n\t\t\t\tif (spi_sclk) begin\n\t\t\t\t\tspi_sclk <= 0;\n\t\t\t\t\tspi_mosi <= buffer[7];\n\t\t\t\tend else begin\n\t\t\t\t\tspi_sclk <= 1;\n\t\t\t\t\tbuffer <= {buffer[6:0], spi_miso};\n\t\t\t\t\txfer_cnt <= xfer_cnt - 1;\n\t\t\t\tend\n\t\t\tend else\n\t\t\tcase (state)\n\t\t\t\t0: begin\n\t\t\t\t\tbuffer <= 'h03;\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 1;\n\t\t\t\tend\n\t\t\t\t1: begin\n\t\t\t\t\tbuffer <= addr[23:16];\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 2;\n\t\t\t\tend\n\t\t\t\t2: begin\n\t\t\t\t\tbuffer <= addr[15:8];\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 3;\n\t\t\t\tend\n\t\t\t\t3: begin\n\t\t\t\t\tbuffer <= addr[7:0];\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 4;\n\t\t\t\tend\n\t\t\t\t4: begin\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 5;\n\t\t\t\tend\n\t\t\t\t5: begin\n\t\t\t\t\trdata[7:0] <= buffer;\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 6;\n\t\t\t\tend\n\t\t\t\t6: begin\n\t\t\t\t\trdata[15:8] <= buffer;\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 7;\n\t\t\t\tend\n\t\t\t\t7: begin\n\t\t\t\t\trdata[23:16] <= buffer;\n\t\t\t\t\txfer_cnt <= 8;\n\t\t\t\t\tstate <= 8;\n\t\t\t\tend\n\t\t\t\t8: begin\n\t\t\t\t\trdata[31:24] <= buffer;\n\t\t\t\t\tready <= 1;\n\t\t\t\tend\n\t\t\tendcase\n\t\tend\n\tend\n\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "resetn"
                    },
                    {
                      "name": "valid"
                    },
                    {
                      "name": "addr",
                      "range": "[23:0]",
                      "size": 24
                    },
                    {
                      "name": "spi_miso"
                    }
                  ],
                  "out": [
                    {
                      "name": "ready"
                    },
                    {
                      "name": "rdata",
                      "range": "[31:0]",
                      "size": 32
                    },
                    {
                      "name": "spi_cs"
                    },
                    {
                      "name": "spi_sclk"
                    },
                    {
                      "name": "spi_mosi"
                    }
                  ]
                }
              },
              "position": {
                "x": 280,
                "y": 152
              },
              "size": {
                "width": 432,
                "height": 296
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "78f75945-cea9-4b5a-b092-0c1c8c0ed3a3",
                "port": "out"
              },
              "target": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "ee686b12-725d-407a-90c1-ff3b4f45b4bf",
                "port": "out"
              },
              "target": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "resetn"
              }
            },
            {
              "source": {
                "block": "9a275c1b-55c4-4d4a-a74b-1490d8caeb28",
                "port": "out"
              },
              "target": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "valid"
              }
            },
            {
              "source": {
                "block": "f6d61dc3-924c-4d46-b9bd-1559c3931224",
                "port": "out"
              },
              "target": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "addr"
              },
              "size": 24
            },
            {
              "source": {
                "block": "0c81e9d1-795f-4ba3-8d5a-f198d9861711",
                "port": "out"
              },
              "target": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "spi_miso"
              }
            },
            {
              "source": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "ready"
              },
              "target": {
                "block": "e2fe99e9-92cb-43ea-b9c2-6a04a17e6ae1",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "rdata"
              },
              "target": {
                "block": "6cb24b51-0007-413d-9c63-5e9010b32000",
                "port": "in"
              },
              "size": 32
            },
            {
              "source": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "spi_cs"
              },
              "target": {
                "block": "267ebed8-9d5e-4251-b772-954a2aa7dbbb",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "spi_sclk"
              },
              "target": {
                "block": "b921e9d4-e843-44b5-8ded-aecdd1420c1d",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "7e06befa-19d9-4133-bf9c-f4e5a806a2b2",
                "port": "spi_mosi"
              },
              "target": {
                "block": "8e6aca50-d184-4e78-8ac9-26bfdcb89114",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "8dc147848d751861ad658de6a0e3960b89b9719a": {
      "package": {
        "name": "SRAMbus",
        "version": "0.1",
        "description": "Shared SRAM access arbitrator for boot and cpu",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "19d99d7f-c246-4255-acb9-8d6fdcd942ec",
              "type": "basic.input",
              "data": {
                "name": "boot",
                "clock": false
              },
              "position": {
                "x": 72,
                "y": 72
              }
            },
            {
              "id": "84946bb2-4e77-4cc2-b380-0efabfb91feb",
              "type": "basic.input",
              "data": {
                "name": "dma_address",
                "range": "[16:0]",
                "clock": false,
                "size": 17
              },
              "position": {
                "x": 72,
                "y": 152
              }
            },
            {
              "id": "b705a660-684d-4a88-88b7-2672ba772512",
              "type": "basic.output",
              "data": {
                "name": "sram_addr",
                "range": "[18:0]",
                "size": 19
              },
              "position": {
                "x": 784,
                "y": 216
              }
            },
            {
              "id": "db776527-97b2-41ff-b2b8-5bbf9995c0df",
              "type": "basic.input",
              "data": {
                "name": "cpu_address",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 72,
                "y": 232
              }
            },
            {
              "id": "0eddc281-cf93-4b65-a14a-ba8ad56541d5",
              "type": "basic.output",
              "data": {
                "name": "sram_dout",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 784,
                "y": 296
              }
            },
            {
              "id": "aab334cc-fd1b-465e-8067-0d125a07e77c",
              "type": "basic.input",
              "data": {
                "name": "dma_data",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 72,
                "y": 312
              }
            },
            {
              "id": "dd2c60bf-436d-4666-82d3-f92420b1ac60",
              "type": "basic.input",
              "data": {
                "name": "cpu_dout",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": 72,
                "y": 392
              }
            },
            {
              "id": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
              "type": "basic.code",
              "data": {
                "code": "assign sram_addr = boot ? {2'b00,dma_address}:{3'b000,cpu_address};\n\nassign sram_dout = boot ? dma_data :cpu_dout;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "boot"
                    },
                    {
                      "name": "dma_address",
                      "range": "[16:0]",
                      "size": 17
                    },
                    {
                      "name": "cpu_address",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "dma_data",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "cpu_dout",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ],
                  "out": [
                    {
                      "name": "sram_addr",
                      "range": "[18:0]",
                      "size": 19
                    },
                    {
                      "name": "sram_dout",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 312,
                "y": 168
              },
              "size": {
                "width": 344,
                "height": 232
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "19d99d7f-c246-4255-acb9-8d6fdcd942ec",
                "port": "out"
              },
              "target": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "boot"
              }
            },
            {
              "source": {
                "block": "84946bb2-4e77-4cc2-b380-0efabfb91feb",
                "port": "out"
              },
              "target": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "dma_address"
              },
              "size": 17
            },
            {
              "source": {
                "block": "db776527-97b2-41ff-b2b8-5bbf9995c0df",
                "port": "out"
              },
              "target": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "cpu_address"
              },
              "size": 16
            },
            {
              "source": {
                "block": "aab334cc-fd1b-465e-8067-0d125a07e77c",
                "port": "out"
              },
              "target": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "dma_data"
              },
              "size": 16
            },
            {
              "source": {
                "block": "dd2c60bf-436d-4666-82d3-f92420b1ac60",
                "port": "out"
              },
              "target": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "cpu_dout"
              },
              "size": 16
            },
            {
              "source": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "sram_addr"
              },
              "target": {
                "block": "b705a660-684d-4a88-88b7-2672ba772512",
                "port": "in"
              },
              "size": 19
            },
            {
              "source": {
                "block": "cbc8ee5a-0205-41a5-827b-4c5e47ed0f24",
                "port": "sram_dout"
              },
              "target": {
                "block": "0eddc281-cf93-4b65-a14a-ba8ad56541d5",
                "port": "in"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "b47ef1a4e00a8193e22fa1dd2f49d389c9b6acf3": {
      "package": {
        "name": "bootloader",
        "version": "0.1",
        "description": "Flash bootloader copy flash to RAM",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "71681ce6-3708-4689-b268-3c11db8cf05b",
              "type": "basic.output",
              "data": {
                "name": "flash_valid"
              },
              "position": {
                "x": 1192,
                "y": -80
              }
            },
            {
              "id": "2668129f-cd4f-44ea-bdd7-14f005281a66",
              "type": "basic.input",
              "data": {
                "name": "clk",
                "clock": true
              },
              "position": {
                "x": -288,
                "y": 8
              }
            },
            {
              "id": "a24bf319-9d41-44ee-ad60-9a1cd90f9290",
              "type": "basic.output",
              "data": {
                "name": "flash_addr",
                "range": "[23:0]",
                "size": 24
              },
              "position": {
                "x": 1192,
                "y": 56
              }
            },
            {
              "id": "446f8401-0bb5-42b2-95e1-a73c15fe25b4",
              "type": "basic.input",
              "data": {
                "name": "flash_ready",
                "clock": false
              },
              "position": {
                "x": -288,
                "y": 88
              }
            },
            {
              "id": "700fe0e1-e94a-44f7-b76b-973299dcc2d5",
              "type": "basic.input",
              "data": {
                "name": "flash_data",
                "range": "[31:0]",
                "clock": false,
                "size": 32
              },
              "position": {
                "x": -288,
                "y": 168
              }
            },
            {
              "id": "ff6579bd-a2da-4ee7-83a4-be5a5cc9b3e2",
              "type": "basic.output",
              "data": {
                "name": "dma_addr",
                "range": "[16:0]",
                "size": 17
              },
              "position": {
                "x": 1200,
                "y": 192
              }
            },
            {
              "id": "f22d717f-017a-4562-97ec-7a0bbf61f87d",
              "type": "basic.input",
              "data": {
                "name": "locked",
                "clock": false
              },
              "position": {
                "x": -288,
                "y": 248
              }
            },
            {
              "id": "05bb2d1d-4775-45bd-9dab-9b66fbb3ec82",
              "type": "basic.output",
              "data": {
                "name": "boot"
              },
              "position": {
                "x": 1208,
                "y": 312
              }
            },
            {
              "id": "c361180d-4621-4620-a200-af168eb92a27",
              "type": "basic.input",
              "data": {
                "name": "reboot_req",
                "clock": false
              },
              "position": {
                "x": -288,
                "y": 328
              }
            },
            {
              "id": "2eab01ec-bd16-422f-baaa-f0a86bd58d9f",
              "type": "basic.output",
              "data": {
                "name": "boot_data",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 1208,
                "y": 448
              }
            },
            {
              "id": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
              "type": "basic.code",
              "data": {
                "code": "// Bootloader statemachine.\n`define BL_IDLE 0\n`define BL_SETUP 1\n`define BL_WAITFLASH 2\n`define BL_WRITE1 3\n`define BL_WRITE2 4\n`define BL_WRITE3 5\n`define BL_WRITE4 6\n`define BL_DONE 7\n  reg boot;\n  reg flash_valid;\n  reg [23:0] flash_addr;\n  reg [31:0] flash_copy;\n  reg [16:0] dma_addr=0;\n  reg   [2:0]  bl_state=0;\n  wire  [16:0] next_dma_addr = dma_addr + 1;\n  wire [23:0] next_flash_addr = flash_addr + 4;\n  reg   [15:0] boot_data=0;\n\n  always@(posedge clk) begin\n    if (~locked) begin\n      boot <= 1;\n      flash_valid <=0;\n      bl_state <= `BL_SETUP;\n      flash_addr <= 24'h40000;\n      boot_data <= 16'h0000;\n    end\n    else\n      case (bl_state)\n      `BL_IDLE:       begin\n                        boot <= 0;\n                        //delay <= vga_req;\n                        flash_valid <=0;\n                        bl_state <= reboot_req ? `BL_SETUP : `BL_IDLE;\n                      end\n      `BL_SETUP:      begin\n                        flash_addr <= 24'h40000;\n                        dma_addr <= 17'h8000;\n                        flash_valid <=1;\n                        boot <= 1;\n                        bl_state <= `BL_WAITFLASH;\n                      end\n      `BL_WAITFLASH:  begin\n                        //flash_valid <=0;\n                        if (flash_ready) begin\n                           flash_copy <= flash_data;\n                           flash_addr <= next_flash_addr;\n                           //flash_valid <=1;\n                           bl_state <= `BL_WRITE1;\n                        end\n                      end\n      `BL_WRITE1:     begin\n                        //flash_valid <=0;\n                        boot_data <= flash_copy[15:0];\n                        bl_state <= `BL_WRITE2;\n                      end\n      `BL_WRITE2:     begin\n                        dma_addr <= next_dma_addr;\n                        boot_data <= flash_copy[31:16];\n                        bl_state <= `BL_DONE;\n                      end\n      `BL_DONE:       begin\n                        dma_addr <= next_dma_addr;\n                        bl_state <= next_dma_addr[16] ? `BL_IDLE : `BL_WAITFLASH;\n                      end\n      default:        bl_state <=`BL_IDLE;\n      endcase\n  end\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "flash_ready"
                    },
                    {
                      "name": "flash_data",
                      "range": "[31:0]",
                      "size": 32
                    },
                    {
                      "name": "locked"
                    },
                    {
                      "name": "reboot_req"
                    }
                  ],
                  "out": [
                    {
                      "name": "flash_valid"
                    },
                    {
                      "name": "flash_addr",
                      "range": "[23:0]",
                      "size": 24
                    },
                    {
                      "name": "dma_addr",
                      "range": "[16:0]",
                      "size": 17
                    },
                    {
                      "name": "boot"
                    },
                    {
                      "name": "boot_data",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": -32,
                "y": -120
              },
              "size": {
                "width": 1072,
                "height": 680
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "2668129f-cd4f-44ea-bdd7-14f005281a66",
                "port": "out"
              },
              "target": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "446f8401-0bb5-42b2-95e1-a73c15fe25b4",
                "port": "out"
              },
              "target": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "flash_ready"
              }
            },
            {
              "source": {
                "block": "700fe0e1-e94a-44f7-b76b-973299dcc2d5",
                "port": "out"
              },
              "target": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "flash_data"
              },
              "size": 32
            },
            {
              "source": {
                "block": "f22d717f-017a-4562-97ec-7a0bbf61f87d",
                "port": "out"
              },
              "target": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "locked"
              }
            },
            {
              "source": {
                "block": "c361180d-4621-4620-a200-af168eb92a27",
                "port": "out"
              },
              "target": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "reboot_req"
              }
            },
            {
              "source": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "flash_valid"
              },
              "target": {
                "block": "71681ce6-3708-4689-b268-3c11db8cf05b",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "flash_addr"
              },
              "target": {
                "block": "a24bf319-9d41-44ee-ad60-9a1cd90f9290",
                "port": "in"
              },
              "size": 24
            },
            {
              "source": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "dma_addr"
              },
              "target": {
                "block": "ff6579bd-a2da-4ee7-83a4-be5a5cc9b3e2",
                "port": "in"
              },
              "size": 17
            },
            {
              "source": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "boot"
              },
              "target": {
                "block": "05bb2d1d-4775-45bd-9dab-9b66fbb3ec82",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "de2db4e7-b640-45dc-a389-66d3d82bed2b",
                "port": "boot_data"
              },
              "target": {
                "block": "2eab01ec-bd16-422f-baaa-f0a86bd58d9f",
                "port": "in"
              },
              "size": 16
            }
          ]
        }
      }
    },
    "f7dd2ac89ad4bef413a6e6b89faac40bbebd61ec": {
      "package": {
        "name": "",
        "version": "",
        "description": "",
        "author": "",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "71628cdc-a383-435f-a4ee-42d1cdf5739f",
              "type": "basic.output",
              "data": {
                "name": "sram_addr",
                "range": "[18:0]",
                "size": 19
              },
              "position": {
                "x": -264,
                "y": 160
              }
            },
            {
              "id": "2c316daa-b031-4239-a23d-ffa14f005cb6",
              "type": "basic.input",
              "data": {
                "name": "SRAMD",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": -136,
                "y": 160
              }
            },
            {
              "id": "f460f4f5-8fa9-49b9-8e68-3f8831f701ae",
              "type": "basic.input",
              "data": {
                "name": "sram_addr",
                "range": "[18:0]",
                "clock": false,
                "size": 19
              },
              "position": {
                "x": -448,
                "y": 448
              }
            },
            {
              "id": "05d0ea77-6648-48c2-9eed-c858bb980db1",
              "type": "basic.output",
              "data": {
                "name": "dout",
                "range": "[15:0]",
                "size": 16
              },
              "position": {
                "x": 688,
                "y": 504
              }
            },
            {
              "id": "1c9faf8b-efcc-458a-96a7-9afecc3d531a",
              "type": "basic.input",
              "data": {
                "name": "din",
                "range": "[15:0]",
                "clock": false,
                "size": 16
              },
              "position": {
                "x": -448,
                "y": 520
              }
            },
            {
              "id": "cfe7b36a-1475-45a6-b728-a0ba2a26e6b7",
              "type": "basic.input",
              "data": {
                "name": "wr_en",
                "clock": false
              },
              "position": {
                "x": -448,
                "y": 600
              }
            },
            {
              "id": "5801c2c6-9af6-42fc-8b3a-8e8ae81e09cd",
              "type": "basic.input",
              "data": {
                "name": "wg",
                "clock": false
              },
              "position": {
                "x": -448,
                "y": 680
              }
            },
            {
              "id": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
              "type": "basic.code",
              "data": {
                "code": "  SB_IO #(\n        .PIN_TYPE(6'b 1010_01),\n        .PULLUP(1'b 0)\n    ) sram_io [15:0] (\n        .PACKAGE_PIN(SRAM_D),\n        .OUTPUT_ENABLE(wr_en),\n        .D_OUT_0(Din),\n        .D_IN_0(Dout)\n    );",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "SRAM_D",
                      "range": "[15:0]",
                      "size": 16
                    },
                    {
                      "name": "wr_en"
                    },
                    {
                      "name": "wg"
                    },
                    {
                      "name": "Din",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ],
                  "out": [
                    {
                      "name": "Dout",
                      "range": "[15:0]",
                      "size": 16
                    }
                  ]
                }
              },
              "position": {
                "x": 120,
                "y": 352
              },
              "size": {
                "width": 456,
                "height": 360
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "2c316daa-b031-4239-a23d-ffa14f005cb6",
                "port": "out"
              },
              "target": {
                "block": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
                "port": "SRAM_D"
              },
              "size": 16
            },
            {
              "source": {
                "block": "f460f4f5-8fa9-49b9-8e68-3f8831f701ae",
                "port": "out"
              },
              "target": {
                "block": "71628cdc-a383-435f-a4ee-42d1cdf5739f",
                "port": "in"
              },
              "size": 19
            },
            {
              "source": {
                "block": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
                "port": "Dout"
              },
              "target": {
                "block": "05d0ea77-6648-48c2-9eed-c858bb980db1",
                "port": "in"
              },
              "size": 16
            },
            {
              "source": {
                "block": "1c9faf8b-efcc-458a-96a7-9afecc3d531a",
                "port": "out"
              },
              "target": {
                "block": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
                "port": "Din"
              },
              "size": 16
            },
            {
              "source": {
                "block": "cfe7b36a-1475-45a6-b728-a0ba2a26e6b7",
                "port": "out"
              },
              "target": {
                "block": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
                "port": "wr_en"
              }
            },
            {
              "source": {
                "block": "5801c2c6-9af6-42fc-8b3a-8e8ae81e09cd",
                "port": "out"
              },
              "target": {
                "block": "ae03954c-dfa0-4a31-a4fc-27caffcad06e",
                "port": "wg"
              }
            }
          ]
        }
      }
    },
    "b42fe4886af45d655453bb36e28f4c5838b905dd": {
      "package": {
        "name": "chargen",
        "version": "0.1",
        "description": "Character Generator",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "4bc9adae-f51c-4add-9e17-e0ced1ebc37e",
              "type": "basic.input",
              "data": {
                "name": "address",
                "range": "[9:0]",
                "clock": false,
                "size": 10
              },
              "position": {
                "x": 64,
                "y": 232
              }
            },
            {
              "id": "8dc18642-c2fc-497b-828a-ac24d0a902c0",
              "type": "basic.output",
              "data": {
                "name": "dout",
                "range": "[7:0]",
                "size": 8
              },
              "position": {
                "x": 832,
                "y": 232
              }
            },
            {
              "id": "192822e1-3ddf-45e3-b769-0f8f957b5e65",
              "type": "basic.code",
              "data": {
                "code": "reg [7:0]dout;\nalways @ (address)\nbegin\ncase (address)\n3 :dout = 8'b00011100 ;\n4 :dout = 8'b00100010 ;\n5 :dout = 8'b00000010 ;\n6 :dout = 8'b00011010 ;\n7 :dout = 8'b00101010 ;\n8 :dout = 8'b00101010 ;\n9 :dout = 8'b00011100 ;\n19 :dout = 8'b00001000 ;\n20 :dout = 8'b00010100 ;\n21 :dout = 8'b00100010 ;\n22 :dout = 8'b00100010 ;\n23 :dout = 8'b00111110 ;\n24 :dout = 8'b00100010 ;\n25 :dout = 8'b00100010 ;\n35 :dout = 8'b00111100 ;\n36 :dout = 8'b00010010 ;\n37 :dout = 8'b00010010 ;\n38 :dout = 8'b00011100 ;\n39 :dout = 8'b00010010 ;\n40 :dout = 8'b00010010 ;\n41 :dout = 8'b00111100 ;\n51 :dout = 8'b00011100 ;\n52 :dout = 8'b00100010 ;\n53 :dout = 8'b00100000 ;\n54 :dout = 8'b00100000 ;\n55 :dout = 8'b00100000 ;\n56 :dout = 8'b00100010 ;\n57 :dout = 8'b00011100 ;\n67 :dout = 8'b00111100 ;\n68 :dout = 8'b00010010 ;\n69 :dout = 8'b00010010 ;\n70 :dout = 8'b00010010 ;\n71 :dout = 8'b00010010 ;\n72 :dout = 8'b00010010 ;\n73 :dout = 8'b00111100 ;\n83 :dout = 8'b00111110 ;\n84 :dout = 8'b00100000 ;\n85 :dout = 8'b00100000 ;\n86 :dout = 8'b00111000 ;\n87 :dout = 8'b00100000 ;\n88 :dout = 8'b00100000 ;\n89 :dout = 8'b00111110 ;\n99 :dout = 8'b00111110 ;\n100 :dout = 8'b00100000 ;\n101 :dout = 8'b00100000 ;\n102 :dout = 8'b00111100 ;\n103 :dout = 8'b00100000 ;\n104 :dout = 8'b00100000 ;\n105 :dout = 8'b00100000 ;\n115 :dout = 8'b00011110 ;\n116 :dout = 8'b00100000 ;\n117 :dout = 8'b00100000 ;\n118 :dout = 8'b00100110 ;\n119 :dout = 8'b00100010 ;\n120 :dout = 8'b00100010 ;\n121 :dout = 8'b00011110 ;\n131 :dout = 8'b00100010 ;\n132 :dout = 8'b00100010 ;\n133 :dout = 8'b00100010 ;\n134 :dout = 8'b00111110 ;\n135 :dout = 8'b00100010 ;\n136 :dout = 8'b00100010 ;\n137 :dout = 8'b00100010 ;\n147 :dout = 8'b00011100 ;\n148 :dout = 8'b00001000 ;\n149 :dout = 8'b00001000 ;\n150 :dout = 8'b00001000 ;\n151 :dout = 8'b00001000 ;\n152 :dout = 8'b00001000 ;\n153 :dout = 8'b00011100 ;\n163 :dout = 8'b00000010 ;\n164 :dout = 8'b00000010 ;\n165 :dout = 8'b00000010 ;\n166 :dout = 8'b00000010 ;\n167 :dout = 8'b00100010 ;\n168 :dout = 8'b00100010 ;\n169 :dout = 8'b00011100 ;\n179 :dout = 8'b00100010 ;\n180 :dout = 8'b00100100 ;\n181 :dout = 8'b00101000 ;\n182 :dout = 8'b00110000 ;\n183 :dout = 8'b00101000 ;\n184 :dout = 8'b00100100 ;\n185 :dout = 8'b00100010 ;\n195 :dout = 8'b00100000 ;\n196 :dout = 8'b00100000 ;\n197 :dout = 8'b00100000 ;\n198 :dout = 8'b00100000 ;\n199 :dout = 8'b00100000 ;\n200 :dout = 8'b00100000 ;\n201 :dout = 8'b00111110 ;\n211 :dout = 8'b00100010 ;\n212 :dout = 8'b00110110 ;\n213 :dout = 8'b00101010 ;\n214 :dout = 8'b00100010 ;\n215 :dout = 8'b00100010 ;\n216 :dout = 8'b00100010 ;\n217 :dout = 8'b00100010 ;\n227 :dout = 8'b00100010 ;\n228 :dout = 8'b00110010 ;\n229 :dout = 8'b00101010 ;\n230 :dout = 8'b00100110 ;\n231 :dout = 8'b00100010 ;\n232 :dout = 8'b00100010 ;\n233 :dout = 8'b00100010 ;\n243 :dout = 8'b00011100 ;\n244 :dout = 8'b00100010 ;\n245 :dout = 8'b00100010 ;\n246 :dout = 8'b00100010 ;\n247 :dout = 8'b00100010 ;\n248 :dout = 8'b00100010 ;\n249 :dout = 8'b00011100 ;\n259 :dout = 8'b00111100 ;\n260 :dout = 8'b00100010 ;\n261 :dout = 8'b00100010 ;\n262 :dout = 8'b00111100 ;\n263 :dout = 8'b00100000 ;\n264 :dout = 8'b00100000 ;\n265 :dout = 8'b00100000 ;\n275 :dout = 8'b00011100 ;\n276 :dout = 8'b00100010 ;\n277 :dout = 8'b00100010 ;\n278 :dout = 8'b00100010 ;\n279 :dout = 8'b00101010 ;\n280 :dout = 8'b00100100 ;\n281 :dout = 8'b00011010 ;\n291 :dout = 8'b00111100 ;\n292 :dout = 8'b00100010 ;\n293 :dout = 8'b00100010 ;\n294 :dout = 8'b00111100 ;\n295 :dout = 8'b00101000 ;\n296 :dout = 8'b00100100 ;\n297 :dout = 8'b00100010 ;\n307 :dout = 8'b00011100 ;\n308 :dout = 8'b00100010 ;\n309 :dout = 8'b00010000 ;\n310 :dout = 8'b00001000 ;\n311 :dout = 8'b00000100 ;\n312 :dout = 8'b00100010 ;\n313 :dout = 8'b00011100 ;\n323 :dout = 8'b00111110 ;\n324 :dout = 8'b00001000 ;\n325 :dout = 8'b00001000 ;\n326 :dout = 8'b00001000 ;\n327 :dout = 8'b00001000 ;\n328 :dout = 8'b00001000 ;\n329 :dout = 8'b00001000 ;\n339 :dout = 8'b00100010 ;\n340 :dout = 8'b00100010 ;\n341 :dout = 8'b00100010 ;\n342 :dout = 8'b00100010 ;\n343 :dout = 8'b00100010 ;\n344 :dout = 8'b00100010 ;\n345 :dout = 8'b00011100 ;\n355 :dout = 8'b00100010 ;\n356 :dout = 8'b00100010 ;\n357 :dout = 8'b00100010 ;\n358 :dout = 8'b00100010 ;\n359 :dout = 8'b00010100 ;\n360 :dout = 8'b00010100 ;\n361 :dout = 8'b00001000 ;\n371 :dout = 8'b00100010 ;\n372 :dout = 8'b00100010 ;\n373 :dout = 8'b00100010 ;\n374 :dout = 8'b00100010 ;\n375 :dout = 8'b00101010 ;\n376 :dout = 8'b00110110 ;\n377 :dout = 8'b00100010 ;\n387 :dout = 8'b00100010 ;\n388 :dout = 8'b00100010 ;\n389 :dout = 8'b00010100 ;\n390 :dout = 8'b00001000 ;\n391 :dout = 8'b00010100 ;\n392 :dout = 8'b00100010 ;\n393 :dout = 8'b00100010 ;\n403 :dout = 8'b00100010 ;\n404 :dout = 8'b00100010 ;\n405 :dout = 8'b00010100 ;\n406 :dout = 8'b00001000 ;\n407 :dout = 8'b00001000 ;\n408 :dout = 8'b00001000 ;\n409 :dout = 8'b00001000 ;\n419 :dout = 8'b00111110 ;\n420 :dout = 8'b00000010 ;\n421 :dout = 8'b00000100 ;\n422 :dout = 8'b00001000 ;\n423 :dout = 8'b00010000 ;\n424 :dout = 8'b00100000 ;\n425 :dout = 8'b00111110 ;\n435 :dout = 8'b00011100 ;\n436 :dout = 8'b00010000 ;\n437 :dout = 8'b00010000 ;\n438 :dout = 8'b00010000 ;\n439 :dout = 8'b00010000 ;\n440 :dout = 8'b00010000 ;\n441 :dout = 8'b00011100 ;\n451 :dout = 8'b00100000 ;\n452 :dout = 8'b00100000 ;\n453 :dout = 8'b00010000 ;\n454 :dout = 8'b00001000 ;\n455 :dout = 8'b00000100 ;\n456 :dout = 8'b00000010 ;\n457 :dout = 8'b00000010 ;\n467 :dout = 8'b00011100 ;\n468 :dout = 8'b00000100 ;\n469 :dout = 8'b00000100 ;\n470 :dout = 8'b00000100 ;\n471 :dout = 8'b00000100 ;\n472 :dout = 8'b00000100 ;\n473 :dout = 8'b00011100 ;\n483 :dout = 8'b00001000 ;\n484 :dout = 8'b00011100 ;\n485 :dout = 8'b00111110 ;\n486 :dout = 8'b00001000 ;\n487 :dout = 8'b00001000 ;\n488 :dout = 8'b00001000 ;\n489 :dout = 8'b00001000 ;\n500 :dout = 8'b00001000 ;\n501 :dout = 8'b00010000 ;\n502 :dout = 8'b00111110 ;\n503 :dout = 8'b00010000 ;\n504 :dout = 8'b00001000 ;\n531 :dout = 8'b00001000 ;\n532 :dout = 8'b00001000 ;\n533 :dout = 8'b00001000 ;\n534 :dout = 8'b00001000 ;\n535 :dout = 8'b00001000 ;\n537 :dout = 8'b00001000 ;\n547 :dout = 8'b00010100 ;\n548 :dout = 8'b00010100 ;\n563 :dout = 8'b00010100 ;\n564 :dout = 8'b00010100 ;\n565 :dout = 8'b00110110 ;\n567 :dout = 8'b00110110 ;\n568 :dout = 8'b00010100 ;\n569 :dout = 8'b00010100 ;\n579 :dout = 8'b00001000 ;\n580 :dout = 8'b00011110 ;\n581 :dout = 8'b00100000 ;\n582 :dout = 8'b00011100 ;\n583 :dout = 8'b00000010 ;\n584 :dout = 8'b00111100 ;\n585 :dout = 8'b00001000 ;\n595 :dout = 8'b00110010 ;\n596 :dout = 8'b00110010 ;\n597 :dout = 8'b00000100 ;\n598 :dout = 8'b00001000 ;\n599 :dout = 8'b00010000 ;\n600 :dout = 8'b00100110 ;\n601 :dout = 8'b00100110 ;\n611 :dout = 8'b00010000 ;\n612 :dout = 8'b00101000 ;\n613 :dout = 8'b00101000 ;\n614 :dout = 8'b00010010 ;\n615 :dout = 8'b00101100 ;\n616 :dout = 8'b00101100 ;\n617 :dout = 8'b00010010 ;\n627 :dout = 8'b00001000 ;\n628 :dout = 8'b00001000 ;\n643 :dout = 8'b00000100 ;\n644 :dout = 8'b00001000 ;\n645 :dout = 8'b00010000 ;\n646 :dout = 8'b00010000 ;\n647 :dout = 8'b00010000 ;\n648 :dout = 8'b00001000 ;\n649 :dout = 8'b00000100 ;\n659 :dout = 8'b00010000 ;\n660 :dout = 8'b00001000 ;\n661 :dout = 8'b00000100 ;\n662 :dout = 8'b00000100 ;\n663 :dout = 8'b00000100 ;\n664 :dout = 8'b00001000 ;\n665 :dout = 8'b00010000 ;\n676 :dout = 8'b00001000 ;\n677 :dout = 8'b00101010 ;\n678 :dout = 8'b00011100 ;\n679 :dout = 8'b00101010 ;\n680 :dout = 8'b00001000 ;\n692 :dout = 8'b00001000 ;\n693 :dout = 8'b00001000 ;\n694 :dout = 8'b00111110 ;\n695 :dout = 8'b00001000 ;\n696 :dout = 8'b00001000 ;\n710 :dout = 8'b00001100 ;\n711 :dout = 8'b00001100 ;\n712 :dout = 8'b00000100 ;\n713 :dout = 8'b00001000 ;\n726 :dout = 8'b00111110 ;\n744 :dout = 8'b00001000 ;\n745 :dout = 8'b00001000 ;\n755 :dout = 8'b00000010 ;\n756 :dout = 8'b00000010 ;\n757 :dout = 8'b00000100 ;\n758 :dout = 8'b00001000 ;\n759 :dout = 8'b00010000 ;\n760 :dout = 8'b00100000 ;\n761 :dout = 8'b00100000 ;\n771 :dout = 8'b00011100 ;\n772 :dout = 8'b00100010 ;\n773 :dout = 8'b00100110 ;\n774 :dout = 8'b00101010 ;\n775 :dout = 8'b00110010 ;\n776 :dout = 8'b00100010 ;\n777 :dout = 8'b00011100 ;\n787 :dout = 8'b00001000 ;\n788 :dout = 8'b00011000 ;\n789 :dout = 8'b00001000 ;\n790 :dout = 8'b00001000 ;\n791 :dout = 8'b00001000 ;\n792 :dout = 8'b00001000 ;\n793 :dout = 8'b00011100 ;\n803 :dout = 8'b00011100 ;\n804 :dout = 8'b00100010 ;\n805 :dout = 8'b00000010 ;\n806 :dout = 8'b00011100 ;\n807 :dout = 8'b00100000 ;\n808 :dout = 8'b00100000 ;\n809 :dout = 8'b00111110 ;\n819 :dout = 8'b00011100 ;\n820 :dout = 8'b00100010 ;\n821 :dout = 8'b00000010 ;\n822 :dout = 8'b00001100 ;\n823 :dout = 8'b00000010 ;\n824 :dout = 8'b00100010 ;\n825 :dout = 8'b00011100 ;\n835 :dout = 8'b00000100 ;\n836 :dout = 8'b00001100 ;\n837 :dout = 8'b00010100 ;\n838 :dout = 8'b00111110 ;\n839 :dout = 8'b00000100 ;\n840 :dout = 8'b00000100 ;\n841 :dout = 8'b00000100 ;\n851 :dout = 8'b00111110 ;\n852 :dout = 8'b00100000 ;\n853 :dout = 8'b00111100 ;\n854 :dout = 8'b00000010 ;\n855 :dout = 8'b00000010 ;\n856 :dout = 8'b00100010 ;\n857 :dout = 8'b00011100 ;\n867 :dout = 8'b00011100 ;\n868 :dout = 8'b00100000 ;\n869 :dout = 8'b00100000 ;\n870 :dout = 8'b00111100 ;\n871 :dout = 8'b00100010 ;\n872 :dout = 8'b00100010 ;\n873 :dout = 8'b00011100 ;\n883 :dout = 8'b00111110 ;\n884 :dout = 8'b00000010 ;\n885 :dout = 8'b00000100 ;\n886 :dout = 8'b00001000 ;\n887 :dout = 8'b00010000 ;\n888 :dout = 8'b00100000 ;\n889 :dout = 8'b00100000 ;\n899 :dout = 8'b00011100 ;\n900 :dout = 8'b00100010 ;\n901 :dout = 8'b00100010 ;\n902 :dout = 8'b00011100 ;\n903 :dout = 8'b00100010 ;\n904 :dout = 8'b00100010 ;\n905 :dout = 8'b00011100 ;\n915 :dout = 8'b00011100 ;\n916 :dout = 8'b00100010 ;\n917 :dout = 8'b00100010 ;\n918 :dout = 8'b00011110 ;\n919 :dout = 8'b00000010 ;\n920 :dout = 8'b00000010 ;\n921 :dout = 8'b00011100 ;\n933 :dout = 8'b00001000 ;\n935 :dout = 8'b00001000 ;\n947 :dout = 8'b00001100 ;\n948 :dout = 8'b00001100 ;\n950 :dout = 8'b00001100 ;\n951 :dout = 8'b00001100 ;\n952 :dout = 8'b00000100 ;\n953 :dout = 8'b00001000 ;\n963 :dout = 8'b00000100 ;\n964 :dout = 8'b00001000 ;\n965 :dout = 8'b00010000 ;\n966 :dout = 8'b00100000 ;\n967 :dout = 8'b00010000 ;\n968 :dout = 8'b00001000 ;\n969 :dout = 8'b00000100 ;\n981 :dout = 8'b00111110 ;\n983 :dout = 8'b00111110 ;\n995 :dout = 8'b00100000 ;\n996 :dout = 8'b00010000 ;\n997 :dout = 8'b00001000 ;\n998 :dout = 8'b00000100 ;\n999 :dout = 8'b00001000 ;\n1000 :dout = 8'b00010000 ;\n1001 :dout = 8'b00100000 ;\n1011 :dout = 8'b00011100 ;\n1012 :dout = 8'b00100010 ;\n1013 :dout = 8'b00000100 ;\n1014 :dout = 8'b00001000 ;\n1015 :dout = 8'b00001000 ;\n1017 :dout = 8'b00001000 ;\ndefault :dout = 8'b00000000 ;\nendcase\nend",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "address",
                      "range": "[9:0]",
                      "size": 10
                    }
                  ],
                  "out": [
                    {
                      "name": "dout",
                      "range": "[7:0]",
                      "size": 8
                    }
                  ]
                }
              },
              "position": {
                "x": 272,
                "y": 112
              },
              "size": {
                "width": 456,
                "height": 304
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "4bc9adae-f51c-4add-9e17-e0ced1ebc37e",
                "port": "out"
              },
              "target": {
                "block": "192822e1-3ddf-45e3-b769-0f8f957b5e65",
                "port": "address"
              },
              "size": 10
            },
            {
              "source": {
                "block": "192822e1-3ddf-45e3-b769-0f8f957b5e65",
                "port": "dout"
              },
              "target": {
                "block": "8dc18642-c2fc-497b-828a-ac24d0a902c0",
                "port": "in"
              },
              "size": 8
            }
          ]
        }
      }
    },
    "c4dd08263a85a91ba53e2ae2b38de344c5efcb52": {
      "package": {
        "name": "Bit 0",
        "version": "1.0.0",
        "description": "Assign 0 to the output wire",
        "author": "Jesús Arroyo",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2247.303%22%20height=%2227.648%22%20viewBox=%220%200%2044.346456%2025.919999%22%3E%3Ctext%20style=%22line-height:125%25%22%20x=%22325.37%22%20y=%22315.373%22%20font-weight=%22400%22%20font-size=%2212.669%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%20transform=%22translate(-307.01%20-298.51)%22%3E%3Ctspan%20x=%22325.37%22%20y=%22315.373%22%20style=%22-inkscape-font-specification:'Courier%2010%20Pitch'%22%20font-family=%22Courier%2010%20Pitch%22%3E0%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "19c8f68d-5022-487f-9ab0-f0a3cd58bead",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 608,
                "y": 192
              }
            },
            {
              "id": "b959fb96-ac67-4aea-90b3-ed35a4c17bf5",
              "type": "basic.code",
              "data": {
                "code": "// Bit 0\n\nassign v = 1'b0;",
                "params": [],
                "ports": {
                  "in": [],
                  "out": [
                    {
                      "name": "v"
                    }
                  ]
                }
              },
              "position": {
                "x": 96,
                "y": 96
              },
              "size": {
                "width": 384,
                "height": 256
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "b959fb96-ac67-4aea-90b3-ed35a4c17bf5",
                "port": "v"
              },
              "target": {
                "block": "19c8f68d-5022-487f-9ab0-f0a3cd58bead",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "e04e1531ece65bcd7b91c6ea1e3dbcd590811245": {
      "package": {
        "name": "cpubus",
        "version": "0.1",
        "description": "IO and bus select",
        "author": "J.R.Peterzon",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "779bce36-2fab-4f67-89dd-98ca8b2563a9",
              "type": "basic.output",
              "data": {
                "name": "IOsel"
              },
              "position": {
                "x": 736,
                "y": 88
              }
            },
            {
              "id": "494fac32-c5b7-4a98-80c3-1b115c47558a",
              "type": "basic.output",
              "data": {
                "name": "VGAsel"
              },
              "position": {
                "x": 736,
                "y": 168
              }
            },
            {
              "id": "a97b5b0f-cf20-4b68-9225-f957fec63f38",
              "type": "basic.input",
              "data": {
                "name": "cpu_address",
                "range": "[18:0]",
                "clock": false,
                "size": 19
              },
              "position": {
                "x": 72,
                "y": 216
              }
            },
            {
              "id": "78c719c3-46c3-4534-9a9f-52dcdadbc033",
              "type": "basic.output",
              "data": {
                "name": "VGAmem"
              },
              "position": {
                "x": 736,
                "y": 248
              }
            },
            {
              "id": "9d18b5ff-b31a-4aa8-97f9-d21293038d66",
              "type": "basic.output",
              "data": {
                "name": "VGAbus",
                "range": "[12:0]",
                "size": 13
              },
              "position": {
                "x": 736,
                "y": 328
              }
            },
            {
              "id": "3f269230-ce19-4880-a476-858aeaf9d453",
              "type": "basic.code",
              "data": {
                "code": "assign IOsel=cpu_address[15:12]==4'hb;\nassign VGAsel=IOsel&(cpu_address[11:8]==4'hA);\nassign VGAmem=cpu_address[15:13]==3'b100;\nassign VGAbus=cpu_address[12:0];\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "cpu_address",
                      "range": "[18:0]",
                      "size": 19
                    }
                  ],
                  "out": [
                    {
                      "name": "IOsel"
                    },
                    {
                      "name": "VGAsel"
                    },
                    {
                      "name": "VGAmem"
                    },
                    {
                      "name": "VGAbus",
                      "range": "[12:0]",
                      "size": 13
                    }
                  ]
                }
              },
              "position": {
                "x": 328,
                "y": 152
              },
              "size": {
                "width": 296,
                "height": 184
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "3f269230-ce19-4880-a476-858aeaf9d453",
                "port": "IOsel"
              },
              "target": {
                "block": "779bce36-2fab-4f67-89dd-98ca8b2563a9",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "3f269230-ce19-4880-a476-858aeaf9d453",
                "port": "VGAsel"
              },
              "target": {
                "block": "494fac32-c5b7-4a98-80c3-1b115c47558a",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "3f269230-ce19-4880-a476-858aeaf9d453",
                "port": "VGAmem"
              },
              "target": {
                "block": "78c719c3-46c3-4534-9a9f-52dcdadbc033",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "3f269230-ce19-4880-a476-858aeaf9d453",
                "port": "VGAbus"
              },
              "target": {
                "block": "9d18b5ff-b31a-4aa8-97f9-d21293038d66",
                "port": "in"
              },
              "size": 13
            },
            {
              "source": {
                "block": "a97b5b0f-cf20-4b68-9225-f957fec63f38",
                "port": "out"
              },
              "target": {
                "block": "3f269230-ce19-4880-a476-858aeaf9d453",
                "port": "cpu_address"
              },
              "size": 19
            }
          ]
        }
      }
    },
    "cfd9babc26edba88e2152493023c4bef7c47f247": {
      "package": {
        "name": "Debouncer",
        "version": "1.0.0",
        "description": "Remove the rebound on a mechanical switch",
        "author": "Juan González",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%22-252%20400.9%2090%2040%22%3E%3Cpath%20d=%22M-251.547%20436.672h22.802v-30.353h5.862v30.353h5.259v-30.353h3.447v30.353h2.984v-30.353h3.506v30.523h6.406V405.77h38.868%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%221.4%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M-232.57%20403.877l26.946%2032.391M-205.624%20403.877l-26.946%2032.391%22%20fill=%22none%22%20stroke=%22red%22%20stroke-width=%223%22%20stroke-linecap=%22round%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "4bf41c17-a2da-4140-95f7-2a80d51b1e1a",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 48,
                "y": 144
              }
            },
            {
              "id": "22ff3fa1-943b-4d1a-bd89-36e1c054d077",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 768,
                "y": 208
              }
            },
            {
              "id": "c9e1af2a-6f09-4cf6-a5b3-fdf7ec2c6530",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 48,
                "y": 272
              }
            },
            {
              "id": "92490e7e-c3ba-4e9c-a917-2a771d99f1ef",
              "type": "basic.code",
              "data": {
                "code": "//-- Debouncer Circuit\n//-- It produces a stable output when the\n//-- input signal is bouncing\n\nreg btn_prev = 0;\nreg btn_out_r = 0;\n\nreg [16:0] counter = 0;\n\n\nalways @(posedge clk) begin\n\n  //-- If btn_prev and btn_in are differents\n  if (btn_prev ^ in == 1'b1) begin\n    \n      //-- Reset the counter\n      counter <= 0;\n      \n      //-- Capture the button status\n      btn_prev <= in;\n  end\n    \n  //-- If no timeout, increase the counter\n  else if (counter[16] == 1'b0)\n      counter <= counter + 1;\n      \n  else\n    //-- Set the output to the stable value\n    btn_out_r <= btn_prev;\n\nend\n\nassign out = btn_out_r;\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "in"
                    }
                  ],
                  "out": [
                    {
                      "name": "out"
                    }
                  ]
                }
              },
              "position": {
                "x": 264,
                "y": 112
              },
              "size": {
                "width": 384,
                "height": 256
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "92490e7e-c3ba-4e9c-a917-2a771d99f1ef",
                "port": "out"
              },
              "target": {
                "block": "22ff3fa1-943b-4d1a-bd89-36e1c054d077",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "4bf41c17-a2da-4140-95f7-2a80d51b1e1a",
                "port": "out"
              },
              "target": {
                "block": "92490e7e-c3ba-4e9c-a917-2a771d99f1ef",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "c9e1af2a-6f09-4cf6-a5b3-fdf7ec2c6530",
                "port": "out"
              },
              "target": {
                "block": "92490e7e-c3ba-4e9c-a917-2a771d99f1ef",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "8026abbda6bfa79c6f4c6debd4e97ae7b51ee8d3": {
      "package": {
        "name": "Pull-up",
        "version": "1.0.0",
        "description": "FPGA internal pull-up configuration on the connected input port",
        "author": "Juan González",
        "image": "%3Csvg%20id=%22svg2%22%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%22-265%20401.5%2063.5%2038.4%22%3E%3Cstyle%3E.st0%7Bdisplay:none%7D.st1%7Bfill:none;stroke:#000;stroke-width:.75;stroke-linejoin:round;stroke-miterlimit:10%7D.st2%7Bfill:#010002%7D%3C/style%3E%3Cpath%20class=%22st0%22%20d=%22M-242.5%20411.8v11.8h-5.4v-11.8h5.4m1-1h-7.4v13.8h7.4v-13.8z%22/%3E%3Cpath%20d=%22M-212%20425.6l-15.4-8.7v8.5h-17.4v-2.7c0-.2-.1-.4-.3-.4l-2.3-1.2%205.6-2.9c.2-.1.3-.3.3-.5s-.1-.4-.3-.4l-5.7-2.7%202.4-1.6c.1-.1.2-.2.2-.4v-2.7h3.1l-3.5-6.1-3.5%206.1h3v2.5l-2.9%202c-.1.1-.2.3-.2.5s.1.3.3.4l5.6%202.6-5.6%202.9c-.2.1-.3.3-.3.4s.1.4.3.4l2.9%201.5V425.5H-265v1.2h37.6v8.5l15.4-8.7h10.5v-.8H-212zm-33.3-20.4l2.2%203.9h-4.5l2.3-3.9zm19.2%2027.7v-13.8l12.3%206.9-12.3%206.9z%22/%3E%3C/svg%3E"
      },
      "design": {
        "config": "true",
        "pullup": "true",
        "graph": {
          "blocks": [
            {
              "id": "bb4a1ca9-1b30-471e-92ca-ca7ff2fc1150",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 200
              }
            },
            {
              "id": "a139fa0d-9b45-4480-a251-f4a66b49aa23",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 760,
                "y": 200
              }
            },
            {
              "id": "2b245a71-2d80-466b-955f-e3d61839fe25",
              "type": "basic.code",
              "data": {
                "code": "// Pull up\n\nwire din, dout, outen;\n\nassign o = din;\n\nSB_IO #(\n    .PIN_TYPE(6'b 1010_01),\n    .PULLUP(1'b 1)\n) io_pin (\n    .PACKAGE_PIN(i),\n    .OUTPUT_ENABLE(outen),\n    .D_OUT_0(dout),\n    .D_IN_0(din)\n);",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "i"
                    }
                  ],
                  "out": [
                    {
                      "name": "o"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 104
              },
              "size": {
                "width": 384,
                "height": 256
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "bb4a1ca9-1b30-471e-92ca-ca7ff2fc1150",
                "port": "out"
              },
              "target": {
                "block": "2b245a71-2d80-466b-955f-e3d61839fe25",
                "port": "i"
              }
            },
            {
              "source": {
                "block": "2b245a71-2d80-466b-955f-e3d61839fe25",
                "port": "o"
              },
              "target": {
                "block": "a139fa0d-9b45-4480-a251-f4a66b49aa23",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "96f0988f8164f7c1b216c8ee122d6ce3cf6bc139": {
      "package": {
        "name": "NOT",
        "version": "1.0.0",
        "description": "NOT logic gate",
        "author": "Jesús Arroyo",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2291.33%22%20height=%2245.752%22%20version=%221%22%3E%3Cpath%20d=%22M0%2020.446h27v2H0zM70.322%2020.447h15.3v2h-15.3z%22/%3E%3Cpath%20d=%22M66.05%2026.746c-2.9%200-5.3-2.4-5.3-5.3s2.4-5.3%205.3-5.3%205.3%202.4%205.3%205.3-2.4%205.3-5.3%205.3zm0-8.6c-1.8%200-3.3%201.5-3.3%203.3%200%201.8%201.5%203.3%203.3%203.3%201.8%200%203.3-1.5%203.3-3.3%200-1.8-1.5-3.3-3.3-3.3z%22/%3E%3Cpath%20d=%22M25.962%202.563l33.624%2018.883L25.962%2040.33V2.563z%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%223%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "18c2ebc7-5152-439c-9b3f-851c59bac834",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 144
              }
            },
            {
              "id": "664caf9e-5f40-4df4-800a-b626af702e62",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 752,
                "y": 144
              }
            },
            {
              "id": "5365ed8c-e5db-4445-938f-8d689830ea5c",
              "type": "basic.code",
              "data": {
                "code": "// NOT logic gate\n\nassign c = ~ a;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a"
                    }
                  ],
                  "out": [
                    {
                      "name": "c"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 48
              },
              "size": {
                "width": 384,
                "height": 256
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "18c2ebc7-5152-439c-9b3f-851c59bac834",
                "port": "out"
              },
              "target": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "a"
              }
            },
            {
              "source": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "c"
              },
              "target": {
                "block": "664caf9e-5f40-4df4-800a-b626af702e62",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "7ebc902cbb1c4db116741533a86182485900ecda": {
      "package": {
        "name": "AND",
        "version": "1.0.0",
        "description": "AND logic gate",
        "author": "Jesús Arroyo",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%22-252%20400.9%2090%2040%22%3E%3Cpath%20d=%22M-252%20409.9h26v2h-26zM-252%20429.9h27v2h-27z%22/%3E%3Cpath%20d=%22M-227%20400.9v39.9h20.4c11.3%200%2020-9%2020-20s-8.7-20-20-20H-227zm2.9%202.8h17.6c9.8%200%2016.7%207.6%2016.7%2017.1%200%209.5-7.4%2017.1-17.1%2017.1H-224c-.1.1-.1-34.2-.1-34.2z%22/%3E%3Cpath%20d=%22M-187.911%20419.9H-162v2h-25.911z%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "18c2ebc7-5152-439c-9b3f-851c59bac834",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 80
              }
            },
            {
              "id": "664caf9e-5f40-4df4-800a-b626af702e62",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 752,
                "y": 144
              }
            },
            {
              "id": "97b51945-d716-4b6c-9db9-970d08541249",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 208
              }
            },
            {
              "id": "00925b04-5004-4307-a737-fa4e97c8b6ab",
              "type": "basic.code",
              "data": {
                "code": "// AND logic gate\n\nassign c = a & b;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a"
                    },
                    {
                      "name": "b"
                    }
                  ],
                  "out": [
                    {
                      "name": "c"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 48
              },
              "size": {
                "width": 384,
                "height": 256
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "18c2ebc7-5152-439c-9b3f-851c59bac834",
                "port": "out"
              },
              "target": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "a"
              }
            },
            {
              "source": {
                "block": "97b51945-d716-4b6c-9db9-970d08541249",
                "port": "out"
              },
              "target": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "b"
              }
            },
            {
              "source": {
                "block": "00925b04-5004-4307-a737-fa4e97c8b6ab",
                "port": "c"
              },
              "target": {
                "block": "664caf9e-5f40-4df4-800a-b626af702e62",
                "port": "in"
              }
            }
          ]
        }
      }
    }
  }
}