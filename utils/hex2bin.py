#!/usr/bin/env python3

import fileinput
import sys

if (len(sys.argv)<3):
    print("Usage: %s <hexfile> <binfile>" % sys.argv[0])
    sys.exit(0)

infile = sys.argv[1] 
outfile = sys.argv[2]

out_data = list()

def set_addr(addr, value):
    while len(out_data) <= addr:
        out_data.append(0)
    out_data[addr] = value

def set_data(addr, value):
    if 0x8000 <= addr < 0x100000:
        set_addr((addr - 0x10000), value)
    else:
        print("ADDR = %x" % addr)
        assert False

cursor = 0
line=""
with open(infile, "rt") as f:
  try:
    for line in f:
        if line.startswith("@"):
            cursor = int(line[1:], 16)*2
            continue
        if line.startswith("#"):
            continue
        for value in line.split():
            set_data(cursor, int(value, 16))
            cursor += 1
  except:
    print(line)

with open(outfile, "wb") as f:
    f.write(bytearray(out_data))

