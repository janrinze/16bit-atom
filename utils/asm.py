#!/usr/bin/env python3

import fileinput
import sys

#print ("This is the name of the script: %s" % sys.argv[0] )
#print ("Number of arguments: %d" % len(sys.argv))
#print ("The arguments are: %s" % str(sys.argv))

labels={}
to_ref = {}
to_parse = {}
has_operands = {}
address=0

output_enabled = 0

tohex = [ '0',
'1',
'2',
'3',
'4',
'5',
'6',
'7',
'8',
'9',
'a',
'b',
'c',
'd',
'e',
'f']


def optprint(line):
  global output_enabled
  if (output_enabled==1):
    print(line)

def optout_word(val):
  global output_enabled
  global address
  if (output_enabled==1):
    print("%02x\n%02x" %(val&255,val>>8))
  address+=1

def gen_out(nibbles):
  global address
  optprint('%s%s\n%s%s' %( nibbles[2],nibbles[3],nibbles[0],nibbles[1]))
  address+=1

def to_int8(src):
  if src[0]=='#':
    src=src[1:]
  tmp=-1
  if (src[0]=='\''):
    tmp = ord(src[1])
  else:
    if (src[0]=='x') or (src[0]=='X') or (src[0]=='&'):
        tmp = int(src[1:],16)
    else:
        tmp = int(src,10)
  if (-1<tmp<256):
    return tmp
  else:
    print("Error: value not 8-bit compatible.")
    return 0

def to_int4(src):
  if (src=='pc'):
    return 15
  if (src=='lr'):
    return 14
  if (src=='sp'):
    return 13
  if (src=='fp'):
    return 12
  if (src[0]=='r'):
    src=src[1:]
  val = to_int8(src)
  if (-1<val<16):
    return val
  else:
    print("Error: value not 4-bit compatible. / invalid register number")
    return 0

def to_ref12( src ):
  global address
  refaddress = 0
  indirect = 0;
  if src[0]=='@':
    src = src[1:]
    indirect = 2048
  if src in labels:
    refaddress = labels[src] - (address+1)
  else:
     if src[0]=='x' or src[0]=='&':
      refaddress = int(src[1:],16) - (address+1)
     else:
      #refaddress = int(src[1:],10) - (address+1)
      optprint("ERROR: cannot process %s" % src)
  if (refaddress>1023 or refaddress < -1022):
    optprint("ERROR: 12-bit offset out of range.")
    return 0
  if refaddress<0:
    refaddress = 2048+refaddress
  return refaddress + indirect

def to_address(src):
  refaddress = 0
  indirect = 0;
  if src in labels:
    refaddress = labels[src]
  else:
     if src[0]=='&' or src[0]=='#':
      refaddress = int(src[1:],16)
     else:
      optprint("ERROR: cannot convert to address %s" % src)
  return refaddress

def reference(op,dat):
  global address
  refaddress = 0
  items = dat.lower().split(",")
  val = to_ref[op] + to_ref12(items[0])
  optout_word(val)

def Raddress(op,dat):
  val = to_ref[op] + 0x800;
  optout_word(val)
  optout_word(to_address(dat))

def abc(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",",2)
  for it in items:
    val += (to_int4(it) << shft)
    shft -= 4
  optout_word(val)

def abdouble(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += (to_int4(items[1]) << 4)
  val += to_int4(items[1])
  optout_word(val)

def abpc(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += (to_int4(items[1]) << 4)
  val += 15
  optout_word(val)

def lit(op,dat):
  items = dat.split(",",1)
  val = to_ref[op] + (to_int4(items[0]) << 8) + to_int8(items[1])
  optout_word(val)

def genrts(op):
  val = to_ref['orr'] + 0xfee
  optout_word(val)

def addr(op,dat):
  global address
  # generate multiple instructions to get rdest=address.
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",",1)
  
  if items[1] in labels:
    ref = address+2 - labels[items[1]]
    if (-256<ref<256):
      regno = to_int4(items[0])
      if (ref<0):
        base_op = 0x80
        ref = -ref
      else:
        base_op = 0x90
      optprint("%02x" % ref)
      optprint("%02x" % (regno+0x70))
      optprint("%02x" % (regno+0xf0))
      optprint("%02x" % (base_op+regno))
    else:
      optprint("ERROR: adr %s,%s : Out of range." %(items[0],items[1]))
  else:
    optprint("ERROR: adr %s,%s : label not found." %(items[0],items[1]))
  address+=2
    
def myjmp(op,dat):
    reference('bmi',dat)
    reference('bpl',dat)

def myloadstore(op,dat):
  global address
  # generate multiple instructions to get rdest=address.
  items = dat.lower().split(",",1)
  ref=to_address(items[1])
  if (op=='load'):
    base_op = 0x40ff
  else:
    base_op = 0x50ff  
  regno = to_int4(items[0])
  optout_word(base_op+(regno<<8))
  optout_word(ref)

statements = [ 
('BPL' , 0x0000 , reference),	# BPL ref
('BMI' , 0x1000 , reference),	# BMI ref
('BEQ' , 0x2000 , reference),	# BEQ ref
('BNE' , 0x3000 , reference),	# BNE ref
('LDR' , 0x4000 , abc      ),	# LDR Ra,[Rb,#off]
('STR' , 0x5000 , abc      ),	# STR Ra,[Rb,#off]
('JSR' , 0x6000 , Raddress),	# JSR Ra+#offset
('LIT' , 0x7000 , lit      ),	
('ADD' , 0x8000 , abc      ),	
('SUB' , 0x9000 , abc      ),	
('SHL' , 0xa000 , abc      ),	
('SHR' , 0xb000 , abc      ),	
('AND' , 0xc000 , abc      ),	
('ORR' , 0xd000 , abc      ),	
('INV' , 0xe000 , abc      ),	
('XOR' , 0xf000 , abc      ),
('EOR' , 0xf000 , abc      ),
#pseudo statement:
('ADR' , 0x0000 , addr     ),
('JMP' , 0x0000 , myjmp    ),
('JPL' , 0x0000 , Raddress),	# BPL ref
('JMI' , 0x1000 , Raddress),	# BMI ref
('JEQ' , 0x2000 , Raddress),	# BEQ ref
('JNE' , 0x3000 , Raddress),	# BNE ref
('load' , 0x0000 , myloadstore   ),
('store' , 0x0000 , myloadstore  ),
('mov' , 0xd000 , abdouble ),
('push' , 0x5000 , abpc),
('pop' , 0x4000 , abpc),

]

single_ops = [
('RTS' , 0xd000 , genrts   )
]
 
	





hex_data = list()



#setup translations
for op,i,parser in statements:
    #print (" op " , op , " is " , i)
    to_ref[op.lower()] = i
    to_parse[op.lower()] = parser
    has_operands[op.lower()] = True

#setup translations
for op,i,parser in single_ops:
    #print (" op " , op , " is " , i)
    to_ref[op.lower()] = i
    to_parse[op.lower()] = parser
    has_operands[op.lower()] = False

special_chars={}
special_chars['n']=10
special_chars['b']=8
special_chars['m']=13
special_chars['l']=12


def add_literal_string(src,wide=False):
  global address
  index=1
  bytes_out=0
  allow_literal = False
  for c in src:
    if ((c=='\\') and (allow_literal==False)):
      allow_literal=True
    else:
      if (((c!='"') and (c!='\\')) or allow_literal):
        bytes_out +=1
        if allow_literal:
          if c in special_chars:
            optprint("%02x" % special_chars[c])
          else:
            optprint("%02x" % (ord(c)))
        else:
          optprint("%02x" % (ord(c)))
        allow_literal = False
        if wide==True:
          bytes_out +=1
          optprint("%02x" % (0))
  # zero terminate
  optprint("%02x" % (0))
  bytes_out +=1
  # padding for 16bit addressing.
  if ((bytes_out & 1)==1):
    optprint("%02x" % (0))
    bytes_out +=1
  address+=(bytes_out>>1)
  wide=False


def output( src ):
  global address
  if src.startswith('w"'):
    add_literal_string(src[1:],True)
  else:
    if src.startswith('"'):
      add_literal_string(src,False)
    else:
      val=0
      if src in labels:
        val = labels[src]
      else:
        try:
          val = int(src,16)
        except:
          val=0
      tmp = "%04x" % val
      optprint("%s\n%s" %(tmp[2:4],tmp[0:2]))
      address+=1


line_no=0

# process .asm file.
for Pass in range(-1,2):
  output_enabled = Pass
  line_no=0
  
  with open(sys.argv[1],"rt") as f:
    try:
      for preline in f:
          line_no+=1
          lline = preline.replace('\n','').lstrip()
          # literal data, hex or label address
          if (lline.startswith('.')):
            optprint("# %s" %lline)
            output(lline[1:])
            continue
          #remove trailing comments, trailing linefeed and convert to lowercase.
          line = preline.replace('\n','').lstrip().rstrip().split("//")[0]
          
  
          
          # comment line
          if (line.startswith("#")):
            optprint(line)
            continue
          
          # programm offset
          if (line.startswith("@")):
            optprint("#Program at: 0x%s" % line[1:])
            optprint(line)
            address = int(line[1:],16)
            continue
          
          # new label
          if (line.endswith(":")):
            labels[line[:-1].lower()]=address
            optprint("#%s: (0x%x)" %( line[:-1].lower() , address))
            continue
          
          # assembler operation.
          ops = line.split(" ")
          if ops[0].lower() in to_parse:
            if has_operands[ops[0].lower()]:
              optprint("# %s %s" % (ops[0].lower(), ops[1]))
              to_parse[ops[0].lower()]( ops[0].lower(),ops[1])
            else:
              optprint("# %s" % ops[0].lower())
              to_parse[ops[0].lower()](ops[0].lower())
          else:
            if ops[0]!='':
              optprint("No such operation: '%s' at line %d" % (ops[0],line_no))
    except Exception as e:
      sys.stderr.write("Parse error at line %d" % line_no)
      sys.stderr.write(str(e))
      sys.exit(0)

