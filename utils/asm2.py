#!/usr/bin/env python3

import fileinput
import sys, traceback

#print ("This is the name of the script: %s" % sys.argv[0] )
#print ("Number of arguments: %d" % len(sys.argv))
#print ("The arguments are: %s" % str(sys.argv))

labels={}
to_ref = {}
to_parse = {}
has_operands = {}
address=0

output_enabled = 0

tohex = [ '0',
'1',
'2',
'3',
'4',
'5',
'6',
'7',
'8',
'9',
'a',
'b',
'c',
'd',
'e',
'f']


def optprint(line):
  global output_enabled
  if (output_enabled==1):
    print(line)

def optout_word(val):
  global output_enabled
  global address
  if (output_enabled==1):
    print("%02x\n%02x" %(val&255,val>>8))
  address+=1

def gen_out(nibbles):
  global address
  optprint('%s%s\n%s%s' %( nibbles[2],nibbles[3],nibbles[0],nibbles[1]))
  address+=1

def to_int16(src):
  if src.startswith('#'):
    src=src[1:]
  tmp=-1
  if (src[0]=='\''):
    tmp = ord(src[1])
  else:
    if (src[0]=='x') or (src[0]=='X') or (src[0]=='&'):
        tmp = int(src[1:],16)
    else:
      if src in labels:
        tmp = labels[src]
      else:
        try:
          tmp = int(src,10) & 0xffff
        except:
          optprint("Error: value not 16-bit compatible.")  

  if (-1 < tmp < 65536):
      return tmp
  else:
    optprint("Error: value not 16-bit compatible.")
    return 0

def to_int8(src):
  if src[0]=='#':
    src=src[1:]
  tmp=-1
  if (src[0]=='\''):
    tmp = ord(src[1])
  else:
    if (src[0]=='x') or (src[0]=='X') or (src[0]=='&'):
        tmp = int(src[1:],16)
    else:
        tmp = int(src,10) &255
  if (-1<tmp<256):
    return tmp
  else:
    print("Error: value not 8-bit compatible.")
    return 0

def to_int4(src):
  if (src=='pc'):
    return 15
  if (src=='lr'):
    return 14
  if (src=='sp'):
    return 13
  if (src=='fp'):
    return 12
  if (src[0]=='r'):
    src=src[1:]
  val = to_int8(src) & 15
  if (-1<val<16):
    return val
  else:
    print("Error: value not 4-bit compatible. / invalid register number")
    return 0

def to_ref12( src ):
  global address
  refaddress = 0
  indirect = 0;
  if src[0]=='@':
    src = src[1:]
    indirect = 2048
  if src in labels:
    refaddress = labels[src] - (address+1)
  else:
     if src[0]=='x' or src[0]=='&':
      refaddress = int(src[1:],16) - (address+1)
     else:
      #refaddress = int(src[1:],10) - (address+1)
      optprint("ERROR: cannot process %s" % src)
  if (refaddress>1023 or refaddress < -1022):
    optprint("ERROR: 12-bit offset out of range.")
    return 0
  if refaddress<0:
    refaddress = 2048+refaddress
  return refaddress + indirect

def to_ref9( src ):
  global address
  refaddress = 0
  indirect = 0;
  if src[0]=='@':
    src = src[1:]
    indirect = 2048
  if src in labels:
    refaddress = labels[src] - (address+1)
  else:
     if src[0]=='x' or src[0]=='&':
      refaddress = int(src[1:],16) - (address+1)
     else:
      #refaddress = int(src[1:],10) - (address+1)
      optprint("ERROR: cannot process %s" % src)
  if (refaddress>255 or refaddress < -256):
    optprint("ERROR: 9-bit offset out of range.")
    #return 0
  refaddress = refaddress & 511;
  return refaddress + indirect

def to_address(src):
  src=src.replace('[','').replace(']','')
  refaddress = 0
  indirect = 0;
  if src in labels:
    refaddress = labels[src]
  else:
     if src[0]=='#':
      refaddress = int(src[1:],10)
     else:
       if src[0]=='&':
         refaddress = int(src[1:],16)
       else:
         optprint("ERROR: cannot convert to address %s" % src)
  return refaddress

def reference(op,dat):
  global address
  refaddress = 0
  items = dat.lower().split(",")
  val = to_ref[op] + to_ref12(items[0])
  optout_word(val)

def Breference(op,dat):
  global address
  refaddress = 0
  items = dat.lower().split(",")
  val = to_ref[op] + to_ref9(items[0])
  optout_word(val)

def Raddress(op,dat):
  val = to_ref[op] + 0x800;
  optout_word(val)
  optout_word(to_address(dat))
  
def RIaddress(op,dat):
  val = to_ref[op] + 0x800;
  optout_word(val)
  optout_word(to_address(dat))

def RPCaddress(op,dat):
  val = to_ref[op] | 0xfff;
  optout_word(val)
  optout_word(to_address(dat))

def abc(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",",2)
  for it in items:
    val += (to_int4(it) << shft)
    shft -= 4
  optout_word(val)

def abcimm(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().split(",",2)
  val += (to_int4(items[0])<<8)
  val += (to_int4(items[1])<<4)
  if (items[2][0]=='#' or items[2][0]=='&'):
    val += 15
    optout_word(val)
    optout_word(to_address(items[2]))
  else:
    val += (to_int4(items[2]))
    optout_word(val)


def abimms(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",",2)
  for it in items:
    val += ((to_int4(it)&15) << shft)
    shft -= 4
  optout_word(val)

  
def aboffset(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().replace('[','').replace(']','').split(",",2)
  dest = to_int4(items[0])
  srcA = to_int4(items[1])
  ref = to_address(items[2])
  offset = to_address(items[2])&0xffff;
  orange = offset >> 3;
  if (orange == 0x1fff or  orange == 0):
    optprint("# single op offset %d" %(ref)) 
    val = val + (dest<<8) + (srcA<<4) + (offset &15);
    optout_word(val)
  else:
    optprint("# extra op offset %d" %(ref)) 
    val = val + (dest<<8) + 0xf0 + srcA;
    optout_word(val)
    optout_word(offset)

def abimmu(op,dat):
  val = to_ref[op]
  shft = 8
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",",2)
  for it in items:
    val += (to_int4(it) << shft)
    shft -= 4
  optout_word(val)

def abdouble(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += (to_int4(items[1]) << 4)
  val += to_int4(items[1])
  optout_word(val)

def abpc(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += (to_int4(items[1]) << 4)
  val += 15
  optout_word(val)

def abzero(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += (to_int4(items[1]) << 4)
  optout_word(val)

def apcpc(op,dat):
  val = to_ref[op]
  items = dat.lower().replace('[','').replace(']','').replace('#','').split(",")
  val += (to_int4(items[0]) << 8)
  val += 255
  optout_word(val)

def lit(op,dat):
  items = dat.split(",",1)
  optprint("# %s " % items[1])
  litval = to_int16(items[1])
  if (litval<255):
    val = to_ref[op] + (to_int4(items[0]) << 8) + litval
    optout_word(val)
  else:
    val = 0x40ff + (to_int4(items[0]) << 8)
    optout_word(val)
    optout_word(litval)

def wlit(op,dat):
  val = to_ref[op]
  items = dat.lower().split(",")
  val += (to_int4(items[0]) << 8)
  val += 255
  optout_word(val)
  optout_word(to_int16(items[1]))

def genrts(op):
  val = to_ref['orr'] + 0xfee
  optout_word(val)

def addr(op,dat):
  # generate multiple instructions to get rdest=address.
  items = dat.lower().split(",",1)
  destreg = to_int4(items[0])
  val = to_ref['ldi'] + 0xff + (destreg << 8)
  optout_word(val)
  val = to_address(items[1])
  optout_word(val)
    
def myjmp(op,dat):
    reference('bmi',dat)
    reference('bpl',dat)

def ldrstr(op,dat):
  global address
  # generate multiple instructions to get rdest=address.
  items = dat.lower().split(",",2)
  ref=to_address(items[1])
  regno = to_int4(items[0])
  if len(items)>2:
    # indexed absolute
    if (op=='load' or op=='ldw'):
      base_op = to_ref['ldi'] 
    else:
      base_op = to_ref['sti']
    base_op = base_op + 0xf0 + to_int4(items[2])
  else:
    # absolute
    if (op=='load' or op=='ldw'):
      base_op = to_ref['ldr']+ 255
    else:
      base_op = to_ref['str'] + 255
  optout_word(base_op+(regno<<8))
  optout_word(ref)

statements = [ 
('BPL' , 0x0000 , Breference), # BPL ref
('BMI' , 0x0200 , Breference), # BMI ref
('BEQ' , 0x0400 , Breference), # BEQ ref
('BNE' , 0x0600 , Breference), # BNE ref

('JSR' , 0x1000 , Raddress  ), # JSR Ra+#offset
('LDR' , 0x2000 , abc       ), # LDR Ra,[Rb,#off]
('STR' , 0x3000 , abc       ), # STR Ra,[Rb,#off]

('LDI' , 0x4000 , aboffset    ), # STI Rd,[Ra,#imm]
('STI' , 0x5000 , aboffset    ), # STI Rd,[Ra,#imm]
('ADI' , 0x6000 , abimms    ), # ADD Rd,Ra,#imm

('LIT' , 0x7000 , lit       ), # LIT Rd,#imm


('ADD' , 0x8000 , abcimm    ), # ADD Rd,Ra,Rb
('SUB' , 0x9000 , abcimm    ), # SUB Rd,Ra,Rb
('AND' , 0xa000 , abcimm    ), # AND Rd,Ra,Rb
('ORR' , 0xb000 , abcimm    ), # ORR Rd,Ra,Rb
('XOR' , 0xc000 , abcimm    ), # XOR Rd,Ra,Rb
('EOR' , 0xc000 , abcimm    ), # EOR Rd,Ra,Rb
('SHL' , 0xd000 , abcimm    ), # SHL Rd,Ra,#imm
('SHR' , 0xe000 , abcimm    ), # SHR Rd,Ra,#imm
('MUL' , 0xf000 , abcimm    ), # MUL Rd,Ra,Rb

#pseudo statement:
('NOT' , 0x6000 , abzero    ), # NOT Rd,Ra
#('JMP' , 0x4fff , RPCaddress), # JMP immediate
('JMP' , 0x1000 , Raddress), # JMP imm16
('JIN' , 0x2fff , RPCaddress), # JMP [imm16]
('MOV' , 0xb000 , abdouble  ), # mov Rd,Ra 
('BYTE', 0x7000 , lit       ), # BYTE Rd,#imm
('WORD', 0x4000 , wlit      ), # WORD Rd,#imm
('ADR' , 0x40ff , addr      ), # adr Rd,dest
('JPL' , 0x0000 , Raddress  ), # BPL ref
('JMI' , 0x0200 , Raddress  ), # BMI ref
('JEQ' , 0x0400 , Raddress  ), # BEQ ref
('JNE' , 0x0600 , Raddress  ), # BNE ref
('load' , 0x40ff, ldrstr    ), # load Rd,[<dest> [,Rb]]
('store', 0x50ff, ldrstr    ), # store Rd,[<dest> [,Rb]]
('LDW' , 0x40ff, ldrstr    ), # load Rd,[<dest> [,Rb]]
('STW',  0x50ff, ldrstr    ), # store Rd,[<dest> [,Rb]]
('push', 0x3000 , abpc      ), # push Rd,Ra 
('psh' , 0x3000 , abpc      ), # psh Rd,Ra
('pop' , 0x2000 , abpc      ), # pop Rd,Ra
]

single_ops = [
('RTS' , 0xd000 , genrts   )
]
 
hex_data = list()

#setup translations
for op,i,parser in statements:
    #print (" op " , op , " is " , i)
    to_ref[op.lower()] = i
    to_parse[op.lower()] = parser
    has_operands[op.lower()] = True

#setup translations
for op,i,parser in single_ops:
    #print (" op " , op , " is " , i)
    to_ref[op.lower()] = i
    to_parse[op.lower()] = parser
    has_operands[op.lower()] = False

special_chars={}
special_chars['n']=10
special_chars['b']=8
special_chars['m']=13
special_chars['l']=12


def add_literal_string(src,wide=False):
  global address
  index=1
  bytes_out=0
  allow_literal = False
  for c in src:
    if ((c=='\\') and (allow_literal==False)):
      allow_literal=True
    else:
      if (((c!='"') and (c!='\\')) or allow_literal):
        bytes_out +=1
        if allow_literal:
          if c in special_chars:
            optprint("%02x" % special_chars[c])
          else:
            optprint("%02x" % (ord(c)))
        else:
          optprint("%02x" % (ord(c)))
        allow_literal = False
        if wide==True:
          bytes_out +=1
          optprint("%02x" % (0))
  # zero terminate
  optprint("%02x" % (0))
  bytes_out +=1
  # padding for 16bit addressing.
  if ((bytes_out & 1)==1):
    optprint("%02x" % (0))
    bytes_out +=1
  address+=(bytes_out>>1)
  wide=False


def output( src ):
  global address
  if src.startswith('w"'):
    add_literal_string(src[1:],True)
  else:
    if src.startswith('"'):
      add_literal_string(src,False)
    else:
      val=0
      if src in labels:
        val = labels[src]
      else:
        try:
          val = int(src,16)
        except:
          val=0
      tmp = "%04x" % val
      optprint("%s\n%s" %(tmp[2:4],tmp[0:2]))
      address+=1


line_no=0

# process .asm file.
for Pass in range(-1,2):
  output_enabled = Pass
  line_no=0
  
  with open(sys.argv[1],"rt") as f:
    try:
      for preline in f:
          line_no+=1
          lline = preline.replace('\n','').lstrip()
          # literal data, hex or label address
          if (lline.startswith('.')):
            optprint("# %s" %lline)
            output(lline[1:])
            continue
          #remove trailing comments, trailing linefeed and convert to lowercase.
          Parts = preline.replace('\n','').lstrip().rstrip().split("//")
          line = Parts[0]

          if (len(Parts)>1):
            optprint("# %s" %(Parts[1]))

          # comment line
          if (line.startswith("#")):
            optprint(line)
            continue
          
          # programm offset
          if (line.startswith("@")):
            optprint("#Program at: 0x%s" % line[1:])
            optprint(line)
            address = int(line[1:],16)
            continue
          
          # new label
          if (line.endswith(":")):
            labels[line[:-1]]=address
            optprint("#%s: (0x%x)" %( line[:-1] , address))
            continue
          
          if (line.count('=') > 0 and line.count(',')==0):
            defs = line.lower().split('=')
            labels[defs[0]]=to_int16(defs[1])
            optprint("#%s: (0x%x)" %( defs[0] ,labels[defs[0]]))
            continue
          
          # assembler operation.
          ops = line.split(" ")
          if ops[0].lower() in to_parse:
            if has_operands[ops[0].lower()]:
              optprint("# %s %s" % (ops[0].lower(), ops[1]))
              to_parse[ops[0].lower()]( ops[0].lower(),ops[1])
            else:
              optprint("# %s" % ops[0].lower())
              to_parse[ops[0].lower()](ops[0].lower())
          else:
            if ops[0]!='':
              optprint("No such operation: '%s' at line %d" % (ops[0],line_no))
    except Exception as e:
      exc_type, exc_value, exc_traceback = sys.exc_info()
      sys.stderr.write("Parse error at line %d" % line_no)
      sys.stderr.write(str(e))
      traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
      sys.exit(0)

