PROJ = 16bitAtom
DEVICE = hx8k
BOARD = icoboard
PIN_DEF = $(BOARD).pcf
END_SPEED = 33
FOOTPRINT = ct256
SEED=3537404793
# \3547801813 Total number of logic levels: 15 Total path delay: 31.66 ns (31.58 MHz)-abc2
# \2510111486 Total number of logic levels: 19 Total path delay: 32.24 ns (31.02 MHz)-abc2
# \4230664 Total number of logic levels: 17 Total path delay: 29.49 ns (33.91 MHz)-abc2
# \2627447371 Total number of logic levels: 13 Total path delay: 31.79 ns (31.46 MHz)-abc2

# 3537404793 Total number of logic levels: 24 Total path delay: 31.11 ns (32.15 MHz)-abc2

# 238522875 Total number of logic levels: 25 Total path delay: 34.28 ns (29.17 MHz)
# 2251395291 Total number of logic levels: 25 Total path delay: 33.21 ns (30.11 MHz) -retime

all: $(PROJ).rpt $(PROJ).bin

%.blif: %.v  cpu2.v
	#yosys -p 'synth_ice40 -retime -abc2 -top top -blif $@' $< > YOSYS.LOG
	yosys -p 'synth_ice40 -retime -abc2 -top top -blif $@' $< |grep arning
	#grep arning YOSYS.LOG

%.asc: $(PIN_DEF) $(PROJ).blif
	#arachne-pnr -s $(SEED) -d $(subst hx,,$(subst lp,,$(DEVICE))) -P $(FOOTPRINT) -o $@ -p $^ > ARACHNE.LOG
	arachne-pnr -s 2495655555 -d $(subst hx,,$(subst lp,,$(DEVICE))) -P $(FOOTPRINT) -o $@ -p $^ > ARACHNE.LOG
	cat ARACHNE.LOG

%.bin: %.asc
	icepack $< $@

%.rpt: %.asc
	icetime -d $(DEVICE) -P $(FOOTPRINT) -p $(PIN_DEF) -tm $<

rewire:
	rm -f $(PROJ).asc $(PROJ).rpt $(PROJ).bin
	arachne-pnr -r -d $(subst hx,,$(subst lp,,$(DEVICE))) -P $(FOOTPRINT) -o $(PROJ).asc -p $(PIN_DEF) $(PROJ).blif
	make

wire:
	rm -f $(PROJ).asc $(PROJ).rpt $(PROJ).bin
	make

bootimage.bin: boot.pasm splashscreen.hex
	echo "@8000" > boot.hex
	cat splashscreen.hex >> boot.hex
	./utils/asm2.py ./boot.pasm >> boot.hex
	./utils/hex2bin.py ./boot.hex ./bootimage.bin

bin/cc16:
	make -C compiler

bootrom.bin:./AtomOS/AtomOS.sco bootrom.asm bin/cc16
	bin/cc16 ./AtomOS/AtomOS.sco > ./AtomOS/AtomOS.asm
	cat bootrom.asm | gcc -E - | grep -v '^ *$$' > bootrom.pasm
	utils/asm2.py bootrom.pasm > bootrom.hex
	./utils/hex2bin.py bootrom.hex ./bootrom.bin

boot.pasm: boot.asm atom_io.asm lib.asm basic.asm string.asm stdio.asm stdlib.asm graphics.asm
	cat boot.asm | gcc -E - > boot.pasm

prog: $(PROJ).bin
	iceprog $<

icoboard: $(PROJ).bin $(PROJ).rpt bootrom.bin
	icoprog -f < $(PROJ).bin
	icoprog -O 4 -f < bootrom.bin
	icoprog -b

flash:
	icoprog -f < $(PROJ).bin
	icoprog -O 4 -f < bootrom.bin
	icoprog -b

sudo-prog: $(PROJ).bin
	@echo 'Executing prog as root!!!'
	sudo iceprog $<

clean:
	rm -f $(PROJ).blif $(PROJ).asc $(PROJ).rpt $(PROJ).bin bootimage.bin boot.pasm boot.hex *.LOG

.SECONDARY:
.PHONY: all prog clean
