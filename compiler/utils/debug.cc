/*
 * debug.cc
 * 
 * Copyright 2018  <janrinze@tegrabook>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <string>
#include <iostream>

#include "debug.h"

static int line_num;
static std::string Sfilename;
std::stack<fileref_t> debug_context::filerefs;
debug_context::debug_context(std::string filename){
  fileref_t ref;
  ref.line_num=line_num;
  ref.Sfilename=Sfilename;
  filerefs.push(ref);
  Sfilename=filename;
  line_num=1;
}
debug_context::~debug_context(){
  if (!filerefs.empty()){
    line_num=filerefs.top().line_num;
    Sfilename=filerefs.top().Sfilename;
    filerefs.pop();
  }
}

std::string deb_get_file_name(){
  return Sfilename;
}
int deb_get_file_line_num(){
  return line_num;
}
void debug_advance_line(){
  line_num++;
}
void debout (std::string n,std::string r) {
   //std::cerr << "# DEBUG:" << n << r << " at line " << line_num << " in file "<< Sfilename << "\n";
}

void debout (std::string n) {
   //std::cerr << "# DEBUG:" << n << " at line " << line_num << " in file "<< Sfilename << "\n";
}

void err_report (std::string n,std::string r) {
    //print_stacktrace();
    std::cerr << "ERROR: " << n << r << " at line " << line_num << " in file "<< Sfilename << "\n";
    //dump();
    exit (1);
}

void err_report (std::string n) {
    //print_stacktrace();
    std::cerr << "ERROR: " << n << " at line " << line_num << " in file "<< Sfilename << "\n";
    //dump();
    exit (1);
}

void warning_report (std::string n) {
    std::cerr << "Warning: " << n << " at line " << line_num << " in file "<< Sfilename << "\n";
}

void warning_report (std::string n,std::string r) {
    std::cerr << "Warning: " << n << r << " at line " << line_num << " in file "<< Sfilename << "\n";
}


