#define DECLARE_ENUM(ename, ...) \
    enum ename { __VA_ARGS__, MAX_NUMBER_OF_##ename }; \
    static std::string ename##Strings[MAX_NUMBER_OF_##ename]; \
    static const char* ename##ToString(ename e) { \
        if (ename##Strings[0].empty()) { SplitEnumArgs(#__VA_ARGS__, ename##Strings, MAX_NUMBER_OF_##ename); } \
        return ename##Strings[e].c_str(); \
    } \
    static ename StringTo##ename(const char* szEnum) { \
        for (int i = 0; i < MAX_NUMBER_OF_##ename; i++) { if (ename##Strings[i] == szEnum) { return (ename)i; } } \
        return MAX_NUMBER_OF_##ename; \
    }
