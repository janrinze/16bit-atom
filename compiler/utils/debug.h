/*
 * debug.h
 * 
 * Copyright 2018  <janrinze@tegrabook>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEBUG_H
#define DEBUG_H

#include <string>
#include <stack>

typedef struct {
  int line_num;
  std::string Sfilename;
}  fileref_t;


class debug_context {
  private:
  static std::stack<fileref_t> filerefs;
  public:
    debug_context(std::string filename);
    ~debug_context();
};

std::string deb_get_file_name();
int deb_get_file_line_num();
void debug_advance_line();
void debout (std::string n,std::string r);
void debout (std::string n);
void err_report (std::string n,std::string r);
void err_report (std::string n);
void warning_report (std::string n);
void warning_report (std::string n,std::string r);

#endif /* DEBUG_H */ 
