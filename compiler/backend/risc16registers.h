#include <string>
#include <list>

typedef enum {
    UNUSED_REG,
    FREE_REG,
    EVAL_REG,
    TMP_REG,
} regstate_t;

typedef enum {
    REG_RETURN,
    REG_PC,
    REG_RESULT,
    REG_ARGS,
    REG_STACK,
    REG_THIS,
    REG_GENERIC,
} regtype_t;

typedef struct {
  regtype_t type;
  std::string name;
} regdef_t;

typedef struct {
    regstate_t state;
    bool used;
    class Object * ref;
} alloc_state;


unsigned int num_regs();
std::string & reg_name(int r);
std::string & reg_name(regtype_t type);
unsigned int reg_num(std::string &r);
unsigned int reg_num(regtype_t type);
std::list<std::string> reg_names_for_type(regtype_t type);
