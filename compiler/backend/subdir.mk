################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../backend/risc16registers.cc \
../backend/risc16inst.cc


OBJS += \
./backend/risc16registers.o \
./backend/risc16inst.o

CC_DEPS += 

# Each subdirectory must supply rules for building sources it contributes

backend/%.o: ./backend/%.cc 
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ ${CC_OPTIMIZE} -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
