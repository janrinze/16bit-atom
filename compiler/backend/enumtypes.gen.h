/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */
/* We do not support C11 <threads.h>.  */
enum ops { OP_LABEL, OP_ASM, OP_ALLOCATE, OP_BLOCK, OP_GOTO, OP_BRA_EQ, OP_BRA_NE, OP_ASSIGN, OP_FETCH, OP_VASSIGN, OP_SASSIGN, OP_CALL, OP_PUSH, OP_POP, OP_EVAL, OP_REF, OP_VREF, OP_NEST_IN, OP_NEST_OUT, OP_ADD, OP_SUB, OP_MUL, OP_DIV, OP_AND, OP_ORR, OP_XOR, OP_INCR, OP_DECR, OP_SHLEFT, OP_SHRIGHT, OP_GREATER, OP_SMALLER, OP_EQUAL, OP_GREQUAL, OP_SMEQUAL, OP_NOTEQUAL, OP_ENTER, OP_RETURN, MAX_NUMBER_OF_ops };
static std::string opsStrings[MAX_NUMBER_OF_ops];
static const char* opsToString(ops e) {
    if (opsStrings[0].empty()) {
        SplitEnumArgs("OP_LABEL, OP_ASM, OP_ALLOCATE, OP_BLOCK, OP_GOTO, OP_BRA_EQ, OP_BRA_NE, OP_ASSIGN, OP_FETCH, OP_VASSIGN, OP_SASSIGN, OP_CALL, OP_PUSH, OP_POP, OP_EVAL, OP_REF, OP_VREF, OP_NEST_IN, OP_NEST_OUT, OP_ADD, OP_SUB, OP_MUL, OP_DIV, OP_AND, OP_ORR, OP_XOR, OP_INCR, OP_DECR, OP_SHLEFT, OP_SHRIGHT, OP_GREATER, OP_SMALLER, OP_EQUAL, OP_GREQUAL, OP_SMEQUAL, OP_NOTEQUAL, OP_ENTER, OP_RETURN", opsStrings, MAX_NUMBER_OF_ops);
    }
    return opsStrings[e].c_str();
}
static ops StringToops(const char* szEnum) {
    for (int i = 0; i < MAX_NUMBER_OF_ops; i++) {
        if (opsStrings[i] == szEnum) {
            return (ops)i;
        }
    }
    return MAX_NUMBER_OF_ops;
}
enum reftype { REF_UNUSED , REF_ABSOLUTE, REF_INDIRECT, REF_LABEL, REF_STACK, REF_ARGS, REF_S_INDIRECT, REF_A_INDIRECT, REF_R_INDIRECT, REF_HEAP_OFFSET, REF_HEAP_ADDRESS, REF_CLASS_OFFSET, REF_CLASS_OFFS_INDIRECT, REF_REGISTER, REF_IMMEDIATE, MAX_NUMBER_OF_reftype };
static std::string reftypeStrings[MAX_NUMBER_OF_reftype];
static const char* reftypeToString(reftype e) {
    if (reftypeStrings[0].empty()) {
        SplitEnumArgs("REF_UNUSED , REF_ABSOLUTE, REF_INDIRECT, REF_LABEL, REF_STACK, REF_ARGS, REF_S_INDIRECT, REF_A_INDIRECT, REF_R_INDIRECT, REF_HEAP_OFFSET, REF_HEAP_ADDRESS, REF_CLASS_OFFSET, REF_CLASS_OFFS_INDIRECT, REF_REGISTER, REF_IMMEDIATE", reftypeStrings, MAX_NUMBER_OF_reftype);
    }
    return reftypeStrings[e].c_str();
}
static reftype StringToreftype(const char* szEnum) {
    for (int i = 0; i < MAX_NUMBER_OF_reftype; i++) {
        if (reftypeStrings[i] == szEnum) {
            return (reftype)i;
        }
    }
    return MAX_NUMBER_OF_reftype;
}
