/*
 * codegen.h
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef CODEGEN_H
#define CODEGEN_H
#include <iostream>
#include <string>
#include <sstream>
#include <map>

#include "enumtypes.h"
#include "risc16registers.h"
#include "risc16inst.h"



class Rref_t {
public:
    reftype type;
    void * ref;
    void * Iref;
    Rref_t():type(REF_UNUSED),ref(nullptr),Iref(nullptr){};
    Rref_t(reftype t,void * r):type(t),ref(r),Iref(nullptr){};
    Rref_t(reftype t,void * r,void * i):type(t),ref(r),Iref(i){};
    Rref_t(void*p):type(p==nullptr?REF_UNUSED:REF_ABSOLUTE),ref(p),Iref(nullptr){};
    Rref_t(unsigned int i):type(REF_IMMEDIATE),ref((void*)i),Iref(nullptr){};
};

class inst {
public:
    ops op;
    Rref_t srcA,srcB;
    Rref_t dst;

    unsigned int size; // of the ref..
    std::string comment;
    std::string label;
    std::string to_asm(){ return gen_asm(*this); };
    std::string desc() {
        std::ostringstream o;
        o << "# [ op: "<<opsToString(op) << " ";
        if (dst.type!=REF_UNUSED) o << "dst("<< reftypeToString(dst.type) << " " << dst.ref << ") ";
         if (srcA.type!=REF_UNUSED) o <<  "srcA ("<< reftypeToString(srcA.type) << " " << srcA.ref << ") ";
         if (srcB.type!=REF_UNUSED) o << "srcB ("<< reftypeToString(srcB.type) << " "<< srcB.ref << ") ";
        o << "size:" << size << " ]\n";
        o << "# [ " << comment << " ]";
        o << "# [ label: " << label << " ]";
        o << to_asm();
        return o.str();
    };
};



class Codegen
{

public:
    Codegen(inst * instructions) {};
    friend std::ostream & operator << (std::ostream & stream,Codegen obj);

private:

    std::map<std::string,std::string> blocks;
};


#endif /* CODEGEN_H */
