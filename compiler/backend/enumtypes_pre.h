
#include "../utils/enum_macro.h"

DECLARE_ENUM(ops, \
             OP_LABEL, \
             OP_ASM, \
             OP_ALLOCATE, \
             OP_BLOCK, \
             OP_GOTO, \
             OP_BRA_EQ, \
             OP_BRA_NE, \
             OP_ASSIGN, \
             OP_FETCH, \
             OP_VASSIGN,  \
             OP_SASSIGN, \
             OP_CALL, \
             OP_PUSH, \
             OP_POP, \
             OP_EVAL, \
             OP_REF, \
             OP_VREF, \
             OP_NEST_IN, \
             OP_NEST_OUT, \
             OP_ADD, \
             OP_SUB, \
             OP_MUL, \
             OP_DIV, \
             OP_AND, \
             OP_ORR, \
             OP_XOR, \
             OP_INCR, \
             OP_DECR, \
             OP_SHLEFT, \
             OP_SHRIGHT, \
             OP_GREATER, \
             OP_SMALLER, \
             OP_EQUAL, \
             OP_GREQUAL, \
             OP_SMEQUAL, \
             OP_NOTEQUAL, \
             OP_ENTER, \
             OP_RETURN \
            )

DECLARE_ENUM(reftype,  \
             REF_UNUSED , \
             REF_ABSOLUTE, \
             REF_INDIRECT, \
             REF_LABEL, \
             REF_STACK, \
             REF_ARGS, \
             REF_S_INDIRECT, \
             REF_A_INDIRECT, \
             REF_R_INDIRECT, \
             REF_HEAP_OFFSET, \
             REF_HEAP_ADDRESS, \
             REF_CLASS_OFFSET, \
             REF_CLASS_OFFS_INDIRECT, \
             REF_REGISTER, \
             REF_IMMEDIATE \
            )
