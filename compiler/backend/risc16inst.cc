#include <iostream>
#include <string>
#include <sstream>

#include "risc16inst.h"
#include "codegen.h"

static std::string to_ref(Rref_t t) {
    std::ostringstream o;
    switch (t.type) {
    case REF_UNUSED:
        break;
    case REF_REGISTER:
        o<< "r" <<(unsigned int)t.ref ;
        break;
    case REF_STACK:
        o<< "[r13,#" <<(unsigned int)t.ref <<"]";
        break;
    case REF_A_INDIRECT:
    case REF_ARGS:
        o<< "[r11,#" <<0x10000-(unsigned int)(t.ref) <<"]";
        break;
    case REF_CLASS_OFFSET:
        o<< "[r12,#"<<(unsigned int)t.ref <<"]" ;
        break;
    case REF_IMMEDIATE:
        o<< "#"<<(((unsigned int)t.ref)&0xffff);
        break;
    case REF_ABSOLUTE:
        o<< "[r" <<(unsigned int)t.ref <<",#0]";
        break;
    case REF_HEAP_ADDRESS:
        o<< "[#" <<(unsigned int)t.ref <<"]";
        break;

        
    default:
        o<< "ERROR: unimplemented reference.";
        break;
    }
    return o.str();
}
static int to_RDIU(Rref_t t) {
    int o;
    switch (t.type) {
    case REF_UNUSED:
        o='U';
        break;
    case REF_REGISTER:
        o='R';
        break;
    case REF_STACK:
        o='I';
        break;
    case REF_ARGS:
        o ='I';
        break;
    case REF_CLASS_OFFSET:
        o='I';
        break;
    case REF_IMMEDIATE:
        o='D';
        break;
    case REF_LABEL:
        o='L';
        break;
    case REF_ABSOLUTE:
        o='I';
        break;
    case REF_HEAP_ADDRESS:
        o='H';
        break;
    case REF_A_INDIRECT:
        o='A';
        break;
    default:
        o='?';
        break;
    }
    return o;
}

static bool indirect(Rref_t t) {
    switch (t.type) {
    case REF_UNUSED:
    case REF_REGISTER:
    case REF_IMMEDIATE:
        return false;
    default:
        break;
    }
    return true;
}
static bool immediate(Rref_t t) {
    switch (t.type) {
    case REF_IMMEDIATE:
        return true;
    default:
        break;
    }
    return false;
}

std::string gen_asm(class inst & i) {
    std::ostringstream o;
    std::string dst=to_ref(i.dst);
    std::string srcA=to_ref(i.srcA);
    std::string srcB=to_ref(i.srcB);
    bool indirect_dst = indirect(i.dst);
    bool indirect_src = indirect(i.srcA)||indirect(i.srcB);
    int matrix = (to_RDIU(i.dst)<<16) + (to_RDIU(i.srcA)<<8) + to_RDIU(i.srcB);
    o <<"\n";
    switch (i.op) {
    case OP_LABEL:
        o << i.label << ": ";
        break;
    case OP_ASM:
        o << i.label << "\n";
        break;
    case OP_GOTO:
        o << " JMP " << i.label ;
        break;
    case OP_PUSH:
        o << " PUSH "<< dst << "," << srcA ;
        break;
    case OP_POP:
        o << " POP "<< dst << "," << srcA  ;
        break;
    case OP_ASSIGN:
        switch (matrix) {
        case 'RRU' :
            o << " ORR " << dst << "," <<srcA <<"," <<srcA;
            break;
        case 'RLU' :
            srcA=i.label;
        case 'RDU' :
            o << " LIT " << dst << "," <<srcA ;
            break;
        case 'RIU' :
            o << " LDI " << dst << "," <<srcA ;
            break;
        case 'RHU' :
            o << " LDW " << dst << "," <<srcA ;
            break;
        case 'HRU' :
            o << " STW "<< srcA  << ","  << dst;
            break;
        case 'IRU' :
            o << " STI "<< srcA  << ","  << dst;
            break;
        case 'RAU':
            o << " LDI " << dst << "," <<srcA << "\n";
            o << " LDI " << dst << ",[" << dst << ",#" << (unsigned int)i.srcA.Iref<< "]";
            break;
        case 'IDU':
            o << " LIT r0," << srcA << "\n";
            o << " STI r0," << dst;
            break;
        case 'HDU':
            o << " LIT r0," << srcA << "\n";
            o << " STW r0," << dst;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_FETCH:
        switch (matrix) {
        case 'RRR' :
            o << " LDR " << dst << ",[" <<srcA <<"," << srcB<< "]";
            break;
        case 'RRD' :
            o << " LDI " << dst << ",[" <<srcA <<"," << srcB<< "]";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_CALL:
        o << " JSR " << i.label ;
        break;
    case OP_BRA_EQ :
        o << " BEQ " << i.label ;
        break;
    case OP_BRA_NE :
        o << " BNE " << i.label ;
        break;
    case OP_ADD:
        switch (matrix) {
        case 'RRR' :o << " ADD " << dst << "," <<srcA <<"," << srcB;
            break;
        case 'RRD' :
              {
                unsigned int tmp=((unsigned int)i.srcB.ref)&0xffff;
                if (tmp==0) o << " ORR " << dst << "," <<srcA <<"," << srcA;
                else {
                    if ((tmp!=0)&&((tmp<8)||(tmp>(0x10000-8)))) o << " ADI " << dst << "," <<srcA <<"," << srcB;
                    else  o << " ADD " << dst << "," <<srcA <<"," << srcB;
                }
              }
              break;
            
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_SUB :
        switch (matrix) {
        case 'RRR' :o << " SUB " << dst << "," <<srcA <<"," << srcB;
                    break;
        case 'RRD' :
            {
              unsigned int tmp=(0x10000-(((unsigned int)i.srcB.ref)&0xffff))&0xffff;
              if (tmp==0) o << " ORR " << dst << "," <<srcA <<"," << srcA;
              else {
                  if ((tmp<8)||(tmp>(0x10000-8))) o << " ADI " << dst << "," <<srcA <<",#" << tmp;
                  else o << " SUB " << dst << "," <<srcA <<"," << srcB;
              
              }
            }
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_SHLEFT:
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " SHL " << dst << "," <<srcA <<"," << srcB;
            break;
        case 'RRI' :
            o << " SHL " << dst << "," <<srcA <<"," << srcB;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_SHRIGHT:
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " SHR " << dst << "," <<srcA <<"," << srcB;
            break;
        case 'RRI' :
            o << " SHR " << dst << "," <<srcA <<"," << srcB;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_AND :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
             o << " AND " << dst << "," <<srcA <<"," << srcB;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_ORR :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
             o << " ORR " << dst << "," <<srcA <<"," << srcB;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_XOR :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " EOR " << dst << "," <<srcA <<"," << srcB;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;

    case OP_MUL:
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " MUL r15," << srcA << "," << srcB << "\n";
            o << " ORR r0,r0,r0\n";
            o << " ORR r0,r0,r0\n";
            o << " MUL " << dst << ",r0,r0";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_DIV:
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " MUL r15," << srcA << "," << srcB << "\n";
            o << " ORR r0,r0,r0\n";
            o << " ORR r0,r0,r0\n";
            o << " ORR r0,r0,r0\n";
            o << " MUL " << dst << ",r1,r0";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
        case OP_EQUAL :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " EOR " << dst << "," <<srcA <<"," << srcB << "\n";
            o << " NOT " << dst << "," << dst ;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_NOTEQUAL :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " EOR " << dst << "," <<srcA <<"," << srcB << "\n";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_SMALLER :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " SUB " << dst << "," << srcA <<"," <<srcB << "\n";
            o << " SHR " << dst << "," << dst << ",#15";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
     case OP_GREATER :
        switch (matrix) {
        case 'RRR' :
            o << " SUB " << dst << "," <<srcB << "," << srcA <<"\n";
            o << " SHR " << dst << "," << dst << ",#15";
            break;
        case 'RRD' :
            o << " LIT r0," << srcB <<"\n";
            o << " SUB " << dst << ",r0," << srcA <<"\n";
            o << " SHR " << dst << "," << dst << ",#15";
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
    case OP_GREQUAL :
        switch (matrix) {
        case 'RRR' :
        case 'RRD' :
            o << " SUB " << dst << "," << srcA <<"," <<srcB << "\n";
            o << " SHR " << dst << "," << dst << ",#15"<< "\n";
            o << " NOT " << dst << "," << dst ;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
     case OP_SMEQUAL :
        switch (matrix) {
        case 'RRR' :

            o << " SUB " << dst << "," <<srcB << "," << srcA <<"\n";
            o << " SHR " << dst << "," << dst << ",#15"<< "\n";
            o << " NOT " << dst << "," << dst ;
            break;
        case 'RRD' :
            o << " LIT r0," << srcB <<"\n";
            o << " SUB " << dst << ",r0," << srcA <<"\n";
            o << " SHR " << dst << "," << dst << ",#15"<< "\n";
            o << " NOT " << dst << "," << dst ;
            break;
        default:
            std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
        } ;
        break;
   default:
        std::cerr << "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
            exit(1);
            break;
    };
    
    o <<"\n";if (i.comment!="") o<< "// " << i.comment<<"\n";
    //if (o.str()=="") o<< "ERROR: unimplemented " << opsToString(i.op) << " " << dst << "," << srcA << "," << srcB  <<"\n";
    return o.str();
}


