#include "risc16registers.h"
#include <string>
#include <list>

// ABI definition

static regdef_t regs[] = { 
  {REG_RESULT,"_R0"},
  {REG_GENERIC,"_R1"},
  {REG_GENERIC,"_R2"},
  {REG_GENERIC,"_R3"},
  {REG_GENERIC,"_R4"},
  {REG_GENERIC,"_R5"},
  {REG_GENERIC,"_R6"},
  {REG_GENERIC,"_R7"},
  {REG_GENERIC,"_R8"},
  {REG_GENERIC,"_R9"},
  {REG_GENERIC,"_R10"},
  {REG_ARGS,"_ARGS"},
  {REG_THIS,"this"},
  {REG_STACK,"_SP"},
  {REG_RETURN,"_LR"},
  {REG_PC,"_PC"}
};
  
unsigned int num_regs() {
    return 16;
}
std::string & reg_name(int r) {
    return regs[r].name;
}
static std::string not_found = "NOTFOUND";

std::string & reg_name(regtype_t type) {
    for (auto &t :regs) 
      if (t.type==type) return t.name;
    return not_found;
}

unsigned int reg_num(std::string &r){
  int ret=0;
  for (auto &t :regs) {
    if (t.name==r) return ret;
    ret++;
  }
  return -1;
}
unsigned int reg_num(regtype_t type){
  int ret=0;
  for (auto &t :regs) {
    if (t.type==type) return ret;
    ret++;
  }
  return -1;
}

std::list<std::string> reg_names_for_type(regtype_t type){
  std::list<std::string> l;
  for (auto &t :regs) {
    if (t.type==type) l.push_back(t.name);
  }
  return l;
}
