/*
 * cc16 my compiler.
 */

#include <string>
#include <iostream>

#include "frontend/parser.h"
#include "backend/codegen.h"

using namespace std;

int main(int argc,char **argv)
{
    if (argc<2) {
        cout<< "Usage :" << argv[0] << " <file> \n";
        return 1;
    }

    Parser parser(argv[1]);

    parser.dump();

    //cerr << parser.desc();

    return 0;
}
