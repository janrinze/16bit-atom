/*
 * method.h
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef METHOD_H
#define METHOD_H
#include <map>
#include <list>
#include <vector>
#include <string>
#include <stack>
#include "../backend/codegen.h"
#include "object.h"



typedef struct {
  std::map<string,class Object*> objects;
  unsigned int stack_size=0;
} scope_t;

class Method
{
  private:
  unsigned int stack_size=0;
  unsigned int max_stack_size=0;
  unsigned int add_size(int s);
public:
    Method();
    char * address; // function pointer, all functions are absolute.
    std::string name;
    bool is_proto=false;
    unsigned int litpool_size=0;
    unsigned int code_size=0;
    
    unsigned int args_size=0;
    unsigned int first_arg_size=0;
    class_type return_type;
    reftype rtype;
    class Class * return_class;
    class Class * parent_class;
    int is_pointer; // 0 is none 1 is pointer 2 is pointer to pointer etc. 256*n is n*ref
    std::map<string,class Object*> objects;
    std::vector<scope_t> objectsstack;
    std::list<std::string> inputs;
    std::list<inst> instructions;
    alloc_state availableRegs[64];
    int avail_count=0;
    int inusecount=0;
    //std::list<alloc_state> availableRegs;
    std::list<string> literals;

    unsigned int get_stack_size();
    std::string typedesc();
    std::string desc();
    std::string code();
    void add_inst(inst op);
    void prepend_inst(inst op);
    void add_brancheq(const std::string &comment,const std::string &label);
    void add_branchNE(const std::string &comment,const std::string &label);
    void add_label(const std::string &comment,const std::string &label);

    void add_jmp(const std::string &comment,const std::string &label);
    void add_inst_set_result(const std::string &comment,const std::string &obname);
    void add_asm(const std::string &comment,const std::string &label);

    void replace_last_inst(const inst & op);
    
    Object * get_available_reg(regstate_t t);
    Object * get_highest_available_reg(regstate_t t);
    
    void release_all_eval_regs();
    void release_ALL_regs();
    void add_available_reg(Object * r);
    void release_reg(Object * r);
    Object * has_member(string s);
    unsigned int add_literal(string s);
    unsigned int add_args_size(int s);
    void scope_in();
    void scope_out();
    void scope_all_out();
    unsigned int add_local_object(string & name,Object * o);
    bool verify_stack(std::stack<Object *> & arguments);

    /* add your private declarations */
};

#endif /* METHOD_H */
