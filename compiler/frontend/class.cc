/*
 * class.cc
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "class.h"

Class::Class()
{

}

string Class::desc() {
    std::ostringstream o;
    std::string prefix;
    if (name!="") prefix+=name+".";
    o << "# {\n";
    for (auto l:classes)
        o << "# class " << l.first << "[" << l.second->size << "bytes]\n"  << l.second->desc();
    for (auto j:objects)
        o<< "# " <<  j.second->typedesc() << " " << j.first << " (" << j.second->get_size() << "@" << j.second->address << ")\n" ;
    for (auto k:methods)
        o << prefix<< k.first << ":\n# "<< k.second->typedesc() << k.first << k.second->desc();
    o << "# }\n";
    return o.str();
}

string Class::code() {
    std::ostringstream o;
    std::string prefix;
    if (name!="") prefix+=name+".";
    for (auto l:classes)
        o << l.second->code();
    
    for (auto k:methods)
        o << prefix<< k.first << ":\n// "<< k.first << k.second->code();
    o << "\n";
    return o.str();
}
Object * Class::has_member(string s)
{
    if (objects.find(s)!=objects.end()) return objects[s];
    return nullptr;
}

Method * Class::has_method(string s)
{
    if (methods.find(s)!=methods.end()) return methods[s];
    return nullptr;
}

Class * Class::has_class(string s)
{
    if (classes.find(s)!=classes.end()) return classes[s];
    return nullptr;
}
unsigned int Class::add_size(unsigned int s)
{
    unsigned int t = size;
    size+=s;
    return t;
}

unsigned int Class::add_member(std::string name , Object*o){
  if(o) {
      objects[name]=o;
      o->cparent=this;
      if (o->classref ) {
          int size = o->get_size();//((o->is_pointer&255)==0)? o->classref->size : 1;
          o->rtype=REF_CLASS_OFFSET;
          return o->address=add_size(size);
      }
  }
  return 0;
}
