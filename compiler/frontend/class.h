/*
 * class.h
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef CLASS_H
#define CLASS_H

#include "object.h"
#include "method.h"

class Class
{
private:
    unsigned int add_size(unsigned int s);
public:
    Class();
    char * address;
    unsigned int size=0;
    std::string name;

    std::map<string,Method*> methods;
    std::map<string,Object*> objects;
    std::map<string,Class*>  classes;

    std::string desc();
    std::string code();
    unsigned int add_member(std::string name , Object*o);
    
    Method * has_method(string s);
    Object * has_member(string s);
    Class * has_class(string s);

private:
    /* add your private declarations */
};

#endif /* CLASS_H */
