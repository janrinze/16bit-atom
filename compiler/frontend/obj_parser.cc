/*
 * parser.cc
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "parser.h"

#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <queue>

using namespace std;

#define LINE_SIZE 1024

#define Perror( ... ) { printf(__VA_ARGS__);}

Parser::Parser(string _filename)
{

    filename = _filename;
    // ope the source file

    source.open(filename.c_str(), ios_base::in);
    if ( source.fail() )
    {
        cout << "An error has occurred whilst opening "<< filename << endl;
        exit(0);
    }
    tkparse = new token_parser(source);
    tkparse->parse_tokens();
    pstate.push(IDLE);
    token_queue tokens ;
    for (auto item :tkparse->token_list) if (item->type()!=t_whitespace) tokens.push(item);
    while (tokens.size()>0) parse(tokens);
}



bool Parser::parse(token_queue &tokens)
{
    curr_ob=null;
    while (tokens.size()>0)
    {
        base_token * item = tokens.front();
        tokens.pop();
        switch (item->type())
        {
        case t_invalid_token:
            cout << "Error: " << filename << " line " << line_num << "\n";
            break;
        case t_punctuation:
            switch (item->ref())
            {
            case 59: // closing statement
                return 1;
            case 123:
                cout << "starting block\n";
                while (parse(tokens));
                cout << "ended block\n";
                break;
            case 125:
                return 0;
                break;
            default:
                cout << "ops: " << item->val() << "[" << item->ref() << "]\n";
                break;
            } ;
            break;

        case t_symbol:
            if (internal_op(item->val())) {
                do_internal_op(item->val(),tokens);
            } else if (is_class(item->val())) {
                new_object(item->val(),tokens);
            } else if (is_obj(item->val()) {
            obj_reference(item->val(),tokens);
            } else parse_failed(item->val());
                break;
            case t_eol:
                    line_num++;
                    break;
                case t_integer:
                        cout << "Int: " << item->ref() << "[" << item->val() << "]\n";
                             break;
                         case t_literal:
                                 break;
                             case t_const_literal:
                                     break;
                                 case t_whitespace:
                                         break;
                                     case t_eof: return;
                                         default:
                                                 break;
                                        };
    }
    //
    return 1;
}
