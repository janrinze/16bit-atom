/*
 * parser.cc
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <queue>
#include "../backend/codegen.h"
#include "parser.h"
#include "../utils/debug.h"

static accept_t if_statement { nullptr, t_symbol , "if" };
static accept_t then_statement { nullptr, t_symbol , "then" };
static accept_t parenth_open {nullptr, t_punctuation , "("};
static accept_t parenth_close {nullptr, t_punctuation , ")"};
static accept_t brack_open {nullptr, t_punctuation , "{"};
static accept_t sqbrack_open {nullptr, t_punctuation , "["};
static accept_t sqbrack_close {nullptr, t_punctuation , "]"};
static accept_t end_statement {nullptr, t_punctuation , ";"};
static accept_t colon_char {nullptr, t_punctuation , ":"};
static Object Zero;

static Rref_t unused_src { REF_UNUSED,(void *)0};

static inline Rref_t ObSrc( Object * ob) {
    return { ob->rtype,(void *)ob->address,(void *)ob->Iaddress};
}

static inline Rref_t ImmSrc( unsigned int v) {
    return { REF_IMMEDIATE,(void *)v};
}

using namespace std;

#define LINE_SIZE 1024

#define Perror( ... ) { printf(__VA_ARGS__);}


static inline std::string int_to_str (unsigned int n, int len) {
    string result (len--, '0');
    for (int val= (n<0)?-n:n; len>=0&&val!=0; --len,val/=10)
        result[len]='0'+val%10;
    //if (len>=0&&n<0) result[0]='-';
    return result;
}
static std::string generate_tag() {
    std::string ret="tag";
    static unsigned int index;
    ret += int_to_str (index++,6);
    return ret;
}
// keep line number up to date.
base_token * Parser::next_token() {
    if (tokens.size()>0) {
        tokens.pop();
        while ((tokens.size()>1) && tokens.front()->type() == t_eol) {
            debug_advance_line();
            tokens.pop();
        }
        return tokens.front();
    }
    dump();
    err_report ("ran out of tokens");
    return nullptr;
}

bool Parser::is_token(const string &ref){
  base_token * t = tokens.front();
  if (t->val()==ref) return true;
  return false;
}

static unsigned int precedence (unsigned int token) {
    switch (token) {

    case '.' :
        return 100;
        
    case '->' :
        return 90;
        
    case 'xdni':
    case 'indx':
        return 85;
        
    case '++':
    case '--':
        return 84;
        
    case 'llac' :
    case 'call' :
        return 81;
        
    case 'ref': return 75;
    
    case '*' :
    case '/' :
    case '%' :
        return 70;
        
    case '+' :
    case '-' :
        return 50;
        
    case '<<' :
    case '>>' :
        return 40;
        
    case '>' :
    case '<' :
    case '<=' :
    case '>=' :
        return 38;

    case '==' :
    case '!=' :
        return 37;

    case '&' :
        return 36;
    case '^' :
        return 35;
    case '|' :
        return 34;
    case '&&' :
        return 33;

    case '||' :
        return 32;

  
    case '=' :
    case '+=' :
    case '-=' :
    case '*=' :
    case '/=' :
    case '%=' :
        return 30;
        
    case ',' :
        return 25;
    case 'end' :
        return 0;
    default : err_report("precedence not properly established.");
        break;
    }
    return 10;
}

static unsigned int re_interpret(unsigned int token) {
  char temp=token;
  switch (token) {

    case '.' :    break;
    case '->' :   break;
    case 'xdni':  break;
    case 'indx':  break;
    case '++':    break;
    case '--':    break;
    case 'llac' : break;
    case 'call' : break; 
    case '*' :   return 'dref';
    case '/' :    break;
    case '%' :    break;
    case '+' :    break;
    case '-' :    break;
    case '<<' :   break;
    case '>>' :   break;
    case '>' :    break;
    case '<' :    break;
    case '<=' :   break;
    case '>=' :   break;
    case '==' :   break;
    case '!=' :   break;
    case '&' :   return 'ref';
    case '^' :    break;
    case '|' :    break;
    case '&&' :   break;
    case '||' :   break;
    case '=' :    break;
    case '+=' :   break;
    case '-=' :   break;
    case '*=' :   break;
    case '/=' :   break;
    case '%=' :   break;
    case ',' :    break;
    case 'end' :  break;
    default :     break;
    }
  return token;
}
Parser::Parser (string _filename) {
    classes["int"]=new Class;
    classes["int"]->size = 1;
    classes["wchar"]=new Class;
    classes["wchar"]->size = 1;
    classes["char"]=new Class;
    classes["char"]->size = 1;
    classes["register"]=new Class;
    classes["register"]->size = 1;
    classes["void"]=new Class;
    classes["void"]->size = 0;
    objects["__void__"] =new Object;
    objects["__void__"][0]= {
        .address=reg_num(REG_STACK),
        .type="void",
        .rtype=REF_REGISTER,
        .classref=classes["void"]
    };
    // pre defined objects
    for (unsigned int id=0; id<num_regs(); id++) {
        Object * obj=new Object;
        *obj = {
            .address=id,
            .type="int",
            .rtype=REF_REGISTER,
            .classref=classes["int"]
        };
        objects[reg_name(id)] = obj;
    }
    objects["__StackPtr__"] =new Object;
    objects["__StackPtr__"][0]= {
        .address=reg_num(REG_STACK),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"],
    };
    objects["__ProgramCounter__"]=new Object;
    objects["__ProgramCounter__"][0]= {
        .address=reg_num(REG_PC),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"]
    };
    objects["__ArgsPtr__"]=new Object;
    objects["__ArgsPtr__"][0]= {
        .address=reg_num(REG_ARGS),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"],
        .cparent=nullptr,
        .mparent=nullptr
    };
    objects["__ThisPtr__"]=new Object;
    objects["__ThisPtr__"][0]= {
        .address=reg_num(REG_THIS),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"]
    };
    objects["__RetVal__"]=new Object;
    objects["__RetVal__"][0]= {
        .address=reg_num(REG_RESULT),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"]
    };
    objects["__ReturnPtr__"]=new Object;
    objects["__ReturnPtr__"][0]= {
        .address=reg_num(REG_RETURN),
        .type="int",
        .rtype=REF_REGISTER,
        .classref=classes["int"]
    };
    char * staticheap = (char *) 0x4000;
    static Object __Global_Space__ = { .address= (unsigned int)staticheap,
                                       .type="__Global__",.rtype=REF_ABSOLUTE,
                                       .classref= (Class *)this, //classes["__Global__"],
                                       .cparent=nullptr,
                                       .mparent=nullptr ,
                                       .Iaddress=0,
                                       .is_pointer=1
                                     };
    Zero = Object();
    Zero.rtype=REF_IMMEDIATE;
    Zero.classref=classes["int"];
    
    objects["__Global_Space__"] = &__Global_Space__;
    builtin["class"]= (handler)&Parser::add_class;
    builtin["using"]= (handler)&Parser::do_namespace;
    builtin["dump"]= (handler)&Parser::dump;
    builtin["include"]= (handler)&Parser::include;
    builtin["if"]= (handler)&Parser::do_if_then_else;
    builtin["for"]= (handler)&Parser::do_for;
    builtin["while"]= (handler)&Parser::do_while;
    builtin["return"]= (handler)&Parser::do_return;
    builtin["continue"]= (handler)&Parser::do_continue;
    builtin["break"]= (handler)&Parser::do_break;
    builtin["switch"]= (handler)&Parser::do_switch;
    builtin["case"]= (handler)&Parser::do_case;
    builtin["default"]= (handler)&Parser::do_default;
    builtin["asm"]= (handler)&Parser::do_asm;
    
    //builtin["new"]=(handler)&Parser::heap_alloc_obj;
    //builtin["static"]=(handler)&Parser::static_alloc_obj;
    Method * m=new Method;
    methods["__init__"]=m;
    for(std::string reg:reg_names_for_type(REG_GENERIC))
        m->add_available_reg (objects[reg]);
    m->add_inst( { .op=OP_ASSIGN,
      .srcA=ImmSrc(0x7fff-20),
      .srcB=unused_src,
      .dst=ObSrc(objects["__StackPtr__"]),
      .size=1,
      .comment="Set initial stack pointer"});
    m->add_inst( { .op=OP_ASSIGN,
      .srcA=ImmSrc(0x7fff-20),
      .srcB=unused_src,
      .dst=ObSrc(objects["__StackPtr__"]),
      .size=1,
      .comment="Set initial stack pointer"});
    

    
    curr_method.push (methods["__init__"]);
    curr_class.push (this);
    currType.push (CLASS);
    name="__Global__";
    
    filename = _filename;
    debug_context deb(filename);
    // open the source file
    if (filename=="") return;
    fstream source;
    source.open (filename.c_str(), ios_base::in);
    if ( source.fail() ) {
        err_report("An error has occurred whilst opening ",filename);
        exit (0);
    }
    token_parser * tkparse = new token_parser (source);
    tkparse->parse_tokens();
    // simple filter if (item->type()!=t_whitespace && item->type()!=t_eol)
    for (auto item :tkparse->token_list) if (item->type()!=t_whitespace) tokens.push (item);

    while (END_FILE!=parse()) {};

    m->add_inst( { .op=OP_SUB,
      .srcA=ObSrc(objects["__StackPtr__"]),
      .srcB=ImmSrc(has_method("main")->get_stack_size()),
      .dst=ObSrc(objects["__StackPtr__"]),
      .size=1,
      .comment="Allocate local var space",
      });
    m->add_asm("reset regs"," lit r0,#0");
    m->add_asm("reset regs"," lit r1,#0");
    m->add_asm("reset regs"," lit r2,#0");
    m->add_asm("reset regs"," lit r3,#0");
    m->add_asm("reset regs"," lit r4,#0");
    m->add_asm("reset regs"," lit r5,#0");
    m->add_asm("reset regs"," lit r6,#0");
    m->add_asm("reset regs"," lit r7,#0");
    m->add_asm("reset regs"," lit r8,#0");
    m->add_asm("reset regs"," lit r9,#0");
    m->add_asm("reset regs"," lit r10,#0");
    
    m->add_inst( { .op=OP_GOTO,
      .srcA=unused_src,
      .srcB=unused_src,
      .dst=unused_src,
      .size=1,
      .comment="Start main()",
      .label="__Global__.main"
      });
}


static char * relpathname(const char * ref,const char * name){
  static char ret[1024];
  char * last_occurrance = ret;
  char * store = ret;
  while(*ref) {
    *store = *ref;
    store ++;
    if (*ref=='/') last_occurrance=store;
    ref++;
  }
  while(*name){
    *last_occurrance=*name;
    last_occurrance++;
    name++;
  }
  *last_occurrance=0;
  return ret;
}
  

void Parser::include() {
    
    base_token * item = Accept (&parenth_open);
    token_parser* tkparse;

    std::string basepath = "";

    token_queue curr_tokens=tokens;
    if (item->type()==t_literal) {
        fstream incsource;
        string fname = relpathname(deb_get_file_name().c_str(),item->val().c_str());
        incsource.open (fname, ios_base::in);
        if ( incsource.fail() ) err_report ("could not open include file ",item->val());
        debug_context deb(fname);
        tkparse= new token_parser (incsource);
        tkparse->parse_tokens();
        token_queue new_tokens;
        for (auto item :tkparse->token_list)
          if (item->type()!=t_whitespace) new_tokens.push (item);
        tokens=new_tokens;
        while (END_FILE!=parse()) {};
    } else err_report ("missing include file name");

    tokens = curr_tokens;

    next_token();
    Accept (&parenth_close);
}

unsigned int Parser::AcceptInt(std::string ErrTxt) {
    base_token * item = tokens.front();
    if (item->type()!=t_integer) err_report(ErrTxt);
    next_token();
    return item->ref();
}

base_token * Parser::Accept (accept_t *a) {
    base_token * item = tokens.front();
    for (; a; a=a->next)
        if (item->type()==a->type && a->value==item->val())
            return next_token();
    err_report ("unexpected ",item->val());
    return nullptr;
}
void Parser::condition(){
  Accept (&parenth_open);ResultNeeded=true;
  parse_expression (currentclass(),currentmethod(),objects["_R0"],')');
  Accept (&parenth_close);ResultNeeded=false;
}

bool Parser::add_epilogue() {
    Method * m = currentmethod();

    m->add_label("set function exit label.",m->name+"_exit");

    // allocate room for locals
    // by skipping SP over the size
    if (m->get_stack_size()>0) {
        inst stackop =inst();
        stackop = {
            .op=OP_ADD,
            .srcA=ObSrc(objects["_SP"]),
            .srcB=ImmSrc(m->get_stack_size()),
            .dst=ObSrc(objects["_SP"]),
            .size = 1,
            .comment="De-allocate local vars"
        };
        m->add_inst(stackop);
    }
    inst stackop =inst();
    stackop.op=OP_POP;
    stackop.srcA=ObSrc(objects["_SP"]);
    stackop.size=1; // of the ref..
    stackop.comment="Restore callee saved reg." ;
    // NOTE: we skip R0 because that is the return register.
    for (int i=m->avail_count-1; i>0; i--) {
        alloc_state s = m->availableRegs[i];
        if (s.used) {
            stackop.dst=ObSrc((Object *)s.ref);
            m->add_inst(stackop);
        }
    }
    if (m->args_size>0) {
        inst stackop=inst();
        stackop.op=OP_POP;
        stackop.dst=ObSrc(objects["_ARGS"]);
        stackop.srcA=ObSrc(objects["_SP"]);
        stackop.size=1; // of the ref..
        stackop.comment="Restore ARGS pointer" ;
        m->add_inst(stackop);
    }
    stackop=inst();
    stackop.op=OP_POP;
    stackop.dst=ObSrc(objects["_PC"]);
    stackop.srcA=ObSrc(objects["_SP"]);
    stackop.size=1; // of the ref..
    stackop.comment="Return to caller." ;
    m->add_inst(stackop);
}

bool Parser::add_prologue() {
    std::stack<inst> prologue;
    Method * m = currentmethod();
    inst stackop = inst();
    stackop.op=OP_LABEL;
    stackop.comment="set function entry label." ;
    stackop.label=m->name;
    prologue.push(stackop);
    stackop=inst();
    stackop.op=OP_PUSH;
    stackop.dst=ObSrc(objects["_LR"]);
    stackop.srcA=ObSrc(objects["_SP"]);
    stackop.size=1; // of the ref..
    stackop.comment="Save return address" ;
    prologue.push(stackop);
    if (m->args_size>0) {
        stackop=inst();
        stackop.op=OP_PUSH;
        stackop.dst=ObSrc(objects["_ARGS"]);
        stackop.srcA=ObSrc(objects["_SP"]);
        stackop.size=1; // of the ref..
        stackop.comment="Save ARGS pointer" ;
        prologue.push(stackop);
         stackop = {
            .op=OP_ASSIGN,
            .srcA=ImmSrc(3+m->args_size),
            .srcB=unused_src,
            .dst=ObSrc(objects["_ARGS"]),
            .size = 1,
            .comment="Set ARGS pointer"
        };       // gen stack offset
        prologue.push(stackop );
        stackop = {
            .op=OP_ADD,
            .srcA=ObSrc(objects["_SP"]),
            .srcB=ObSrc(objects["_ARGS"]),
            .dst=ObSrc(objects["_ARGS"]),
            .size = 1,
            .comment="Set ARGS pointer"
        };
        prologue.push(stackop );
    }
    // allocate room for locals
    /*
     *
     *
     *
     */
    stackop = {
        .op=OP_PUSH,
        .srcA=ObSrc(objects["_SP"]),
        .srcB=unused_src,
        .dst=ObSrc(objects["_R1"]),
        .size = 1,
        .comment="Save used reg on stack"
    };
    //gen_wrapping
    std::stack<alloc_state> unwind;
    // NOTE: we can skip R0 because that is the return register.
    for (int i=1; i<m->avail_count; i++) {
        alloc_state s = m->availableRegs[i];
        if (s.used) {
            stackop.dst=ObSrc((Object *)s.ref);
            prologue.push(stackop);
        }
    }
    // allocate room for locals
    // by skipping SP over the size
    if (m->get_stack_size()>0) {
        stackop = {
            .op=OP_ADD,
            .srcA=ObSrc(objects["_SP"]),
            .srcB=ImmSrc(-m->get_stack_size()),
            .dst=ObSrc(objects["_SP"]),
            .size = 1,
            .comment="Allocate local vars"
        };
        // gen stack offset
        prologue.push(stackop );
    }
    while (!prologue.empty()) {
        m->prepend_inst(prologue.top());
        prologue.pop();
    }
}

bool Parser::do_continue (string n) {
  Method * m = currentmethod();
  if (m==nullptr) err_report (n," outside function.");
  debout (n);
  m->add_jmp("jump to continue tag.",Continue);
  return true;
}

bool Parser::do_break (string n) {
  Method * m = currentmethod();
  if (m==nullptr) err_report (n," outside function.");
  debout (n);
  m->add_jmp("jump to break tag.",Break);
  return true;
}

bool Parser::do_return (string n) {
    Method * m = currentmethod();
    if (m==nullptr) err_report ("\"return\" outside function.");
    base_token * item = tokens.front();
    debout ("return");
    if ((item->type()!=t_punctuation) || (item->ref()!=';')) {
        eval_expression (nullptr,m,objects["_R0"]); // return expression in R0
        
        Tinst=inst();
        Tinst.op=OP_ASSIGN;
        Tinst.srcA = ObSrc(objects["_R1"]);
        Tinst.dst = ObSrc(objects["_R0"]);
        Tinst.size = 1;
        Tinst.comment = "Set return value."  ;
        m->add_inst ( Tinst);
    }
    

    m->add_jmp("Goto epilogue.", m->name+"_exit");
    return true;
}

bool Parser::do_namespace (string n) {
    base_token * item = tokens.front();
    if (item->type()==t_symbol) {
        debout("return object:",n);
        next_token();
    } else if (item->type()==t_integer) {
        debout("return val:",n);
        next_token();
    }
    return true;
}

bool Parser::do_if_then_else (string n) {
    Method * m = currentmethod();
    m->add_label( "Start of IF",generate_tag());
    if (m==nullptr) err_report ("\"if\" outside function.");
    //m->scope_in();
    
    string ElseLabel = generate_tag();
    string EndIfLabel = generate_tag();

    condition();
    
    m->add_label( "End of condition ",generate_tag());
        
    m->add_brancheq("branch to else if not true",ElseLabel);

    

    parse();
    
    
    m->add_label( "End of first block ",generate_tag());
    debout("A do else? ",tokens.front()->val());
    if (is_token(";")) next_token();
    debout("B do else? ",tokens.front()->val());
    if (is_token("else")) {

        next_token();
        m->add_jmp("jump to endif.",EndIfLabel);
        m->add_label( "Start of else block.",ElseLabel);
        parse();

    } else m->add_label( "Start of else block.",ElseLabel);
    
    m->add_label( "End if.",EndIfLabel);
   // m->scope_out();
}

bool Parser::do_while(string n) {


  /*
   *
   * TODO : allow instructions to be added to a different queue
   * so while starts with jump to eval
   * and eval does jump to start on 'true'
   *
   * so we need to be able to reshuffle the code blocks.
   * mainly because the branch relative is not enough..
   * 
   */
   //string Continue,Break;
   // int NestContinue=0,NestBreak=0;
    string OldContinue = Continue;
    string OldBreak = Break;
    
    Method * m = currentmethod();
    
    string WhileBody = generate_tag();
    m->add_label("start WHILE",WhileBody);
    
    string WhileEnd = generate_tag();

    Continue = WhileBody;
    Break = WhileEnd;
    
    condition();
    
    m->add_brancheq("skip WhileBody on false.",WhileEnd);

    parse();
    m->add_jmp("jump to WhileBody.",WhileBody);

    m->add_label("End WHILE statement", WhileEnd );

    Continue = OldContinue;
    Break = OldBreak;
}



bool Parser::do_switch(string n) {


  /*
   *
   * TODO : allow instructions to be added to a different queue
   * so while starts with jump to eval
   * and eval does jump to start on 'true'
   *
   * so we need to be able to reshuffle the code blocks.
   * mainly because the branch relative is not enough..
   * 
   */

    // keep copy for nesting.
    string OldBreak = Break;
    bool OldInsideSwitch = InsideSwitch;
    int OldNumCaseLabels=num_case_labels;
    num_case_labels=0;
    InsideSwitch = true;
    
    Method * m = currentmethod();
    if (m==nullptr) err_report("\"switch\" used outside method or function.");
   
    string SwitchBody = generate_tag();
    string SwitchEnd = generate_tag();
    string SwitchTree = generate_tag();
    
    m->add_label("start SWITCH",SwitchBody);
    

    Break = SwitchEnd;
    
    Accept (&parenth_open);
    parse_expression (currentclass(),currentmethod(),objects["_R0"],')');
    Accept (&parenth_close);
    m->add_jmp("goto switch tree.",SwitchTree);

    parse_block();

    m->add_jmp("goto end of switch.",SwitchEnd);
    
    m->add_label("SWITCH tree", SwitchTree);

    string SkipLabel = generate_tag();

    int num_default_labels=0;
    string default_label="";

    while (num_case_labels && !case_labels.empty()){

      num_case_labels--;
      
      if (case_labels.top().label.substr(0,7)=="default") {
        // keep track of 'default:' label
        if (num_default_labels!=0) err_report("only one default label allowed in switch statement.");
        num_default_labels++;
        default_label = case_labels.top().label;
      } else {

        // create condition check
        m->add_label("next case label",SkipLabel);
        
        m->add_inst( { .op=OP_XOR,
          .srcA=ObSrc(objects["_R1"]),
          .srcB=ImmSrc(case_labels.top().val),
          .dst=ObSrc(objects["_LR"]),
          .size=1,
          .comment="Check for case label." });
        
        SkipLabel = generate_tag();  
        m->add_branchNE("Check next case label on false." ,SkipLabel);
        
        m->add_jmp("goto case label on true." ,case_labels.top().label);
      }
      case_labels.pop();
    }
    m->add_label("last case label",SkipLabel);
    // did we have a default? then jump there.
    if (num_default_labels) m->add_jmp("goto default label." ,default_label);
    if (num_case_labels) err_report("internal error, case label stack corrupt.");

    // add label for where to go when all is done.
    m->add_label("End SWITCH statement", SwitchEnd );

    // restore copy for nesting.
    InsideSwitch= OldInsideSwitch;
    num_case_labels=OldNumCaseLabels;
    Break = OldBreak;
    return true;
}

bool Parser::do_case(string n) {
    // if not true jump to next label check.
    // fall through is complicated..
    // can detect empty statements on m->instcount.
    Method * m = currentmethod();
    if (m==nullptr) err_report("case statement  used outside  method or function.");
    num_case_labels++;
    case_label_t label;
    if (InsideSwitch==false) err_report("case statement outside switch");
    unsigned int val = AcceptInt("Only static integers allowed for case labels.");
    Accept(&colon_char);
    label.val=val;
    label.label=generate_tag();
    m->add_label("case label.",label.label);
    case_labels.push(label);
}

bool Parser::do_default(string n) {
    Method * m = currentmethod();
    if (m==nullptr) err_report("default statement  used outside  method or function.");
    num_case_labels++;
    case_label_t label;
    if (InsideSwitch==false) err_report("default statement outside switch");
    Accept(&colon_char);
    label.val=0;
    label.label="default"+generate_tag();
    m->add_label("default label.",label.label);
    case_labels.push(label);
}

bool Parser::do_for(string n) {
    string OldContinue = Continue;
    string OldBreak = Break;
 
    Method * m = currentmethod();
    if (m==nullptr) err_report("\"for\" used outside  method or function.");
    m->scope_in();
    
    string ForEval = generate_tag();
    string ForAdvance = generate_tag();
    string ForBody = generate_tag();
    string ForEnd = generate_tag();

    Continue = ForAdvance;
    Break = ForEnd;
     
    Accept (&parenth_open);

    parse_expression (currentclass(),currentmethod(),objects["_R0"]);
    Accept (&end_statement);

    m->add_label("start FOR evaluation",ForEval);
    parse_expression (currentclass(),currentmethod(),objects["_R0"]);
    Accept (&end_statement);

    m->add_brancheq("jump to end for if false",ForEnd);
    m->add_jmp("skip advancer.",ForBody);
    m->add_label("entry advance FOR iterator",ForAdvance);
    parse_expression (currentclass(),currentmethod(),objects["_R0"],')');
    Accept (&parenth_close);
    m->add_jmp("perform for evaluation.",ForEval);
    m->add_label("start FOR body",ForBody);
    
    parse();

    m->add_jmp("perform advancer and for check.",ForAdvance);
    m->add_label("End of FOR statement.",ForEnd);

    m->scope_out();

    Continue = OldContinue;
    Break = OldBreak;

}
bool Parser::do_asm(string n) {
  base_token *item = Accept (&parenth_open);
  if (item->type()!=t_literal) err_report("asm: expected string.");
  Method * m = currentmethod();
  if (m==nullptr) err_report("\"asm\" used outside method or function.");
  m->add_asm("inline assembly",item->val());
  item = next_token();
  Accept (&parenth_close);
}

bool Parser::add_class (string name) {
    debout("Class entry ",name);
    base_token * item = next_token();
    Class * thisclass = new Class;
    currentclass()->classes[name]=thisclass;
    thisclass->name=currentclass()->name+"."+name;
    curr_class.push (thisclass);
    currType.push (CLASS);
    // get class definition.
    parse_block();
    curr_class.pop();
    currType.pop();
    return true;
}

bool Parser::add_method (Class * return_class,string type,int is_pointer,string name) {
    // check if prototype exists..
    Method * proto=currentclass()->has_method(name); // we already have this one?
    if (proto && proto->is_proto == false) err_report("redefinition of method ",name);

      Method * m=new Method;
      m->return_type = type;
      m->return_class = return_class;
      m->parent_class = currentclass();
      m->is_pointer = is_pointer;
      m->is_proto = false;
      // return value will always be R0.
      m->add_available_reg (objects[reg_name(REG_RESULT)]);
      for(std::string reg:reg_names_for_type(REG_GENERIC))
          m->add_available_reg (objects[reg]);
      // m->add_available_reg (objects["_ARGS"]);
      // insert in parent class
      currentclass()->methods[name]=m;
      //if (currentclass()!=this)
      name=currentclass()->name+"."+name;
      m->name=name;
      // nested to method
      curr_method.push (m);
      currType.push (METHOD);
      // get input types and names
      int stack_offset=0;
      base_token * item = next_token();
      Class *cref=nullptr;
      bool has_args=false;
      std::string first_arg;
  
      // TODO: fix this messs..
      
      while (item->type() != t_eof) {
          string s=item->val();
          if (item->type()==t_punctuation && item->ref()==')') {
              item = next_token();
              break;
          }
          debout ("add obj ",s);
          if (item->type()==t_symbol) {
              Class * c = has_class (s);
              if (c) {
                  cref=c;
                  item = next_token();
              }
              if (cref==nullptr) err_report("missing class definition before ",item->val());
              string n;
              // add object to method objects.
              if (new_object (cref,s,n,true,REF_ARGS)) {
                  if (!has_args) {
                    first_arg=n;
                  }
                  has_args=true;
                  m->inputs.push_back (n);
                  item = tokens.front();
                  if (item->type()==t_punctuation && item->ref()==')') {
                      item = next_token();
                      break;
                  }
                  if (item->type()==t_punctuation && item->ref()==',') {
                      item = next_token();
                      continue;
                  }
              }
          }
          cout << item->val() << " ";
          err_report ("error parsing parameter list");
      }
      if (has_args) {
        Object * first = m->objects[first_arg];
        first->address = m->add_local_object(first_arg,first);
        // check if proto has same args here..
      }
      
    
    item = tokens.front();
    
    if (item->type()==t_punctuation && item->ref() == ';') // prototype definition.
     {
        item = next_token();
        curr_method.pop();
        currType.pop();
        if(proto&&proto->is_proto==true) err_report("redefinition of prototype method",name);
        m->is_proto=true;
        return true;
      }
    if (proto) delete proto;
    debout ("m parse block");
    Accept(&brack_open);
    while (parse()!=END_BLOCK){};
    
    debout ("end m parse block");
    add_prologue();
    add_epilogue();
    curr_method.pop();
    currType.pop();
    return true;
}


bool Parser::parse_block() {
    base_token * item = tokens.front();
    Method * m = currentmethod();

    // NOTE: scope_in() scop_out() is important to allow
    // local scoped variables.
    // At scope_out Delete object should be called but that is still unimplemented.
    
    if (m!=nullptr) m->scope_in();
    if (item->type()==t_punctuation || item->ref()=='{') {
        item = next_token();
        parse_end_t  ret = OTHERS;
        while ((ret!=END_BLOCK)&&(ret!=END_FILE)) {
          ret=parse();
        }
        if (ret==END_FILE) err_report ("unexpected end of file");
        if (m!=nullptr) m->scope_out();
        return true;
    } else err_report ("instead of '{'  found unexpected :",item->val());
    return false;
}

void Parser::gen_evaluation (std::list<postfix_t> &postfix_expr,Object* destobj) {
// we have a stack of ops(tokens) and arguments(Objects)
// generate the instructions to implement the eval here.
// for a test we just assign the dest.
    std::stack<Object *> Sargs;
    std::stack<Object *> Rargs;
    std::stack<Method *> Margs;
    std::stack<bool> Bargs;
    std::stack<int> arg_count;
    int num_args=0;
    Method *m = currentmethod();
    m->release_all_eval_regs();
    bool has_call = false;
    for (auto step:postfix_expr) {
        if (step.is_op) {
            debout ("operation: ",step.arg);
            if (step.op=='llac' || step.op=='call')has_call = true;
        } else
            debout ("value: ",step.arg);
    }
    /*
    if (postfix_expr.size()==1 && postfix_expr.back().is_op==false) { // single value
        warning_report ("single value evaluation.");
    }*/
    if (postfix_expr.size()==1 && postfix_expr.back().is_op==true) { // single value
        err_report ("no value operation.");
    }
    // keep R0 unused if we call any methods.
    Object * call_return = nullptr;
    call_return = m->get_available_reg (EVAL_REG);
    bool LastItemIsOp=true;
    int num_regs=0;
    // work-around: dereference is missed if last op is index.
    if (postfix_expr.back().is_op && postfix_expr.back().op=='indx') {
        postfix_t endop = postfix_t();
        endop.is_op=true;
        endop.op='end';
        postfix_expr.push_back(endop);
    }
    Object argstack;
    argstack.rtype=REF_STACK;
    argstack.address=0;
    std::stack<Object*> arguments;
    int num_ops=0;
    int argssize=0;
    bool AtoBworkaround=false;
    for (auto item: postfix_expr) {
        bool AtoBworkaround=false;
        LastItemIsOp=item.is_op;
        if (!item.is_op) {
            if (num_regs==11) err_report ("insufficient available regs. reduce reg usage by breaking expressions using multiple assignment.");
            // use free reg for operand
            Object * regn = nullptr;//m->get_available_reg(EVAL_REG);
            
            
            //Sargs.push (regn);
            Rargs.push (item.o);
            Margs.push (item.m);
            Object * ref = item.o;
            void * fn = (void *) item.m;
            if (item.o == nullptr || item.m) {
              
                if (item.m) {
                    Bargs.push(true);
                    Sargs.push (nullptr);
                    /*
                    m->add_inst ( {
                        .op=OP_ASSIGN,
                        .srcA= ObSrc(objects["this"]),
                        .srcB= unused_src,
                        .dst= ObSrc(regn),
                        .size=1,
                        .comment="use reg for the this ptr "
                    });*/
                } else {
                    // claim a register for use
                    regn = m->get_available_reg(EVAL_REG);
                    num_regs++;
                    Sargs.push (regn);
                    switch (item.type) {
                    case t_literal:
                        Bargs.push(true);
                        m->add_inst ({
                            .op=OP_ASSIGN,
                            .srcA= {REF_LABEL, (void *)0},
                            .srcB= unused_src,
                            .dst= {regn->rtype, (void *) regn->address},
                            .size=1,
                            .comment="assign label to reg",
                            .label=item.arg
                        });
                        break;
                    case t_integer:
                        Bargs.push(false);
                        ref = new Object;
                        ref[0]=Object();
                        ref[0].rtype=REF_IMMEDIATE;
                        ref[0].address=item.op;
                        m->release_reg(regn);
                        Sargs.top()=nullptr;
                        
                        Rargs.top()=ref;
                        if (postfix_expr.size()==1) {
                          m->add_inst ({
                              .op=OP_ASSIGN,
                              .srcA= ImmSrc(item.op),
                              .srcB= unused_src,
                              .dst= ObSrc(regn),
                              .size=1,
                              .comment="assign int " +item.arg +" to reg"
                          });
                        }
                        
                        break;
                    default:
                        err_report ("TODO: gen code for non-objects.");
                        break;
                    }
                }
            } else {
                if (item.o->classref==classes["register"]) {
                  //m->release_reg(regn);
                  //Sargs.pop();
                  Sargs.push(item.o);
                  Bargs.push(true);
                } else {

                  // claim a register for use
                  regn = m->get_available_reg(EVAL_REG);
                  num_regs++;
                  Sargs.push (regn);
                  
                  if (postfix_expr.size()>1){
                    Bargs.push(false);
                    //Sargs.push (regn);
                  } else {


                    // if we can eliminate this we can reduce the overhead.

                    
                    Bargs.push(true);
                    //regn = m->get_available_reg(EVAL_REG);
                    //num_regs++;
                    //Sargs.push (regn);
                    std::string comment ="get ";
                    if (ref->rtype == REF_STACK) comment+="local var ";
                    if (ref->rtype == REF_ARGS) comment+="argument var ";
                    if (ref->rtype == REF_HEAP_ADDRESS) comment+="heap var ";
                    
                    m->add_inst ( {
                    .op=OP_ASSIGN,
                    .srcA= ObSrc(ref),
                    .srcB= unused_src,
                    .dst= ObSrc(regn),
                    .size=1,
                    .comment=comment+item.arg
                    });
                  }
                }
            }
        } else {
            // ops require 2 values
            if (Sargs.empty()) err_report ("insufficient terms in evaluation");
            Object * A = Sargs.top();
            Sargs.pop();
            Object * Ar = Rargs.top();
            Rargs.pop();
            Method * Am = Margs.top();
            Margs.pop();
            bool Ab = Bargs.top();
            Bargs.pop();
            
            
            
            num_regs --;
            Object * B;
            Object * Br;
            Method * Bm;
            bool Bb;
            // get dereferenced value
            if (Ar && (Ar->rtype==REF_INDIRECT||Ar->rtype==REF_ABSOLUTE)) {
                m->add_inst ( {
                    .op=OP_ASSIGN,
                    .srcA= {REF_ABSOLUTE, (void *)A->address},
                    .srcB= unused_src,
                    .dst= ObSrc(A),
                    .size=1,//dest->classref->size
                    .comment="fetch first dereferenced value at pointer address",
                });
                Ar->rtype=REF_ABSOLUTE;
            } else {
              if ((Ab==false&&Ar&&Ar->rtype!=REF_IMMEDIATE)
                  ||(Sargs.empty()&&ResultNeeded)) { // when only value and we're a condition.
                    if(A==nullptr) A=m->get_available_reg(EVAL_REG);
                    m->add_inst ( {
                      .op=OP_ASSIGN,
                      .srcA= ObSrc(Ar),
                      .srcB= unused_src,
                      .dst= ObSrc(A),
                      .size=1,
                      .comment="fetch referenced A value"});
                  }
            }
            if (item.op=='end') break;
            if (!Sargs.empty()) { 
                 B = Sargs.top();
                 Br = Rargs.top();
                 Bm = Margs.top();
                 Bb = Bargs.top();
              // Exception: op_call and no arguments..
             // if ((item.op=='call') && (Bm==nullptr) && Am && (Am->args_size==0))
             // {
              //  B=A;
              //  Br=Ar;
               // Bm=Am;
              //  Bb=Ab;
              //} else {

                 
                 if ((Bargs.top()==false) && (item.op != '=')) {
                     if (B==nullptr) {
                        Sargs.top()=B=m->get_available_reg(EVAL_REG);
                     }
                     if (Br && (Br->is_pointer&0x100)) { // Br is an array..
                       Object * Src;
                       unsigned int address = Br->address;
                       switch (Br->rtype) {
                         case REF_CLASS_OFFSET: Src = objects["this"];break;
                         case REF_STACK:        Src = objects["_SP"];break;
                         case REF_ARGS:        Src = objects["_ARGS"];break;
                         case REF_A_INDIRECT:  Br->rtype=REF_ARGS;
                                               m->add_inst ( {
                                                  .op=OP_ASSIGN,
                                                  .srcA= ObSrc(Br),
                                                  .srcB= unused_src,
                                                  .dst= ObSrc(B),
                                                  .size=1,
                                                  .comment="fetch referenced B value"});
                                               Br->rtype=REF_A_INDIRECT;
                                               address=Br->Iaddress;
                                               Src = B;
                                               break;
                         default: err_report("Unhandled array conversion type.");
                                  break;
                       }
                       m->add_inst ( {
                        .op=OP_ADD,
                        .srcA= ObSrc(Src),
                        .srcB= ImmSrc(address),
                        .dst= ObSrc(B),
                        .size=1,
                        .comment="fetch referenced B array address"});
                        Bargs.top()=true;
                     } else {
                       
                       m->add_inst ( {
                        .op=OP_ASSIGN,
                        .srcA= ObSrc(Br),
                        .srcB= unused_src,
                        .dst= ObSrc(B),
                        .size=1,
                        .comment="fetch referenced B value"});
                        Bargs.top()=true;
                    }
                  }
                  // still pretty tricky because we want to keep as low as possible reg count
                  if (A) m->release_reg(A);
              //}
            } else {
              // exceptions to the two args rule:
              if (item.op=='++'||item.op=='--'||item.op=='~'||item.op=='!'|| item.op=='call' || item.op=='ref') {
                AtoBworkaround=true;
                B = A;
                Br = Ar;
                Bm = Am;
                Bb = Ab;
               // A  = &Zero;
               // Ar = &Zero;
               // Am = nullptr;
              } else err_report ("insufficient terms in evaluation");
            }
            // get dereferenced value if not used as assignment destination.
            if (Br && (Br->rtype==REF_ABSOLUTE)&&(item.op!='=')&&(item.op!='-=')&&(item.op!='+=')) {
                m->add_inst ( {
                    .op=OP_ASSIGN,
                    .srcA= {REF_ABSOLUTE, (void *)B->address},
                    .srcB= unused_src,
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size
                    .comment="fetch second dereferenced value at pointer address",
                });
                *Br=*B;
                //err_report("unsure if this is used.");
            };
            //if (A->size != B->size) err_report("operation different size objects");
            enum ops thisop = OP_ADD;
            if (item.op=='end') break;
            switch (item.op) {
            case '+':
                thisop =OP_ADD;
                break;
            case '-':
                thisop =OP_SUB;
                break;
            case '*':
                thisop =OP_MUL;
                break;
            case '/':
				thisop =OP_DIV;
                break;
            #if 0
                m->add_inst ( {
                    .op=OP_PUSH,
                    .srcA= ObSrc(objects["_SP"]),
                    .srcB= unused_src,
                    .dst= ObSrc(A),
                    .size=1,//dest->classref->size  TODO: add complete objects on stack
                    .comment="push A on stack"
                });
                m->add_inst ( {
                    .op=OP_PUSH,
                    .srcA= ObSrc(objects["_SP"]),
                    .srcB= unused_src,
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size  TODO: add complete objects on stack
                    .comment="push B on stack"
                });
                m->add_inst ( {
                    .op=OP_CALL,
                    .srcA= {REF_LABEL, (void *)A->address},
                    .srcB= {REF_LABEL, (void *)A->address},
                    .dst= ObSrc(call_return),
                    .size=1,//dest->classref->size
                    .comment="do div(A,B)",
                    .label="__div__"
                });
                m->add_inst ( {
                    .op=OP_ADD,
                    .srcA= ObSrc(objects["_SP"]),
                    .srcB= ImmSrc(2),
                    .dst= ObSrc(objects["_SP"]),
                    .size=1,//dest->classref->size  TODO: add complete objects on stack
                    .comment="restore stack"
                });
                A=call_return;
                thisop =OP_ASSIGN;
                break;
            #endif
            case '&&': // we should safely be able to assume that previous ops generated a result.
                       // but it should not be handled like this at all..
                       // because here A gets done first here?
                       // somewhere this should have a skip so the second part is not performed..
                       // skipped label is either ')' or ';'..
                       //   m->add_brancheq(expressionEnd);
                       //   continue;
                
            case '&': 
                thisop =OP_AND;
                break;
            case '|':
            case '||':
                thisop =OP_ORR;
                break;
            case '^':
                thisop =OP_XOR;
                break;
            case ',':
                arguments.push(Ar);
                if (A==nullptr&& Ar && Ar->rtype==REF_IMMEDIATE)
                    {
                      A=objects["_R0"];
                      m->add_inst ( {
                        .op=OP_ASSIGN,
                        .srcA= ObSrc(Ar),
                        .srcB= unused_src,
                        .dst= ObSrc(A),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="push first arg on stack"
                      });
                    }
                m->add_inst ( {
                    .op=OP_ASSIGN,
                    .srcA= ObSrc(A),
                    .srcB= unused_src,
                    .dst= ObSrc(&argstack),
                    .size=1,//dest->classref->size  TODO: add complete objects on stack
                    .comment="push arg on stack"
                });
                argstack.address-=(Ar && (Ar->rtype!=REF_INDIRECT) && (Ar->rtype!=REF_A_INDIRECT))?Ar->get_size():1;
                
                continue;
                break;
            case '==':
                thisop = OP_EQUAL;
                break;
            case '!=':
                thisop = OP_NOTEQUAL;
                break;
            case '<':
                thisop = OP_SMALLER;
                break;
            case '>':
                thisop = OP_GREATER;
                break;
             case '<=':
                thisop = OP_SMEQUAL;
                break;
            case '>=':
                thisop = OP_GREQUAL;
                break;
           case '<<':
                thisop = OP_SHLEFT;
                break;
            case '>>':
                thisop = OP_SHRIGHT;
                break;

            case 'call': {
                if (Bm->args_size>0) {
                    arguments.push(Ar);
                    if (A==nullptr&& Ar && Ar->rtype==REF_IMMEDIATE)
                    {
                      A=objects["_R0"];
                      m->add_inst ( {
                        .op=OP_ASSIGN,
                        .srcA= ObSrc(Ar),
                        .srcB= unused_src,
                        .dst= ObSrc(A),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="push first arg on stack"
                      });
                    }
                    m->add_inst ( {
                        .op=OP_ASSIGN,
                        .srcA= ObSrc(A),
                        .srcB= unused_src,
                        .dst= ObSrc(&argstack),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="push first arg on stack"
                    });
                   argstack.address-=(Ar&&Ar->rtype!=REF_INDIRECT&&Ar->rtype!=REF_A_INDIRECT)?Ar->get_size():1;
                }
                if ((-argstack.address) < Bm->args_size)
                      err_report("insufficient args for method");
                 
                bool ThisPtrNeedsRestore=false;
                bool ThisPtrNeedsOffset=false;

                if (Br->rtype==REF_CLASS_OFFSET) {
                  if(Br->address!=0) // not first in class offset.
                    {
                       ThisPtrNeedsOffset=true;
                       m->add_inst ( {
                                .op=OP_ADD,
                                .srcA= ObSrc(objects["this"]),
                                .srcB= ImmSrc(Br->address),
                                .dst= ObSrc(objects["this"]),
                                .size=1,//dest->classref->size
                                .comment="setup this ptr from this object relative pos.",
                            });
                    }
                } else 
                
                if (Br->address!=0) {
                    if (B==nullptr) {
                      B=m->get_available_reg(EVAL_REG);
                      if (!AtoBworkaround) Sargs.top()=B;
                    }
                    m->add_inst ( {
                            .op=OP_ASSIGN,
                            .srcA= ObSrc(objects["this"]),
                            .srcB= unused_src,
                            .dst= ObSrc(B),
                            .size=1,//dest->classref->size
                            .comment="save this ptr in reg.",
                        });
                    switch(Br->rtype) {
                    case REF_STACK:
                        m->add_inst ( {
                            .op=OP_ADD,
                            .srcA= ObSrc(objects["_SP"]),
                            .srcB= ImmSrc(Br->address),
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object stack relative pos.",
                        });
                        ThisPtrNeedsRestore=true;
                        break;
                    case REF_CLASS_OFFSET: err_report("internal error: should have been handled in previous if statement.");
                    /*
                        m->add_inst ( {
                            .op=OP_ASSIGN,
                            .srcA= ImmSrc(Br->address),
                            .srcB= unused_src,
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object address",
                        });*/
                        m->add_inst ( {
                            .op=OP_ADD,
                            .srcA= ObSrc(objects["this"]),
                            .srcB= ImmSrc(Br->address),
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object stack relative pos.",
                        });
                        ThisPtrNeedsRestore=true;
                        break;
                    case REF_A_INDIRECT:
                        m->add_inst ( {
                            .op=OP_FETCH,
                            .srcA= ObSrc(objects["_ARGS"]),
                            .srcB= ImmSrc(-Br->address),
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object address",
                        });
                        if (Br->Iaddress){
                          m->add_inst ( {
                            .op=OP_ADD,
                            .srcA= ObSrc(objects["this"]),
                            .srcB= ImmSrc(Br->Iaddress),
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object stack relative pos.",
                        });
                        }
                        ThisPtrNeedsRestore=true;
                        break;
                   case REF_HEAP_ADDRESS:
                        m->add_inst ( {
                            .op=OP_ASSIGN,
                            .srcA= ImmSrc(Br->address),
                            .srcB= unused_src,
                            .dst= ObSrc(objects["this"]),
                            .size=1,//dest->classref->size
                            .comment="setup this ptr from object address",
                        });
                        ThisPtrNeedsRestore=true;
                        break;
                 /*   case REF_UNUSED:
                        if (Bm->parent_class==this) { // base function
                            m->add_inst ( {
                                .op=OP_ASSIGN,
                                .srcA= ImmSrc(objects["__Global_Space__"]->address),
                                .srcB= unused_src,
                                .dst= ObSrc(objects["this"]),
                                .size=1,//dest->classref->size
                                .comment="setup this ptr from object address",
                            });
                            ThisPtrNeedsRestore=true;
                          };*/
                    default:
                            err_report("called method has unsupported object reference.");
                       
                    }
                } //else warning_report("called method has same this ptr.");
                //}
               
                /*
                // ensure Bo now will be of the return type
                if (Bm->args_size>0) {
                    m->add_inst ( {
                        .op=OP_PUSH,
                        .srcA= ObSrc(objects["_SP"]),
                        .srcB= unused_src,
                        .dst= ObSrc(A),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="push A on stack"
                    });
                } else { // keep operand because it did not belong to the result.
                    Sargs.push (A);
                    Rargs.push (Ar);
                    Margs.push (Am);
                }*/
                if (Bm->args_size>0) {
                    Bm->verify_stack(arguments);
                    m->add_inst ( {
                        .op=OP_SUB,
                        .srcA= ObSrc(objects["_SP"]),
                        .srcB= ImmSrc(Bm->args_size),
                        .dst= ObSrc(objects["_SP"]),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="compensate for args stack"
                    });
                }
                m->add_inst ( {
                    .op=OP_CALL,
                    .srcA= {REF_LABEL, (void *)A->address},
                    .srcB= {REF_LABEL, (void *)A->address},
                    .dst= ObSrc(call_return),
                    .size=1,//dest->classref->size
                    .comment="do call :",
                    .label=Bm->name
                });
                if (Bm->args_size>0) {
                    m->add_inst ( {
                        .op=OP_ADD,
                        .srcA= ObSrc(objects["_SP"]),
                        .srcB= ImmSrc(Bm->args_size),
                        .dst= ObSrc(objects["_SP"]),
                        .size=1,//dest->classref->size  TODO: add complete objects on stack
                        .comment="restore stack"
                    });
                    argstack.address+=Bm->args_size;
                }
                if(ThisPtrNeedsRestore)
                  if (Br) m->add_inst ( {
                      .op=OP_ASSIGN,
                      .srcA= ObSrc(B),
                      .srcB= unused_src,
                      .dst= ObSrc(objects["this"]),
                      .size=1,//dest->classref->size  TODO: add complete objects on stack
                      .comment="retrieve this ptr"
                  });
                if(ThisPtrNeedsOffset==true) {
                       m->add_inst ( {
                                .op=OP_SUB,
                                .srcA= ObSrc(objects["this"]),
                                .srcB= ImmSrc(Br->address),
                                .dst= ObSrc(objects["this"]),
                                .size=1,//dest->classref->size
                                .comment="setup this ptr from this object relative pos.",
                            });
                }
                if (B==nullptr) {
                      B=m->get_available_reg(EVAL_REG);
                      if (!AtoBworkaround) Sargs.top()=B;
                    }
                A=call_return;
                thisop =OP_ASSIGN;
            }
            break;
            case 'ref':
                  {
                       Object * Src;
                       // A was already released..
                       A= m->get_available_reg(EVAL_REG);

                       // dereference object.
                       if (Am==nullptr)
                       switch (Ar->rtype) {
                         case REF_CLASS_OFFSET: Src = objects["this"];break;
                         case REF_STACK:        Src = objects["_SP"];break;
                         case REF_ARGS:        Src = objects["_ARGS"];break;
                         case REF_A_INDIRECT:  Src = objects["_ARGS"];
                                                Ar->rtype=REF_ARGS;
                                                 m->add_inst ( {
                                                    .op=OP_ASSIGN,
                                                    .srcA= ObSrc(Ar),
                                                    .srcB= unused_src,
                                                    .dst= ObSrc(A),
                                                    .size=1,
                                                    .comment="fetch referenced address(&A)"});
                                                 if (Ar->Iaddress)
                                                   m->add_inst ( {
                                                      .op=OP_ADD,
                                                      .srcA= ObSrc(A),
                                                      .srcB= ImmSrc(Ar->Iaddress),
                                                      .dst= ObSrc(A),
                                                      .size=1,
                                                      .comment="fetch dereferenced address(&A)"});
                                                Ar->rtype=REF_A_INDIRECT;
                                                Sargs.push(A);
                                                Rargs.push(Ar);
                                                Margs.push(Am);
                                                Bargs.push(true);
                                                Ab=true;
                                                continue;
                         default: err_report("Unhandled referenced address conversion type.");
                                  break;
                       }
                       unsigned int RefAddress=Ar->address;
                       unsigned int IRefAddress=Ar->Iaddress;
                       if (Am) RefAddress=(unsigned int)Am->address;
                       

                       m->add_inst ( {
                        .op=OP_ADD,
                        .srcA= ObSrc(Src),
                        .srcB= ImmSrc(RefAddress),
                        .dst= ObSrc(A),
                        .size=1,
                        .comment="fetch referenced address(&A)"});
                        Ar->rtype=REF_A_INDIRECT;
                        Sargs.push(A);
                        Rargs.push(Ar);
                        Margs.push(Am);
                        Bargs.push(true);
                        Ab=true;
                    }
                    continue;
                      
            case 'indx':
            case 'xdni':
                if ((Br->is_pointer&511)==0) err_report("dereferencing non pointer",item.arg);
                if ((Ar->rtype==REF_ABSOLUTE)||(Ar->rtype==REF_REGISTER)) {
                  // we can take a short cut here..
                }
                // for now only pointer
                thisop = OP_ADD;
                // notify we are now absolute.
                // TODO actual on stack indexed
                if ((Br->is_pointer&0x10000)==0) Br->rtype=REF_ABSOLUTE;//REF_INDIRECT;
                else {
                  Object * tO=B;
                  // index on register
                  B=m->get_available_reg(EVAL_REG);
                  Object * tBr=new Object;
                  tBr[0]=Br[0];
                  Br=tBr;
                  Rargs.top()=Br;
                  Br->rtype=REF_ABSOLUTE;
                  Sargs.top()=B;
                  m->add_inst ( {
                    .op=OP_ADD,
                    .srcA= ObSrc(tO),
                    .srcB= ObSrc(A?A:Ar),
                    .dst= ObSrc(B),
                    .size=1,
                    .comment="new index pointer"
                  });
                  continue;
                }
                break;
            case '=':
                thisop =OP_ASSIGN;
                // find dereferenced pointers
                if (Br) {
                  if (Br->rtype==REF_INDIRECT||Br->rtype==REF_ABSOLUTE) {
                      Br->address=B->address;
                      Br->rtype==REF_ABSOLUTE;
                  }
                  B=Br;
                }
                break;
            case '+=':
                m->add_inst ( {
                    .op=OP_ADD,
                    .srcA= ObSrc(B),
                    .srcB= ObSrc(A?A:Ar),
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size
                    .comment="do A "+ item.arg + " B"});
                thisop =OP_ASSIGN;
                // find dereferenced pointers
                if (Br->rtype==REF_INDIRECT||Br->rtype==REF_ABSOLUTE) {
                    Br->address=B->address;
                    Br->rtype==REF_ABSOLUTE;
                }
                A=B;
                B=Br;
                break;
            case '-=':
                m->add_inst ( {
                    .op=OP_SUB,
                    .srcA= ObSrc(B),
                    .srcB= ObSrc(A?A:Ar),
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size
                    .comment="do A "+ item.arg + " B"});
                thisop =OP_ASSIGN;
                // find dereferenced pointers
                if (Br->rtype==REF_INDIRECT||Br->rtype==REF_ABSOLUTE) {
                    Br->address=B->address;
                    Br->rtype==REF_ABSOLUTE;
                }
                A=B;
                B=Br;
                break;
            case '--':
            case '++': {
                  Object * Dest=B;
                  int size = (Br && (Br->is_pointer &255) && Br->classref)? Br->classref->size : 1;
                  if (item.op=='--') size=-size;
                  if (Br && Br->rtype==REF_REGISTER && (B->address==Br->address)) { // we have a reg val that is referenced first time.
                    if ((Ar==Br)&&Sargs.empty()) {
                      m->add_inst ( {
                      .op=OP_ADD,
                      .srcA= ObSrc(B),
                      .srcB= ImmSrc(size),
                      .dst= ObSrc(B),
                      .size=1,//dest->classref->size
                      .comment="do A "+ item.arg});
                      continue;
                    } else {
                        Dest = m->get_available_reg(EVAL_REG);
                        if (!Sargs.empty()) Sargs.top() = Dest;
                        A=Dest;
                      }
                  }
                                      
                  m->add_inst ( {
                      .op=OP_ADD,
                      .srcA= ObSrc(B),
                      .srcB= ImmSrc(size),
                      .dst= ObSrc(Dest),
                      .size=1,//dest->classref->size
                      .comment="do A "+ item.arg});
                  thisop =OP_ASSIGN;
                  // find dereferenced pointers
  
                  if (Br->rtype==REF_INDIRECT||Br->rtype==REF_ABSOLUTE) {
                      Br->address=B->address;
                      Br->rtype==REF_ABSOLUTE;
                  }
                  B=Br;
                }
                break;
/*             case '--':
                m->add_inst ( {
                    .op=OP_ADD,
                    .srcA= ObSrc(B),
                    .srcB= ImmSrc(-1),
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size
                    .comment="do A "+ item.arg });
                thisop =OP_ASSIGN;
                // find dereferenced pointers
                if (Br->rtype==REF_INDIRECT||Br->rtype==REF_ABSOLUTE) {
                    Br->address=B->address;
                    Br->rtype==REF_ABSOLUTE;
                }
                B=Br;
                break;*/
           default:
                err_report ("unknow operation in expression evaluation",item.arg);
                break;
            }
            if (thisop!=OP_PUSH) {
                Object * Dest=B;
                
                if (thisop!=OP_ASSIGN) {
                    if (Br && Br->rtype==REF_REGISTER && (B->address==Br->address)) { // we have a reg val that is referenced first time.
                      Dest = m->get_available_reg(EVAL_REG);
                      if (!Sargs.empty()) Sargs.top() = Dest;
                    }
                    m->add_inst ( {
                      .op=thisop,
                      .srcA= ObSrc(B),
                      .srcB= ObSrc(A?A:Ar),
                      .dst= ObSrc(Dest),
                      .size=1,//dest->classref->size
                      .comment="do A "+ item.arg + " B"
                      });
                }
                else {
                    m->add_inst ( {
                    .op=thisop,
                    .srcA= ObSrc(A?A:Ar),
                    .srcB= unused_src,
                    .dst= ObSrc(B),
                    .size=1,//dest->classref->size
                    .comment=(Br && Br->rtype==REF_ABSOLUTE)? "store A at [B]":"do B = A"
                    });
                  }
            } else
                m->add_inst ( {
                .op=thisop,
                .srcA= Br->rtype==REF_ABSOLUTE? ObSrc(B):ObSrc(objects["_SP"]),
                .srcB= unused_src,
                .dst= ObSrc(A),
                .size=1,//dest->classref->size  TODO: add complete objects on stack
                .comment=Br->rtype==REF_ABSOLUTE? "store A at [B]":"push A on stack"
            });
        }
    }
    /*
    m->replace_last_inst((inst) {
               .op=OP_ASSIGN,
                .srcA.type=objects["_R2"]->rtype,
                .dst.type=dest->rtype,
                .srcA.ref=(void *)objects["_R2"]->address,
                .dst.ref=(void *)dest->address,
                .size=1,//dest->classref->size
                .comment="push result"
               });*/
    //if (!LastItemIsOp) err_report("left over terms in evaluation");
    m->release_all_eval_regs();
}



/*
While (we have not reached the end of Q)
        If (an operand is found)
           Add it to P
        End-If
        If (a left parenthesis is found)
           Push it onto the stack
        End-If
        If (a right parenthesis is found)
           While (the stack is not empty AND the top item is
                  not a left parenthesis)
              Pop the stack and add the popped value to P
           End-While
           Pop the left parenthesis from the stack and discard it
        End-If
        If (an operator is found)
           If (the stack is empty or if the top element is a left
               parenthesis)
              Push the operator onto the stack
           Else
              While (the stack is not empty AND the top of the stack
                     is not a left parenthesis AND precedence of the
                     operator <= precedence of the top of the stack)
                 Pop the stack and add the top value to P
              End-While
              Push the latest operator onto the stack
           End-If
        End-If
     End-While
     While (the stack is not empty)
        Pop the stack and add the popped value to P
     End-While
*/
unsigned int Parser::eval_expression (Object * g,Method *m,Object * dest) {
    return parse_expression (g?g->classref:currentclass(),m?m:currentmethod(),dest);
}

char hexchar[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

static string to_hex(unsigned int ref){
  string ret;
  ret = ret + hexchar[(ref>>12)&0xf];
  ret = ret + hexchar[(ref>>8)&0xf];
  ret = ret + hexchar[(ref>>4)&0xf];
  ret = ret + hexchar[(ref)&0xf];
  return ret;
}

// very simple array constructor.
// allows adding int and literals.
void Parser::process_const_array(string & tag,unsigned int & size){
    base_token * item = tokens.front();
    int depth=0;
    if ((item->type() == t_punctuation)&&(item->ref()=='{')) {
      depth++;
      item = next_token();
    }
    std::queue< string > str_literal_tags;
               
    bool negate=false;
   
    while (depth)
    {
       if ((item->type() == t_punctuation)&&(item->ref()=='{')) {
         depth++;
         item = next_token();
         continue;
       }
       if ((item->type() == t_punctuation)&&(item->ref()=='}')) {
         depth--;
         item = next_token();
         continue;
       }
       if ((item->type() == t_punctuation)&&(item->ref()==',')) {
         item = next_token();
         continue;
       }
       if ((item->type() == t_punctuation)&&(item->ref()=='-')) {
         negate=true;
         item = next_token();
         continue;
       }
       if (item->type() == t_integer || item->type() == t_const_literal) {
         unsigned int ref = item->ref();
         if (negate) ref=0x10000-ref;
         negate=false;
         str_literal_tags.push(to_hex(ref));
         item = next_token();
         continue;
       }
       if (item->type() == t_literal) {
         unsigned int ref = item->ref();
         negate=false;
         string thistag = generate_tag();
         str_literal_tags.push(thistag);
         cout << thistag <<":\n";
         cout <<".w\""<< item->val() <<"\"\n";
         item = next_token();
         continue;
       }
       err_report("cannot process :" ,item->val());
    }
    tag = generate_tag();
    cout << tag << ":\n";
    while (!str_literal_tags.empty())
    {
      size++;
      cout <<"." << str_literal_tags.front()<<"\n";
      str_literal_tags.pop();
    }
}
unsigned int Parser::parse_expression (Class * cl,Method *mt,Object*obj,unsigned int br_at) {
    debout ( "expression");
    base_token * item = tokens.front();
    std::stack<unsigned int> Stack;
    std::list<postfix_t> postfix_expr;
    postfix_t Sinsert;
    unsigned int Call = (unsigned int)'call';
    bool possible_fnptr = false;
    bool new_objects = false;
    bool has_assignment = false;
    int nest = 0;
    bool classdef_busy=false;
    bool start_parenth=false;
    bool re_interpret_prefix = true;
    Class * cptr = nullptr;
    string c_name;
    while (item->type() != t_punctuation || (item->ref() != ';'))  {
        if ((item->type() == t_punctuation)&&(item->ref()=='{')) {
          string val;
          unsigned int ref;
          process_const_array(val,ref);
          Sinsert = postfix_t();
          Sinsert.is_op = false;
          Sinsert.op = ref; // should be the size of the data 
          Sinsert.arg = val; // will be the label.
          Sinsert.type = t_const_array;
          postfix_expr.push_back (Sinsert);
          item = tokens.front();
          continue;
        }
        //debout( "expression: ",item->val());
        if ((nest==0)&&(item->type() == t_punctuation)&&(item->ref()==br_at)) break;

        // replace '()' with void.

        if(start_parenth &&(item->type()==t_punctuation)&&(item->ref()==')'))
        {
            Sinsert = postfix_t();
            Sinsert.is_op = false;
            Sinsert.op = 0;
            Sinsert.arg = "__void__";
            Sinsert.type = t_symbol;
            postfix_expr.push_back (Sinsert);
        }
        start_parenth=false;
        if ((item->type()==t_punctuation)&&(item->ref()=='('))
          start_parenth=true;
        

        
        if (item->type()==t_symbol || item->type()==t_integer || item->type()==t_literal || item->type()==t_const_literal) {
            re_interpret_prefix = false;
            string val = item->val();
            unsigned int ref=item->ref();
            auto type = item->type();
            if (item->type()==t_symbol) {
                Class * c=is_class (val);
                // enable object definitions inside expressions
                if ((is_obj(nullptr,nullptr,val)==nullptr) && (is_method(val)==nullptr) &&( c || classdef_busy)) {
                    debout("expression created new object :",val);
                    if ( c ) {
                        cptr = c;
                        c_name = val;
                        item=next_token();
                    }
                    classdef_busy=true;
                    debout("expression created new ",c_name);
                    debout(" object :",item->val());
                    if (cptr->has_member(item->val())==nullptr) {
                      curr_type_t result =new_object (cptr,c_name , val ,false, (currenttype()==METHOD)?REF_STACK:REF_CLASS_OFFSET);
                      if (result==METHOD) return 0; // only one method at a time.
                    } else err_report("redefinition of ",item->val());
                    new_objects = true;
                    item = tokens.front();
                    if (item->type() == t_punctuation && item->ref() == ';') break; // end loop..
                    if (item->type() == t_punctuation && item->ref() == ',') continue; // skip inserting in ops
                } else {
                    possible_fnptr = true;
                    item = next_token();
                }
            } else item = next_token();
            Sinsert = postfix_t();
            Sinsert.is_op = false;
            Sinsert.op = ref;
            Sinsert.arg = val;
            Sinsert.type = type;
            postfix_expr.push_back (Sinsert);
            
            continue;
        }
        if (item->type()==t_punctuation) {
            if ((item->ref()=='=')||(item->ref()=='+=')||(item->ref()=='-=')) {
              has_assignment=true;
              
            }
              
            if ((item->ref()==',') && (nest==0) && (cptr )) { // continuation of object declarations.
                while (!Stack.empty() && Stack.top()!='(' && Stack.top()!='['&&  (precedence (Call) <= precedence (Stack.top()))) {
                    Sinsert = postfix_t();
                    Sinsert.is_op = true;
                    Sinsert.op = Stack.top();
                    postfix_expr.push_back (Sinsert);
                    Stack.pop();
                }
                Stack.push (item->ref());
                item = next_token(); // look ahead
                classdef_busy=true;possible_fnptr = false;
                re_interpret_prefix = true;
                continue;
            }
            classdef_busy = false;
            if ((item->ref()=='(') || (item->ref()=='[')) {
                nest ++;
                re_interpret_prefix = true;
                if (possible_fnptr) {
                    unsigned int insert = 'indx';
                    if (item->ref()=='(') insert='call';
                    while (!Stack.empty() && Stack.top()!='(' && Stack.top()!='['&&  (precedence (insert) <= precedence (Stack.top()))) {
                        Sinsert = postfix_t();
                        Sinsert.is_op = true;
                        Sinsert.op = Stack.top();
                        postfix_expr.push_back (Sinsert);
                        Stack.pop();
                    }
                    Stack.push (insert);
                }
                possible_fnptr=false;
                Stack.push (item->ref());
                item = next_token();
                continue;
            }
            possible_fnptr = false;
            if (item->ref()==')') {
                re_interpret_prefix = false;
                nest --;
                while (!Stack.empty() && Stack.top()!='('&& Stack.top()!='[') {
                    //Pop the stack and add the popped value to P
                    Sinsert = postfix_t();
                    Sinsert.is_op = true;
                    Sinsert.op = Stack.top();
                    postfix_expr.push_back (Sinsert);
                    Stack.pop();
                }
                if (Stack.empty()) err_report ("parenthesis imbalance");
                if (Stack.top()!='(') err_report ("unexpected parenthesis imbalance");
                //Pop the left parenthesis from the stack and discard it
                Stack.pop();
                item = next_token();
                continue;
            }
            if (item->ref()==']') {
                re_interpret_prefix = false;
                nest --;
                while (!Stack.empty() && Stack.top()!='['&& Stack.top()!='(') {
                    Sinsert = postfix_t();
                    Sinsert.is_op = true;
                    Sinsert.op = Stack.top();
                    postfix_expr.push_back (Sinsert);
                    Stack.pop();
                }
                if (Stack.empty()) err_report ("square bracket imbalance");
                if (Stack.top()!='[') err_report ("unexpected square bracket imbalance");
                //Pop the left square bracket from the stack and discard it
                Stack.pop();
                item = next_token();
                continue;
            }
            if (Stack.empty() || Stack.top()=='(') {
                // handle possible prefix method
                if (re_interpret_prefix) {
                   Stack.push(re_interpret(item->ref()));
                   re_interpret_prefix=false;
                } else {
                   Stack.push(item->ref());
                   re_interpret_prefix=true;
                };
                item = next_token();
                continue;
            }
            while (!Stack.empty() && Stack.top()!='(' && Stack.top()!='['&&  (precedence (item->ref()) <= precedence (Stack.top()))) {
                Sinsert = postfix_t();
                Sinsert.is_op = true;
                Sinsert.op = Stack.top();
                postfix_expr.push_back (Sinsert);
                Stack.pop();
            }
            // handle possible prefix method
            if (re_interpret_prefix) {
                Stack.push(re_interpret(item->ref()));
                re_interpret_prefix=false;
            } else {
                Stack.push(item->ref());
                re_interpret_prefix=true;
            };
            item = next_token();
            continue;
        }
        err_report ("unexpected ",item->val());
    }
    while (!Stack.empty()) {
        Sinsert = postfix_t();
        Sinsert.is_op = true;
        Sinsert.op = Stack.top();
        postfix_expr.push_back (Sinsert);
        Stack.pop();
    }
    
    if(new_objects && (has_assignment==false)) {
      debout ("skipping code generation, no assignments.");
    } else postfix_to_inst (postfix_expr,obj);
    
    debout ("END expression");
    return item->ref() == ',';
}
static void print_postfix (std::list<postfix_t> &postfix_expr) {
    string out="";
    for (auto p : postfix_expr) {
        char s[5] = {0,0,0,0,0};
        if (p.is_op) {
            unsigned char s[5] = {0,0,0,0,0};
            * ((unsigned int *)s) = p.op;
            out += string((char*)s) + " ";
        } else {
            out += " "+  p.arg + "(";
            switch (p.type) {
            case t_symbol:
                out +="ob ref " ;
                if (p.o) out += string("[") + p.o->typedesc() + string(" at ") + to_string((int) p.o->address ) + "]";
                if (p.m) out += p.m->typedesc()+ " fn()@" + to_string((int) p.m->address);
                break;
            case t_literal:
                out +="imm lit";
                break;
            case t_integer:
                out +="imm int";
                break;
            default:
                out +="other";
                break;
            };
            out += ") ";
        }
    }
    debout(" Postfix result:",out);
}


void Parser::postfix_to_inst (std::list<postfix_t> &postfix_expr,Object *destobj) {
    std::list<postfix_t> dotresolved;
    std::list<postfix_t> resolved;
    int indx=0;
    int rindx=0;
    print_postfix (postfix_expr);
    for (auto p : postfix_expr) {
        if (p.is_op) {
            char s[]= {0,0,0,0,0};
            ((unsigned int *)s)[0]=p.op;
            p.arg = s;
            // name resolve
            if (p.op=='.') {
                postfix_t a = dotresolved.back();
                dotresolved.pop_back();
                postfix_t b = dotresolved.back();
                dotresolved.pop_back();
                if (b.o==nullptr) {
                    Object * ref =is_obj (nullptr,nullptr,b.arg);
                    if (ref==nullptr) err_report (b.arg," undefined ");
                    Object * tO = new Object;
                    *tO = *ref;
                    b.o=tO;
                    if (b.o->is_pointer&255) err_report (b.arg," is a pointer so cannot use '.'");
                }
                Object * ref = b.o->has_member (a.arg);
                if (ref) {
                    reftype rtype = b.o->rtype;
                    unsigned int address = (b.o->rtype==REF_A_INDIRECT) ?b.o->address :b.o->address+=ref->address;
                    unsigned int Iaddress = (b.o->rtype==REF_A_INDIRECT) ?b.o->Iaddress+=ref->address :b.o->Iaddress;
                    // overwrite b object with dereference
                    b.o[0]=ref[0];
                    b.o->rtype=rtype;
                    b.o->address=address;
                    b.o->Iaddress=Iaddress;
                   
                    b.arg+=".";
                    b.arg+=a.arg;
                    dotresolved.push_back (b);
                } else {
                    b.m = b.o->has_method (a.arg);
                    if (b.m == nullptr) err_report (a.arg," undefined ");
                    b.arg+=".";
                    b.arg+=a.arg;
                    b.o->mparent=b.m;
                    b.o->classref=b.m->return_class;
                    dotresolved.push_back (b);
                }
            } else dotresolved.push_back (p);
        } else dotresolved.push_back (p);
    }
    for (auto p : dotresolved) {
        if (!p.is_op && (p.type==t_symbol) && (p.o==nullptr)) {
            Object * ref = is_obj (nullptr,nullptr,p.arg);
            Object * tO = new Object;
            *tO=(ref==nullptr) ? Object():*ref;
            p.o=tO;
            if (ref==nullptr) { //perhaps a function?
                p.m = is_method (p.arg);
                if (p.m==nullptr) {
                    p.c=is_class(p.arg);
                    if (p.c==nullptr) {
                        err_report (p.arg," undefined ");
                    }
                    err_report("found class ",p.arg);
                }
                tO->mparent=p.m;
                //tO->rtype=REF_LABEL;
                //tO->label="__data__";
                debout("found method ",p.arg);
            } else {
                debout("found object ",p.arg);
            }
        }
        if (!p.is_op && (p.type==t_literal)) {
            string ref = generate_tag();
            cout << ref << ":\n";
            cout <<".w\""<< p.arg<<"\"\n";
            p.arg=ref;
        }
        if (!p.is_op && (p.type==t_const_array)) {
            //this has been converted already.
            p.type=t_literal;
        }
        if (!p.is_op && (p.type==t_const_literal)) {
            unsigned int ref=p.arg.c_str()[0] + (p.arg.c_str()[0]<<8);
            //cout << ref << ":\n";
            //cout <<".w\""<< p.arg<<"\"\n";
            p.arg=ref;
            p.type=t_integer;
        }
        
        resolved.push_back (p);
    }
    print_postfix (resolved);
    gen_evaluation (resolved,destobj);
}
/*
 * parse_end_t Parser::parse()
 * coarse parsing for scaffold.
 */

parse_end_t Parser::parse() {
    bool ret;
    while (tokens.size()>0) {
        base_token * item = tokens.front();
        if (item->type()==t_eof) return END_FILE;
        //cout << "checking " << item->val() << "\n";
        switch (item->type()) {
        case t_invalid_token:
            err_report ("unexpected ",item->val());
            break;
        case t_punctuation:
            switch (item->ref()) {
            case ';': // closing statement
                item = next_token();
                return END_STATEMENT;
            case '{':
                //cout << "starting block\n";
                parse_block();
                return END_STATEMENT;
                //cout << "ended block\n";
                break;
            case '}':
                item = next_token();
                return END_BLOCK;
                break;
            default:
                err_report ("unexpected ",item->val());
                item = next_token();
                break;
            } ;
            break;
        case t_symbol:
            if (is_internal_op (item->val())) {
                next_token();
                do_internal_op (item->val());
                return END_STATEMENT;
            }
            parse_expression (currentclass(),currentmethod(),objects["_R0"]); // ret= obj_reference(obj,item->val());
            return END_STATEMENT;
            if (Class * c=is_class (item->val())) {
                string name;
                next_token();
                ret = new_object (c,item->val(),name,false,(currenttype()==METHOD)?REF_STACK:REF_CLASS_OFFSET);
                Object * nw = (currenttype()==METHOD)?currentmethod()->objects[name]:currentclass()->objects[name];
                item = tokens.front();
                while (item->type()==t_punctuation && ((item->ref()==',')||(item->ref()=='='))) // next item.
                {
                  if (item->ref()=='='){
                    next_token();
                    parse_expression (currentclass(),currentmethod(),objects["_R0"],',');
                    currentmethod()->add_inst( {.op=OP_ASSIGN,
                      .srcA=ObSrc(objects["_R1"]),
                      .srcB=unused_src,
                      .dst=ObSrc(nw),
                      .size=c->size,
                      .comment="allocate initial value to " + name}
                    );
                    item = tokens.front();
                    continue;
                  }
                  next_token();
                  ret = new_object (c,item->val(),name,false,(currenttype()==METHOD)?REF_STACK:REF_CLASS_OFFSET);
                  nw = (currenttype()==METHOD)?currentmethod()->objects[name]:currentclass()->objects[name];
                  item = tokens.front();
                }
                // cout << "finished addobject()\n";
                return END_STATEMENT;
            } else {
                parse_expression (currentclass(),currentmethod(),objects["_R0"]); // ret= obj_reference(obj,item->val());
                return END_STATEMENT;
            }
            item = next_token();
            break;
        case t_eol:
            debug_advance_line();
            item = next_token();
            break;
        case t_whitespace:
            item = next_token();
            break;
        case t_eof:
            return END_FILE;
        default:
            err_report("unexpected ",item->val());
            item = next_token();
            break;
        };
    }
    //
    return END_FILE;
}

bool  Parser::do_internal_op (string s ) {
    handler h=builtin[s];
    s=tokens.front()->val();
    if (h) (this->*h) (s);
    return 0;
}

curr_type_t  Parser::new_object (Class * cref,string s , string & name ,bool params,reftype rtype) {
    int is_pointer=0;
    base_token * item = tokens.front();
    while (item->type() == t_punctuation) {
        if (item->ref() == 0x2a) { // check for pointer *
            is_pointer++;
            item = next_token();
            continue;
        }
        if (item->ref() == 0x2a2a) { // check for double pointer **
            is_pointer+=2;
            item = next_token();
            continue;
        }
        if (item->ref() == 0x2a2a2a) { // check for triple pointer ***
            is_pointer+=3;
            item = next_token();
            continue;
        }
        if (item->ref() == 0x2a2a2a2a) { // check for quadruple pointer ****
            is_pointer+=4;
            item = next_token();
            continue;
        }
        if (item->ref() == 0x26) { // check for ref &
            is_pointer+=0x200;
            item = next_token();
            if (rtype==REF_ARGS) rtype=REF_A_INDIRECT;
            continue;
        }
        if (item->ref() == 0x2626) { // check for double ref &
            is_pointer+=2*0x200;
            item = next_token();
            if (rtype==REF_ARGS) rtype=REF_A_INDIRECT;
            continue;
        }
        if (item->ref() == 0x262626) { // check for triple ref &
            is_pointer+=3*0x200;
            item = next_token();
            if (rtype==REF_ARGS) rtype=REF_A_INDIRECT;
            continue;
        }
        if (item->ref() == 0x26262626) { // check for quadruple ref &
            is_pointer+=4*0x200;
            item = next_token();
            if (rtype==REF_ARGS) rtype=REF_A_INDIRECT;
            continue;
        }
        break;
    }
    // cout << "new_object: " << s << " " << item->val() << "\n";
    if (item->type() != t_symbol) {
        err_report ("Failed to parse object definition");
        return OBJ;
    }
    
    name = item->val();
    item = next_token();
    if (item->type()==t_punctuation && item->ref()=='(') { // we found a method definition.
        if (currenttype()!=CLASS)  err_report("method definition inside method not allowed.");
        if (cref==classes["register"]) err_report("register definition of method not allowed.");
        add_method (cref,s,is_pointer,name);
        return METHOD;
    }

    if ((cref==classes["register"])&&(currenttype()!=METHOD))err_report("register definition only allowed in method body.");
    
    if (cref==classes["register"] ) {
      Object * reg = currentmethod()->get_highest_available_reg(TMP_REG);
      if (reg) {
        Object * o = new Object;
        *o=*reg;
        o->type=s;
        if (is_pointer)is_pointer|=0x10000;
        o->is_pointer=is_pointer;
        o->classref = classes["register"];
        currentmethod()->add_local_object(name,o);
        return OBJ;
      }
    }
    Object * o = new Object;
    *o=Object();
    o->type=s;
    o->is_pointer=is_pointer;
    o->classref = cref;
    o->rtype=rtype;
    o->cparent = currentclass();
    o->mparent = currentmethod();
    
    unsigned int size = (is_pointer==0)?cref->size:1;
    if (item->type()==t_punctuation && item->ref()=='[') { // we found a size definition.
      item = Accept(&sqbrack_open);
      int size = AcceptInt("only static array sizes allowed.");
      o->AddArraySize(size);
      o->is_pointer|=256;
      item = Accept(&sqbrack_close);
    }
      
    // link
    switch (o->rtype) {
    case   REF_CLASS_OFFSET:
        o->address = currentclass()->add_member(name,o);
        if (currentclass()==this) {
          o->address+=objects["__Global_Space__"]->address;
          o->rtype=REF_HEAP_ADDRESS;
        }
        currentclass()->objects[name]=o;
        break;
    case  REF_A_INDIRECT:o->address = currentmethod()->add_local_object(name,o);
        break;
    case  REF_STACK:
    case  REF_ARGS:o->address = currentmethod()->add_local_object(name,o);
        break;
        
    default:
        err_report ("error cannot add object to object");
        break;
    }
    
    return OBJ;
}

Class * Parser::currentclass() {
    if (!curr_class.empty()) return curr_class.top();
    err_report ("internal error:"," class stack empty");
    return nullptr;
}

Method * Parser::currentmethod() {
    if (!curr_method.empty()) return curr_method.top();
    err_report ("internal error:"," method stack empty");
    return nullptr;
}

curr_type_t Parser::currenttype() {
    if (!currType.empty()) return currType.top();
    err_report ("internal error:"," type stack empty");
    return CLASS;
}

void  Parser::dump() {
  /*
    cout << "# builtin:\n";
    for (auto i :builtin) cout << "# "<< i.first <<"\n";
    cout << "# defs:\n";*/
    cout << code();
}
