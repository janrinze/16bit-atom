/*
 * method.cc
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "../utils/debug.h"
#include "object.h"
#include "method.h"
#include "class.h"

static Object Zero;

static Rref_t unused_src { REF_UNUSED,(void *)0};

static inline Rref_t ObSrc( Object * ob) {
    return { ob->rtype,(void *)ob->address};
}

static inline Rref_t ImmSrc( unsigned int v) {
    return { REF_IMMEDIATE,(void *)v};
}

Method::Method()
{
  stack_size=0;
  max_stack_size=0;
  avail_count=0;
  inusecount=0;
}

unsigned int Method::get_stack_size(){
  return max_stack_size;
}

unsigned int Method::add_size(int s) {
        unsigned int t = stack_size;
        stack_size+=s;
        if (stack_size>max_stack_size) max_stack_size=stack_size;
        return t+1;
}
std::string Method::typedesc()
{
    std::ostringstream o;
    o <<return_type<<" ";
    for (int tt=1; tt<=((is_pointer&255)&15); tt++) o << "*";
    for (int t=1; t<=((is_pointer>>9)&15); t++) o << "&";
    return o.str();
}
void Method::add_inst(inst op)
{
    instructions.push_back(op);
}
void Method::prepend_inst(inst op)
{
    instructions.push_front(op);
}
    
void Method::add_brancheq(const std::string &comment,const std::string &label){
  inst branch = inst();
  branch.op=OP_BRA_EQ;
  branch.comment=comment;
  branch.label=label;
  add_inst( branch );
}
void Method::add_branchNE(const std::string &comment,const std::string &label){
  inst branch = inst();
  branch.op=OP_BRA_NE;
  branch.comment=comment;
  branch.label=label;
  add_inst( branch );
}
void Method::add_label(const std::string &comment,const std::string &label){
  inst branch = inst();
  branch.op=OP_LABEL;
  branch.comment=comment;
  branch.label=label;
  add_inst( branch );
}

void Method::add_jmp(const std::string &comment,const std::string &label){
  inst branch = inst();
  branch.op=OP_GOTO;
  branch.comment=comment;
  branch.label=label;
  add_inst( branch );
}
void Method::add_asm(const std::string &comment,const std::string &label){
  inst branch = inst();
  branch.op=OP_ASM;
  branch.comment=comment;
  branch.label=label;
  add_inst( branch );
}
void Method::add_inst_set_result(const std::string &comment,const std::string &obname){
  inst branch = inst();
  branch.op=OP_ASSIGN;
  branch.srcA=ObSrc(availableRegs[0].ref);
  branch.dst=ObSrc(objects[obname]);
  branch.comment=comment;
  add_inst( branch );
}
void Method::replace_last_inst(const inst & op)
{
    instructions.pop_back();
    instructions.push_back(op);
}

void Method::release_all_eval_regs()
{
  for (int i=0;i<avail_count;i++) {
    if (availableRegs[i].state==EVAL_REG) {
        availableRegs[i].state=FREE_REG;
        inusecount--;
    }
  }
}

void Method::release_ALL_regs()
{
    //for (auto &i:availableRegs) i.state=FREE_REG;
    for (int i=0;i<avail_count;i++) availableRegs[i].state=FREE_REG;
    inusecount=0;
}

void Method::add_available_reg(Object * r)
{
  availableRegs[avail_count++] = (alloc_state) {
        .state=FREE_REG,.used=false,.ref=r
    };
    //availableRegs.push_back((alloc_state) {
    //    .state=FREE_REG,.used=false,.ref=(void*)r
    //});
}

void Method::release_reg(Object * r)
{
    for (int i=0;i<avail_count;i++) 
       if (availableRegs[i].ref==(void *)r) {
            availableRegs[i].state=FREE_REG;
            inusecount --;
            return;
        };
    std::cerr << "Warning NOT released\n";
}

unsigned int Method::add_literal(string s)
{
    literals.push_back(s);
    litpool_size+=s.length()+1;
    return (unsigned int)address - litpool_size;
}

unsigned int Method::add_args_size(int s)
{
  if (first_arg_size){
    unsigned int t = args_size;
    args_size+=s;
    return t+1;
  }
  first_arg_size=s;
  return 0;
}
std::string Method::desc()
{
    std::ostringstream o;
    o << " (";
    int pc=0;
    for (auto k:inputs) {
        if (pc) o <<",";
        o << objects[k]->typedesc() << k ;
        pc=1;
    };
    o << ")\n#{\n";
    for (auto j:objects) {
        o << "#"<< j.second->typedesc() << " " << j.first << " (sz:" << j.second->get_size() <<"@"<<j.second->address<< ")\n" ;
    }
    // asm label
    //o << name << ":\n";
    /*   
    for (auto i:instructions)
        o << "# desc "<< i.desc() << "\n";*/
    o << "#}\n";
    return o.str();
}
std::string Method::code()
{
    std::ostringstream o;
    o << " (";
    int pc=0;
    for (auto k:inputs) {
        if (pc) o <<",";
        //o << args[k]->typedesc() << k ;
        pc=1;
    };
    o << ")\n";
       
    for (auto i:instructions)
        o << i.to_asm() << "\n";

    return o.str();
}

Object * Method::has_member(string s)
{
    if (objects.find(s)!=objects.end()) return objects[s];
    if (!objectsstack.empty()) 
      for (int t=objectsstack.size();t>0;t--) 
        if (objectsstack[t-1].objects.find(s)!=objectsstack[t-1].objects.end()) return objectsstack[t-1].objects[s];
    return nullptr;
}

// on '{'
void Method::scope_in() {
  scope_t scope;
  scope.objects=objects;
  scope.stack_size=stack_size;
  objectsstack.push_back(scope);
  objects.clear();
}
// on '}'
void Method::scope_out() {
  // do we need to run 'delete' ?
  for (auto ob : objects) {
    if (ob.second->has_method("Delete")) {
      std::cerr << "Delete on scope out not yet implemented.";
      exit(1);
    }
    if (ob.second->rtype=REF_REGISTER) // release TMP
    {
      for (int r=0;r<avail_count;r++) {
        if ((availableRegs[r].state==TMP_REG)&&
        (ob.second->address==((Object *)availableRegs[r].ref)->address))
           availableRegs[r].state=FREE_REG;
      }
    }
  }  
  if (!objectsstack.empty()) {
    objects = objectsstack.back().objects;
    stack_size = objectsstack.back().stack_size;
    objectsstack.pop_back();
  }
}
// on return()
void Method::scope_all_out() {
  while (!objectsstack.empty()) scope_out();
  scope_out();
}
Object * Method::get_highest_available_reg(regstate_t t)
{

    for (int r=avail_count;r>=0;r--)
       if (availableRegs[r].state==FREE_REG) {
            availableRegs[r].state=t;
            availableRegs[r].used=true;
            inusecount ++;
            return (Object *)availableRegs[r].ref;
        };
    std::cout << "Internal error,"<< inusecount<< " no free regs.\n";
    return nullptr;
};
Object * Method::get_available_reg(regstate_t t)
{

    for (int r=0;r<avail_count;r++)
       if (availableRegs[r].state==FREE_REG) {
            availableRegs[r].state=t;
            availableRegs[r].used=true;
            inusecount ++;
            return (Object *)availableRegs[r].ref;
        };
    std::cout << "Internal error,"<< inusecount<< " no free regs.\n";
    return nullptr;
};
unsigned int Method::add_local_object(string & name,Object * o) {
      // we should check for any errors here.
      if(o) {
          objects[name]=o;
          o->mparent=this;
          if (o->classref ) {
              int size =o-> get_size();//((o->is_pointer&255)==0)? o->classref->size : 1;
              switch(o->rtype){
                    case  REF_STACK:  return add_size(size);
                    case  REF_A_INDIRECT: return add_args_size(1);  //exception on the rules, we use the pointer.           
                    case  REF_ARGS:   return add_args_size(size);
                    case  REF_REGISTER:   return 0;               
                    default:  err_report("cannot add unknown class ref to method.",reftypeToString(o->rtype));
              }
          }
      }
      return 0;
    };

bool  Method::verify_stack(std::stack<Object *> & arguments){
  if (args_size>0) std::cerr<<"Warning: Not yet verifying arguments of methods.\n";
  return true;
}
/*
    void add_push(const std::string &comment,const Object * item,const Object * stk){
      inst stackop=inst();
      stackop.op=OP_PUSH;
      stackop.dst=ObSrc(item);
      stackop.srcA=ObSrc(stk);
      stackop.size=1; // of the ref..
      stackop.comment="Restore ARGS pointer" ;
      add_inst(stackop);
    };

    void add_pop(const std::string &comment,const Object * &ob){
      inst stackop=inst();
      stackop.op=OP_POP;
      stackop.dst=ObSrc(objects["_ARGS"]);
      stackop.srcA=ObSrc(objects["_SP"]);
      stackop.size=1; // of the ref..
      stackop.comment="Restore ARGS pointer" ;
      add_inst(stackop);
    };
  */
