/*
 * parser.h
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef PARSER_H
#define PARSER_H

#include <map>
#include <string>
#include <stack>
#include <list>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

#include "tokens.h"
#include "../backend/codegen.h"

#include "object.h"
#include "method.h"
#include "class.h"


/* term : type of value pushed on the stack.
 *
 */

typedef enum {
    CLASS,
    METHOD,
    OBJ,
} curr_type_t;

typedef union {
    Class * c;
    Object * o;
    Method * m;
} ref_t;

typedef struct {
    bool is_op;
    unsigned int op;
    std::string arg;
    type_of_token type;
    Object * o;
    Method * m;
    Class *c;
} postfix_t;

typedef enum {
    END_STATEMENT,
    END_BLOCK,
    END_FILE,
    OTHERS
} parse_end_t;

typedef struct accept_t {
    struct accept_t * next;
    type_of_token type;
    string value;
} accept_t;

typedef struct{
  unsigned int val;
  string label;
} case_label_t;

class Parser : public Class {

    typedef bool (Parser::*handler)(string);
  private:
    string filename;
    std::map<string,handler> builtin;
    std::list<char *> literals;


    token_queue tokens;
    stack<Class *> curr_class;
    stack<Method *> curr_method;
    stack<curr_type_t> currType;

    list<Class *> namespaces;
    inst Tinst;
    base_token * next_token();
    void include();
    bool add_epilogue();
    bool add_prologue();
    Class * currentclass();
    Method * currentmethod();
    curr_type_t currenttype();
    string Continue,Break;
    int NestContinue=0,NestBreak=0;
    bool InsideSwitch=false;
    bool ResultNeeded=false;
    string SwitchVariable;
    std::stack<case_label_t> case_labels;
    int num_case_labels=0;
    Object *StackPtr, *ProgramCounter, *ArgsPtr, *ThisPtr, *RetVal, *ReturnPtr;

  public:


    Parser(string _filename );
    bool parse_block();
    parse_end_t parse();
    unsigned int parse_expression(Class * cl,Method *mt,Object*obj,unsigned int br_at=';');
    unsigned int AcceptInt(string ErrTxt);
    base_token * Accept(accept_t *);

    bool is_internal_op(string n) {
        if (builtin.find(n)!= builtin.end()) return true;
        return false;
    };

    Object * is_obj(Method *m,Class * c,string n) {
        // try method given
        Object * ret = m ? m->has_member(n) : nullptr;
        // try class given
        if (ret==nullptr) ret = c ? c->has_member(n) : nullptr;
        // try curent stack
        if (ret==nullptr && !curr_method.empty()) ret = curr_method.top()->has_member(n);
        if (ret==nullptr && !curr_class.empty()) ret = curr_class.top()->has_member(n);
        // try outside
        if (ret==nullptr) return has_member(n);
        return ret;
    };

    Class * is_class(string n) {
        Class * ret = nullptr;
        // try current stack
        if (!curr_class.empty()) ret = curr_class.top()->has_class(n);
        // should try namespaces too
        for(auto c : namespaces )
            if (ret==nullptr) ret = c->has_class(n);
        // try globals
        if (ret==nullptr) ret = has_class(n);
        return ret;
    };

    Method * is_method(string n) {
        //std::cout << "find method " << n << "\n";
        Method * ret = nullptr;
        if (!curr_class.empty()) ret = curr_class.top()->has_method(n);
        if (ret==nullptr) ret = has_method(n);
        //if (ret==nullptr) std::cout << "find method cannot find" <<n<< "\n";
        // should try namespaces too
        for(auto c : namespaces )
            if (ret==nullptr) ret = c->has_method(n);
        return ret;
    };
    bool is_token(const string &t);
    void condition();
    bool do_return(string n);
    bool do_namespace(string n);
    bool do_if_then_else(string n);
    bool do_for(string n);
    bool do_while(string n);
    bool do_continue(string n);
    bool do_break(string n);
    bool do_switch(string n);
    bool do_case(string n);
    bool do_default(string n);
    bool do_asm(string n);
    void process_const_array(string & tag,unsigned int & size);
    void gen_evaluation(std::list<postfix_t> &postfix_expr,Object* destobj);
    void postfix_to_inst(std::list<postfix_t> &postfix_expr,Object* destobj);

    bool add_class(string n);
    bool add_method(Class *c,string type,int is_pointer, string name);

    bool do_internal_op(string);
    curr_type_t new_object(Class * cref,string s , string & name ,bool params,reftype rtype);
    unsigned int eval_expression(Object *,Method *m,Object *);

    void dump();
};

#endif /* PARSER_H */
