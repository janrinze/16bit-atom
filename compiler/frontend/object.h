/*
 * object.h
 *
 * Copyright 2018  <janrinze@tegrabook>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include <list>

typedef std::string class_type;
//#include "../backend/utils/string.h"
#include "tokens.h"
#include "../backend/codegen.h"
#include "method.h"

class Object
{
public:

    unsigned int address; // address of object. either offset in class or absolute.
    
    class_type type;
    reftype rtype;


    class Class * classref;
    class Class * cparent;
    class Method * mparent;
    unsigned int Iaddress; // offset after dereferencing.

    int is_pointer; // 0 is none 1 is pointer 2 is pointer to pointer etc. 256*n is n*ref
    std::list<int> array_sizes;
    int get_size();

    string typedesc()
    {
        std::ostringstream o;
        o<<type<<" ";
        for (int tt=1; tt<=(is_pointer&255); tt++) o << "*";
        for (int t=1; t<=(is_pointer>>9); t++) o << "&";
        if (is_pointer&256)
         for (int j:array_sizes)
           o<<"["<<j<<"]";
        return o.str();
    };
    class Method * has_method(string s);
    class Object * has_member(string s);
    void AddArraySize(int s);
private:
    /* add your private declarations */
};

#endif /* OBJECT_H */
