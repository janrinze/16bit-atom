################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 


ifneq ($(TARGET_PLATFORM),windows)
CC_SRCS += \
../frontend/object.cc	\
../frontend/method.cc \
../frontend/class.cc \
../frontend/parser.cc \
../frontend/tokens.cc


OBJS += \
./frontend/object.o	\
./frontend/method.o \
./frontend/class.o \
./frontend/parser.o \
./frontend/tokens.o

else

endif


CC_DEPS += 


# Each subdirectory must supply rules for building sources it contributes

frontend/%.o: ./frontend/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${CC_OPTIMIZE} ${CC_FLAGS} -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
