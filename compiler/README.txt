cc16 is a technology preview for object oriented compiler.

The extensive use of OO on top of C can facilitate programmers
to keep their focus on functionality. Having a full C++ compiler
would be too much work for one person.
This compiler is written in one week.

version 0.0.1

building:

  make

usage:
  cc16 <objccfile>

outputs assembler for the 16 bit RISC.

works on:

  Ubuntu/Debian

depends on:

  TBD

requirements:

	TDB

TODO:
 - peephole optimizer
 - rewrite with proper tree like processing.
 - nesting/scope of variables
 - collapse of compile-time calculations
 - char 'x' not as string (DONE)
 - dynamic allocation
 - deep copy
 - allow fast empty fuctions
 - do init of object vars
 - enable function pointers
 
(c) 2018 Jan Rinze Peterzon janrinze@gmail.com
 
