`include "pll.v"
`include "cpu2.v"
`include "flashmem/icosoc_flashmem.v"
`include "vga/vga.v"
`include "PIA8255/pia8255.v"
//`include "m6522/m6522.v"
`include "spi/spi.v"
`include "utils/debounce.v"

`ifdef verilator
    `define pull_up( source , type, dest)   wire dest; assign dest=source;
    `define pull_N_up( source , type,num, dest)     wire [num:0] dest=source;
`else
`define pull_up( source , type, dest) 	wire dest;  SB_IO #(	.PIN_TYPE(6'b0000_01),.PULLUP(1'b1)	) type (.PACKAGE_PIN(source),.D_IN_0(dest));
`define pull_N_up( source , type,num, dest) 	wire [num:0] dest;  SB_IO #(	.PIN_TYPE(6'b0000_01),.PULLUP(1'b1)	) type[num:0] (.PACKAGE_PIN(source),.D_IN_0(dest));
`endif

module top (
    input pclk,              // 100MHz clock

    // interface to ATOM keyboard
    input shift_key,
    input ctrl_key,
    input [5:0] key_col,
    input rept_key,
    input key_reset,
    output [9:0] key_row,

    output led1,         // 8 user controllable LEDs
    output led2,         // 8 user controllable LEDs
    output led3,         // 8 user controllable LEDs

    // vga RGB 4:4:4 output
    output hsync,
    output vsync,
    output reg [3:0] red,
    output reg [3:0] green,
    output reg [3:0] blue,

    // interface to 1MB SRAM icoboard
    output [18:0] SRAM_A,
    inout [15:0] SRAM_D,
    output SRAM_nCE,
    output SRAM_nWE,
    output SRAM_nOE,
    output SRAM_nLB,
    output SRAM_nUB,

    // SDcard
    input  miso,
    output mosi,
    output ss,
    output sclk,

    // onboard flash
    output SPI_FLASH_CS,
    output SPI_FLASH_SCLK ,
    output SPI_FLASH_MOSI ,
    input SPI_FLASH_MISO
  );

  reg [1:0] sram_state=0;
  wire sram_wrlb, sram_wrub;
  wire [18:0] sram_addr;
  wire [15:0] sram_dout;
  wire [15:0] sram_din;
  wire phi2_we;
 `ifdef verilator
        assign sram_din = phi2_we? 0 : SRAM_D;
        assign SRAM_D = phi2_we? sram_dout:16'hZZZZ;
  `else
  SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) sram_io [15:0] (
        .PACKAGE_PIN(SRAM_D),
        .OUTPUT_ENABLE(phi2_we),
        .D_OUT_0(sram_dout),
        .D_IN_0(sram_din)
    );
  `endif
  wire rst;                  // reset signal
  wire locked;
  wire fclk;
  reg delay;
  wire [15:0] cpu_din;
  wire [15:0] cpu_dout;
  wire [15:0] cpu_address;
  wire cpu_write,cpu_read;

  // Setup Clocks @ 32.5 MHz

  pll mypll(
      	.clock_in(pclk),
        .clock_out(fclk),
        .locked(locked)
  );
  //reg locked;

  //always @* begin
  //  locked=tlocked;
  //end
   // 32.5 MHz cpu
  reg [1:0] cnt=0;
  wire [1:0] nxtcnt;
  assign nxtcnt = cnt +1;

  //reg wg;
  reg spiclk=0;
  reg clk=0,vidclk=0,cpuclk;
  reg boot=0;
  wire kbd_reset;


  reg wg;

	always@(posedge fclk) begin
    spiclk <= ~nxtcnt[0];
		clk <= ~nxtcnt[1];
    //cpuclk <= ~nxtcnt[1] | vga_req ;
    // write gate and write cycle

    // using cpu_write here means that we are delayed by 1 fclk!
    // using ~nxtcnt[1] as a gate keeper it still is properly timed.
    //wg  <= (~nxtcnt[1] & ~nxtcnt[0])|~((cpu_write & ~delay) | boot);
    cnt <= nxtcnt;
    wg <= ~(phi2_we & nxtcnt[1]);
	end

  assign phi2_we = (cpu_write & ~delay) |boot;

  //always @(negedge clk)
  //  not_wg <= phi2_we;

  //wire wg = clk | ~phi2_we;

  // pullup keys
  `pull_up(rept_key,  rept_key_t,		rept_keyp)
  `pull_up(shift_key, shift_keyt,		shift_keyp)
  `pull_up(ctrl_key,  ctrl_keyt,		ctrl_keyp)
  `pull_N_up(key_col,  key_colt,5,		key_colp)
  `pull_up(key_reset, key_reset_t, key_reset_p)

  wire Rpressed,Rreleased;

  PushButton_Debouncer rstkey(
    .clk(clk),
    .PB(key_reset_p),
    .PB_state(kbd_reset),
    .PB_down(Rpressed),
    .PB_up(Rreleased));

  wire reboot_req = (kbd_reset & (~rept_keyp));
  reg IO_req=0;
  reg [15:0] IO_dat=0;

  wire cpu_reset = kbd_reset | (~locked) | boot;

  // reset logic
  always @(posedge clk) begin
    if (~locked) begin
      rst <=1;
      IO_req<=0;
      IO_dat<=16'h0000;
      delay<=1;
      end
    else
      delay<=vga_req;
  end

  reg halt=0;

  cpu mycpu(    .clk(clk),            // clock
                .rst(cpu_reset),            // reset
                .delay(delay),        // bus delay
                .write(cpu_write),    // CPU write request
                //.read(cpu_read),      // CPU read request
                .address(cpu_address),// read/write address
                .dout(cpu_dout),      // write data
                .din(cpu_din)         // read data
           );
  // ------------------------------------------------------------------------------------
	// IO_space
	// ------------------------------------------------------------------------------------

	wire PIO_select;
	wire Extension_select;
	wire VIA_select;
  wire ROMBank_select;
	wire VGAIO_select;
	wire IO_wr;
  wire SDcard_select;
  wire [15:0] IO_out;

  reg IO_select;// = 0;

  always @* begin
     IO_select        = (cpu_address[15:12]==4'hB) ? 1 : 0         ; // #BXXX address
  end
	//assign IO_select        = (cpu_address[15:12]==4'hB) ? 1 : 0         ; // #BXXX address
	assign PIO_select       = (cpu_address[11:10]==2'h0) ? IO_select : 0 ; // #B000 - #B3FF is PIO
	assign Extension_select = (cpu_address[11:10]==2'h1) ? IO_select : 0 ; // #B400 - #B7FF is Extension port
	assign VIA_select       = (cpu_address[11:10]==2'h2) ? IO_select : 0 ; // #B800 - #BBFF is VIA
	assign ROMBank_select   = (cpu_address[11:8]==4'hF) ? IO_select : 0 ; // #BF00 - #BFFF is ROMBank_select
	assign VGAIO_select     = (cpu_address[11:8]==4'hD) ? IO_select : 0 ; // #BD00 - #BDFF is VGAIO
  assign SDcard_select    = (cpu_address[15: 4] == 12'hbc0);            // #BC00 - #BC0F is SDcard SPI

  // ------------------------------------------------------------------------------------
	// 	25.5 Input/Output Port Allocations
	//
	// The  8255  Programmable  Peripheral  Interface  Adapter  contains  three
	// 8-bit ports, and all but one of these lines is used by the ATOM.
	//
	// Port A - #B000
	//        Output bits:      Function:
	//             O -- 3     Keyboard row
	//             4 -- 7     Graphics mode
	//
	// Port B - #B001
	//        Input bits:       Function:
	//             O -- 5     Keyboard column
	//               6        CTRL key (low when pressed)
	//               7        SHIFT keys {low when pressed)
	//
	// Port C - #B002
	//        Output bits:      Function:
	//             O          Tape output
	//             1          Enable 2.4 kHz to cassette output
	//             2          Loudspeaker
	//             3          Not used
	//
	//        Input bits:       Function:
	//             4          2.4 kHz input
	//             5          Cassette input
	//             6          REPT key (low when pressed)
	//             7          60 Hz sync signal (low during flyback)
	//
	// The port C output lines, bits O to 3, may be used for user
	// applications when the cassette interface is not being used.
	// ------------------------------------------------------------------------------------

  wire [7:0] piaPortA , PIO_out;
  wire unused,Speaker,CassOutEn,TapeOut;
  reg [3:0] Chigh;//=4'h0;
  always @*
    Chigh = {vga_vsync_out, rept_keyp, 2'b00};

  // interface to Keyboard
  wire [3:0] keyboard_row,graphics_mode;
  assign keyboard_row = piaPortA[3:0];
  assign graphics_mode = piaPortA[7:4];


  /*
  PushButton_Debouncer rstkey(
    .clk(vidclk),
    .PB(key_reset_p),
    .PB_state(kbd_reset));
  */
	// demux key row select
	reg[9:0] key_demux;//=10'h000;


	always@*
	begin
		case (keyboard_row)
		4'h0: key_demux=10'b1111111110;
		4'h1: key_demux=10'b1111111101;
		4'h2: key_demux=10'b1111111011;
		4'h3: key_demux=10'b1111110111;
		4'h4: key_demux=10'b1111101111;
		4'h5: key_demux=10'b1111011111;
		4'h6: key_demux=10'b1110111111;
		4'h7: key_demux=10'b1101111111;
		4'h8: key_demux=10'b1011111111;
		4'h9: key_demux=10'b0111111111;
		default: key_demux=10'b1111111111;
		endcase
	end
  assign key_row = key_demux;
  // ------------------------------------------------------------------------------------
  // PIA8255 at 0xB000
	// ------------------------------------------------------------------------------------

  PIA8255 pia (
    //.clk(clk),
    .cs(PIO_select),
    .reset(cpu_reset),
    .address(cpu_address[1:0]),
    .Din(cpu_dout[7:0]),
    .we(wg),
    .PIAout(PIO_out),
    .Port_A(piaPortA),
    .Port_B({shift_keyp, ctrl_keyp, key_colp}),
    .Port_C_low({unused,Speaker,CassOutEn,TapeOut}),
    .Port_C_high(Chigh)
  );



  wire [7:0] sd_out;
  reg [7:0] via_dout=0;
  assign IO_out = {8'h00, (SDcard_select)? sd_out:
                  ((PIO_select)? PIO_out:8'h00)};


  `pull_up(miso, miso_t, miso_p)
  wire mosi_r;
  assign mosi= mosi_r;

  // ------------------------------------------------------------------------------------
  // SDcard SPI controller at 0xBC00
	// ------------------------------------------------------------------------------------
  spi sdcard
  (
   .clk(spiclk),
   .reset(cpu_reset),
   .enable(SDcard_select),
   .rnw(wg),
   .addr(cpu_address[2:0]),
   .din(cpu_dout[7:0]),
   .dout(sd_out),
   .miso(miso_p),
   .mosi(mosi_r),
   .ss(ss),
   .sclk(sclk)
   );

  // ------------------------------------------------------------------------------------
  // Bootloader
	// ------------------------------------------------------------------------------------

  wire [31:0] flash_data;
  reg [23:0] flash_addr=0;
  reg flash_valid=0;
  wire flash_ready;

  reg [31:0]  flash_copy=0;

  icosoc_flashmem flasmem(
    .clk(clk),
    .resetn(locked),

    .valid( flash_valid),
    .ready( flash_ready),
    .addr( flash_addr),
    .rdata(flash_data),
    .spi_cs( SPI_FLASH_CS),
    .spi_sclk(SPI_FLASH_SCLK ),
    .spi_mosi(SPI_FLASH_MOSI ),
    .spi_miso(SPI_FLASH_MISO)
   );

  // ------------------------------------------------------------------------------------
  // VGA signal generation
	// ------------------------------------------------------------------------------------

  wire [12:0] vga_req_addr;
  wire vga_req;
  wire [3:0] vga_graphics_mode;
  wire [1:0] shadow_vga;
	reg [15:0] vid_data=0;

  wire [5:0] vga_rgb;
	wire vga_hsync_out,vga_vsync_out;

  // latch vga data
  always@(posedge clk) begin
    if (delay) vid_data <=sram_din;
  end

	vga display(
		.clk( clk ),
		.reset( ~locked|kbd_reset|boot),
		.address(vga_req_addr),
		.vid_data(vid_data),
		.settings({shadow_vga,vga_graphics_mode}),
		.rgb(vga_rgb),
		.hsync(vga_hsync_out),
		.vsync(vga_vsync_out),
    .req(vga_req),
    .cs(VGAIO_select),
    .we(wg),
    .cpu_address(cpu_address[3:0]),
    .Din(cpu_dout)
		);


	always@(posedge clk) begin
    red  <= {vga_rgb[5:4],vga_rgb[5:4]};
    green  <= {vga_rgb[3:2],vga_rgb[3:2]};
    blue  <= {vga_rgb[1:0],vga_rgb[1:0]};
		//rgb   <= vga_rgb;
		vsync <= vga_vsync_out;
		hsync <= vga_hsync_out;
		end

// Bootloader statemachine.
`define BL_IDLE 0
`define BL_SETUP 1
`define BL_WAITFLASH 2
`define BL_WRITE1 3
`define BL_WRITE2 4
`define BL_WRITE3 5
`define BL_WRITE4 6
`define BL_DONE 7

  reg [16:0] dma_addr=0;
  reg   [2:0]  bl_state=0;
  wire  [16:0] next_dma_addr = dma_addr + 1;
  wire [23:0] next_flash_addr = flash_addr + 4;
  reg   [15:0] boot_data=0;

  always@(posedge clk) begin
    if (~locked) begin
      boot <= 1;
      flash_valid <=0;
      bl_state <= `BL_SETUP;
      flash_addr <= 24'h40000;
      boot_data <= 16'h0000;
    end
    else
      case (bl_state)
      `BL_IDLE:       begin
                        boot <= 0;
                        //delay <= vga_req;
                        flash_valid <=0;
                        bl_state <= reboot_req ? `BL_SETUP : `BL_IDLE;
                      end
      `BL_SETUP:      begin
                        flash_addr <= 24'h40000;
                        dma_addr <= 17'h8000;
                        flash_valid <=1;
                        boot <= 1;
                        bl_state <= `BL_WAITFLASH;
                      end
      `BL_WAITFLASH:  begin
                        //flash_valid <=0;
                        if (flash_ready) begin
                           flash_copy <= flash_data;
                           flash_addr <= next_flash_addr;
                           //flash_valid <=1;
                           bl_state <= `BL_WRITE1;
                        end
                      end
      `BL_WRITE1:     begin
                        //flash_valid <=0;
                        boot_data <= flash_copy[15:0];
                        bl_state <= `BL_WRITE2;
                      end
      `BL_WRITE2:     begin
                        dma_addr <= next_dma_addr;
                        boot_data <= flash_copy[31:16];
                        bl_state <= `BL_DONE;
                      end
      `BL_DONE:       begin
                        dma_addr <= next_dma_addr;
                        bl_state <= next_dma_addr[16] ? `BL_IDLE : `BL_WAITFLASH;
                      end
      default:        bl_state <=`BL_IDLE;
      endcase
  end


  //assign delay = vga_req;


  wire [16:0] ext_addr = boot?dma_addr:{ shadow_vga[0],3'b100,vga_req_addr};
  wire ext_addr_valid = delay|boot;
  wire shadow_w_vga = (cpu_address[15:13]==3'b100)?shadow_vga[1]:0;

  assign SRAM_A = ext_addr_valid ? { 2'b00,ext_addr} : {2'b00,shadow_w_vga,cpu_address};
  assign SRAM_nCE = 0;
  assign SRAM_nWE =  wg  ;
  assign SRAM_nOE = phi2_we;
  assign SRAM_nLB = 0;
  assign SRAM_nUB = 0;
  assign sram_dout = boot ? boot_data : cpu_dout;

  // cpu reads from RAM or IO.
  assign cpu_din = IO_select ? IO_out : sram_din;

  // debugging leds

  always@(posedge clk)
    if (cpu_reset)
      halt<=0;
    else
     if (cpu_address==16'hfffe) halt<=1;

  assign led1 = boot;
  assign led2 = cpu_write;
  assign led3 = halt;

endmodule
