int * clookup = {0,0,0,0,0,0,0,0};
int * cmask = {0,0,0,0,0,0,0,0};
int vga_state=0;

int set_vga_mode() {
  int * vgamod=0xbd00;
  vgamod[0]=vga_state;
}

int gr_show(int num){
  vga_state = vga_state | 16;
  if (num==0) vga_state = vga_state ^16;
  set_vga_mode();
}
int gr_write(int num){
  vga_state = vga_state | 32;
  if (num==0) vga_state = vga_state ^32;
  set_vga_mode();
}
int gr_mode(int num){
  vga_state = (vga_state & 0xfff0) | num;
  set_vga_mode();
}
void setcolor(int c) {
    for (int i=0; i<8; i++) {
        clookup[7-i]=c;
        c=c<<2;
    }
}
int clg(int num) {
    scrn=0x8000;
    int * vgamod=0xbd00;
    int c=3;
    for (int i=0; i<8; i++) {
        clookup[7-i]=c;
        cmask[7-i]=c^0xffff;
        c=c<<2;
    }
    if (num==4) {
        gr_mode(0xf);
        memset(0,0x8000,0x1800);
    } else {
        gr_mode(0x0);
        cls();
    }
};

int plot(int x,int y) {
    if (((x|y)&0xff00)|(y>191)) return;
    register * vga_address=0x97e0 + (x>>3)-(y<<5);
    register l=x&7;
    register byte=vga_address[0];
    vga_address[0]=(byte & cmask[l]) |clookup[l];
}

int lastx;
int lasty;
int lastcolor=0xffff;


int plotLineHigh(int x0,int y0, int x1,int y1) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    
    register xi = 1;
    
    if (dx < 0) {
        xi = 0xffff;
        dx = x0 - x1;
    }
    register Dy = dy+dy;
    register Dx = dx+dx;
    register D = Dx - dy;
    register x=x0;
    register y=y0;
    
    for(y=y0; y<y1; y=y+1) {
        plot(x,y);
        if (D > 0) {
            x = x + xi;
            D = D - Dy;
        }
        D = D + Dx;
    }
    plot(x,y);
}

int plotLineLow(int x0,int y0, int x1,int y1) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    register yi = 1;
    if (dy < 0) {
        yi = 0xffff;
        dy = y0 - y1;
    }
    register Dy = dy<<1;
    register Dx = dx<<1;
    register D = Dy - dx;
    register y = y0;
    register x;
    for(x=x0; x<x1; x++) {
        plot(x,y);
        if (D > 0) {
            y = y + yi;
            D = D - Dx;
        }
        D = D + Dy;
    }
    plot(x,y);
}

int plotLine(int x0, int y0, int x1, int y1) {
    int dx=x1-x0;
    int dy=y1-y0;
    if (dx<0) dx=x0-x1;
    if (dy<0) dy=y0-y1;
    if (dy < dx)
        if (x0 > x1)
            plotLineLow(x1, y1, x0, y0);
        else
            plotLineLow(x0, y0, x1, y1);
    else if (y0 > y1)
        plotLineHigh(x1, y1, x0, y0);
    else
        plotLineHigh(x0, y0, x1, y1);
}

void move(int x,int y) {
    lastx=x;
    lasty=y;
}

void draw(int x,int y) {
    plotLine(lastx,lasty,x,y);
    lastx=x;
    lasty=y;
}
