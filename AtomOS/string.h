
int memset(int val,int * dest,int len){
  register d=dest;
  register l=len;
  register v=val;
  asm("shr r14,r9,#3");
  asm("beq memset_single_loop");
  asm("memset_loop:")
  asm("sti r8,[r10,#0]");
  asm("sti r8,[r10,#1]");
  asm("sti r8,[r10,#2]");
  asm("sti r8,[r10,#3]");
  asm("sti r8,[r10,#4]");
  asm("sti r8,[r10,#5]");
  asm("sti r8,[r10,#6]");
  asm("sti r8,[r10,#7]");
  asm("add r10,r10,#8");
  //asm("sub r9,r9,#8");
  //asm("shr r14,r9,#3");
  asm("sub r14,r14,#1");
  asm("bne memset_loop");
  asm("memset_single_loop:")
  asm("and r14,r9,#4");
  asm("beq memset_next_two");
  asm("sti r8,[r10,#0]");
  asm("sti r8,[r10,#1]");
  asm("sti r8,[r10,#2]");
  asm("sti r8,[r10,#3]");
  asm("adi r10,r10,#4");
  asm("memset_next_two:")
  asm("and r14,r9,#2");
  asm("beq memset_next_one:");
  asm("sti r8,[r10,#0]");
  asm("sti r8,[r10,#1]");
  asm("adi r10,r10,#2");
  asm("and r14,r9,#1");
  asm("beq memset_next_done");
  asm("sti r8,[r10,#0]");
  asm("memset_next_done:");
}

int memcpy(int * src,int * dest,int len){
  register *d=dest;
  register l=len;
  register t;
  register *s=src;
  if (d==s) return 0;
  if ((d<s)||((s+l)<d)) {
  asm("shr r14,r9,#3");
  asm("beq memcpy_single_loop");
  asm("memcpy_loop:")
  asm("ldi r8,[r7,#0]");
  asm("sti r8,[r10,#0]");
  asm("ldi r8,[r7,#1]");
  asm("sti r8,[r10,#1]");
  asm("ldi r8,[r7,#2]");
  asm("sti r8,[r10,#2]");
  asm("ldi r8,[r7,#3]");
  asm("sti r8,[r10,#3]");
  asm("ldi r8,[r7,#4]");
  asm("sti r8,[r10,#4]");
  asm("ldi r8,[r7,#5]");
  asm("sti r8,[r10,#5]");
  asm("ldi r8,[r7,#6]");
  asm("sti r8,[r10,#6]");
  asm("ldi r8,[r7,#7]");
  asm("sti r8,[r10,#7]");
  asm("add r10,r10,#8");
  asm("add r7,r7,#8");
  asm("sub r14,r14,#1");
  asm("bne memcpy_loop");
  asm("memcpy_single_loop:")
  asm("and r14,r9,#4");
  asm("beq memcpy_next_two");
  asm("ldi r8,[r7,#0]");
  asm("sti r8,[r10,#0]");
  asm("ldi r8,[r7,#1]");
  asm("sti r8,[r10,#1]");
  asm("ldi r8,[r7,#2]");
  asm("sti r8,[r10,#2]");
  asm("ldi r8,[r7,#3]");
  asm("sti r8,[r10,#3]");
  asm("adi r10,r10,#4");
  asm("adi r7,r7,#4");
  asm("memcpy_next_two:")
  asm("and r14,r9,#2");
  asm("beq memcpy_next_one:");
  asm("ldi r8,[r7,#0]");
  asm("sti r8,[r10,#0]");
  asm("ldi r8,[r7,#1]");
  asm("sti r8,[r10,#1]");
  asm("adi r10,r10,#2");
  asm("adi r7,r7,#2");
  asm("and r14,r9,#1");
  asm("beq memcpy_next_done");
  asm("ldi r8,[r7,#0]");
  asm("sti r8,[r10,#0]");
  asm("memcpy_next_done:");
  } else while (len) {
    d[len]=s[len];
    len --;
  }
}

int memcmp(int * src,int * dest,int len){
  register *d=dest;
  register l=len;
  register t;
  register *s=src;
  if (d==s) return 1;
  while (len) {
    if (d[len]^s[len]) return 0;
    len --;
  }
  return 1;
}

class String {
    wchar s[256];
    int len;

    void addchar(wchar t) {
      if (len<254) {
        s[len]=t;
        len++;
        s[len]=0;
      }
    }
    void delchar() {
      if (len>0) {
        s[len]=0;
        len--;
      }
    }
    
    int cmp(wchar *t) {
      int index=0;
      while(t[0]){
        if (index>=len) return 0;
        if (t[0]!=s[index]) return 0;
        index++;
        t++;
      }
      return 1;
    }
    void add(wchar *t) {
        while((t[0]>0)&&(len<255)) {
            s[len]=t[0];
            len++;
            t++;
        }
        s[len]=0; // null terminate.
    }
    void adds(String &ref) {
      int t=0;
        while((ref.s[t]>0)&&(len<255)) {
            s[len]=ref.s[t];
            len++;
            t++;
        }
        s[len]=0; // null terminate.
    }
    void addint(int t) {
      if (t&0x8000) {
          addchar(0x2d);
          t=(t^0xffff)+1;
      };
      if (t>0) {
          int digit=0;
          if(t>9999) {
              digit=0x30;
              if(t>19999) {
                  digit+=2;
                  t-=20000;
              };
              if(t>9999) {
                  digit+=1;
                  t-=10000;
              };
              addchar(digit);
          };
          for (int j=0; j<4; j++) {
              if(digit) digit=0x30;
              if(t>999) {
                  digit=0x30;
                  if(t>7999) {
                      digit+=8;
                      t-=8000;
                  };
                  if(t>3999) {
                      digit+=4;
                      t-=4000;
                  };
                  if(t>1999) {
                      digit+=2;
                      t-=2000;
                  };
                  if(t>999) {
                      digit+=1;
                      t-=1000;
                  };
              };
              if (digit)addchar(digit);
              t=((t<<2)+t)<<1; // t = t * 10
          };
      } else addchar(0x30);
    }

    void clear() {
        len=0;
        s[len]=0;
    }
    
}

  
