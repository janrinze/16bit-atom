// IO handling keys and screen

// ------------------------------------------------------------------------------------
// 	25.5 Input/Output Port Allocations
//
// The  8255  Programmable  Peripheral  Interface  Adapter  contains  three
// 8-bit ports, and all but one of these lines is used by the ATOM.
//
// Port A - #B000
//        Output bits:      Function:
//             O -- 3     Keyboard row
//             4 -- 7     Graphics mode
//
// Port B - #B001
//        Input bits:       Function:
//             O -- 5     Keyboard column
//               6        CTRL key (low when pressed)
//               7        SHIFT keys {low when pressed)
//
// Port C - #B002
//        Output bits:      Function:
//             O          Tape output
//             1          Enable 2.4 kHz to cassette output
//             2          Loudspeaker
//             3          Not used
//
//        Input bits:       Function:
//             4          2.4 kHz input
//             5          Cassette input
//             6          REPT key (low when pressed)
//             7          60 Hz sync signal (low during flyback)
//
// The port C output lines, bits O to 3, may be used for user
// applications when the cassette interface is not being used.
//
// ------------------------------------------------------------------------------------

wchar * key_lookup = " 3-GQ!X  2,FPZX  1;EOYX  0*DNXX  \b9CMWW ^C8BLVV ]\n7AKUU \\ 6@JTT [ 5/ISS   4.HRR";
int * pressedkeys = {0,0,0,0,0,0,0,0,0,0};
int last_key_row=0;
int key_scan() {
    int j;
    int ret=0;
    int * key_column = 0xb001;
    int * key_row = 0xb000;
    int * key_tab = key_lookup;
    int pressed;
    int changed;
    int last_pressed;
    last_pressed = pressedkeys[last_key_row];
    key_lookup[33]=127;
    key_lookup[5]=27;
    
    for (j=0; j<1000; j++);
    
    key_tab = key_tab + (last_key_row<<3);

	pressed = key_column[0];
	
	pressedkeys[last_key_row]=pressed;
	changed = (pressed ^ last_pressed);
	pressed = pressed ^ 255;
	changed = changed & pressed;
	
	last_key_row = last_key_row +1;
	if (last_key_row>9) last_key_row = 0;
	key_row[0]=last_key_row;
	
	if (changed&63) {
		int ctrl = pressed&0x40;
		int shift = pressed&0x80;
		int bit=1;
		int k;
		for (k=0; k<6; k++) {
			if (changed & 1) {
				int key = key_tab[k];
				if (ctrl) key=key&0x3f;
				if (shift) {
					if (key >0x20) {
					  if (key <0x40) key = key^16;
					  if (key >0x3f) key = key+32;
					}
				}
				
				return key;
			}
			changed = changed >> 1;
		}
	}

    return 0;
}

int get_key(int timeout){
  int k = key_scan();
  if(timeout)
    while((k==0)&&(timeout>0)) {
              k=key_scan();
              timeout--;
    }
  return k;
}

int key_input(){
   
   //int k=1;
   //for (int i=0;(i<100)&&(k>0);i++)
    //  k=key_scan();
   int k=0;
   while (k<1) {
      cursor.toggle();
      k=get_key(800);
   }
   cursor.off();
   printchar(k);
   return k;
}

int sinput(String & str){
  str.clear();
  int k=0;
  while(true) {   
    k=key_input();
    switch (k){
      case 10:  printchar(13);return 1;
      case 127: if (str.len>0) {
                    str.delchar();
                };
                break;
      case 27:  str.clear();
                return 0;
      default:  str.addchar(k);
    }
  }
}
