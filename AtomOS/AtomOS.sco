/* test */

include("stdio.h");
include("Demo.h");

//include("Test.h");

wchar * welcometxt = "ACORN ATOM\n16 BIT RISC\n128K\n";

int main(int argc,char ** argv) {
  String cmdline;
  Demo dm;

  dm.init();

  cls();
  print(welcometxt);

  while(true) {
    printchar('>');
    cmdline.clear();
    if (sinput(&cmdline)) {
      cursor.off();
      if (cmdline.cmp("DEMO"))
          dm.demo(&cmdline);
          
      //else if (cmdline.cmp("TEST")) test.demo(&cmdline);
    }
  }
}
