
print_nibble_hex_number:
  push lr,sp
  push r0,sp
  
  lit lr,#15
  and r0,r0,lr
  
  lit lr,#'0'
  add r0,r0,lr
  
  lit lr,'9'
  sub lr,lr,r0
  bpl  char_print_nibble_hex_number
  
  lit lr,#7
  add r0,r0,lr
  
char_print_nibble_hex_number:
  jsr print_char
  
end_print_nibble_hex_number:
  pop r0,sp
  pop pc,sp
  

print_hex_number:
  push lr,sp
  push r0,sp
  push r1,sp
  orr r1,r0,r0
  
  shr r0,r1,#12
  jsr print_nibble_hex_number
  shr r0,r1,#8
  jsr print_nibble_hex_number
  shr r0,r1,#4
  jsr print_nibble_hex_number
  shr r0,r1,#0
  jsr print_nibble_hex_number
  
  pop r1,sp
  pop r0,sp
  pop pc,sp

tenthousand:
.2710

print_number:
  push lr,sp
  push r1,sp
  lit r1,#0
  jsr   _print_number
  pop r1,sp
  pop pc,sp

_print_number:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  
  shl r2,r1,#4
  
  lit r5,#4
  orr r1,r0,r0
  beq print_zero
  
  bpl positive_number

  lit r0,#x2d
  jsr print_char // print '-'

  lit r0,#0
  sub r1,r0,r1
  
positive_number:
  orr r0,r2,r2
  
  load r2,tenthousand
  shl r4,r2,#1
  
  sub lr,r1,r4
  bmi skip_positive_twenty_thousand

  lit r3,#x32
  orr r0,r0,r3
  sub r1,r1,r4

skip_positive_twenty_thousand:

  
  sub lr,r1,r2
  bmi next_digit_to_print
  
  lit r3,#x31  
  sub r1,r1,r2
  orr r0,r0,r3
  
next_digit_to_print:

  orr r0,r0,r0
  beq skip_digit_to_print
  jsr print_char

skip_digit_to_print:

  // r1 is in thousands now.
  lit lr,#x30
  and r0,r0,lr
  
  lit r2,#125
  shl r2,r2,#6
  
  sub lr,r1,r2
  bmi check_digit_bit_three
  lit r3,#x38
  orr r0,r0,r3
  sub r1,r1,r2
check_digit_bit_three:
  shr r2,r2,#1
  sub lr,r1,r2
  bmi check_digit_bit_two
  lit r3,#x34
  orr r0,r0,r3
  sub r1,r1,r2
check_digit_bit_two:
  shr r2,r2,#1
  sub lr,r1,r2
  bmi check_digit_bit_one  
  lit r3,#x32
  orr r0,r0,r3
  sub r1,r1,r2  
check_digit_bit_one:
  shr r2,r2,#1
  sub lr,r1,r2
  bmi check_digit_bit_zero  
  lit r3,#x31
  orr r0,r0,r3
  sub r1,r1,r2  
check_digit_bit_zero:
  shl r3,r1,#2
  add r1,r1,r3
  shl r1,r1,#1        // r1=r1*10
  lit r2,#1
  sub r5,r5,r2
  bne next_digit_to_print
print_zero:
  lit r1,#x30
  orr r0,r0,r1
  jsr print_char
end_print_number:
  pop r5,sp
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp
