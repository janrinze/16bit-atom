//--------------------------------------------------------------------
// cmp_wstr_wstr:
// input:
//  r0 : pointer to string 1
//  r1 : pointer to string 2
// output:
//  r0 : unchanged if not found, else points to next char in string 1
//  r1 : 0 if not found, else strlen(string 2)
//--------------------------------------------------------------------
cmp_wstr_wstr:
  push lr,sp
  push r2,sp
  push r3,sp

  #if DEBUG
  push r0,sp
  orr r0,r1,r1
  jsr print_wstring
  lit r0,#10
  jsr print_char
  pop r0,sp
#endif

  lit r3,#0
next_cmp_wstr_wstr:
  ldr lr,[r1,r3]
  beq done_cmp_wstr_wstr
  ldr r2,[r0,r3]
  orr r0,r0,r0
  eor r2,r2,lr
  bne notfound_cmp_wstr_wstr
  lit lr,#1
  add r3,r3,lr
  bne next_cmp_wstr_wstr
  
notfound_cmp_wstr_wstr:
  lit r3,#0

done_cmp_wstr_wstr:
  add r0,r0,r3
  orr r1,r3,r3
  pop r3,sp
  pop r2,sp
  pop pc,sp
//--------------------------------------------------------------------
// wstr_len:
// input:
//  r0 : pointer to string 
// output:
//  r0 : unchanged
//  r1 : strlen(string)
//--------------------------------------------------------------------
wstr_len:
  push lr,sp
  orr r1,r0,r0
  lit lr,#1
  sub r1,r1,lr
next_wstr_len:
  pop lr,r1
  bne next_wstr_len
  sub r1,r1,r0
  lit lr,#1
  add r1,r1,lr
  pop pc,sp
  
//--------------------------------------------------------------------
// skip white space:
// input:
//  r0 : pointer to string
// output:
//  r0 : first non white character in string
//--------------------------------------------------------------------

skip_white_space:
  push lr,sp
  push r1,sp
  
next_white_space:
  lit lr,#0
  ldr lr,[r0,lr]
  beq end_skip_white_space
  
  lit r1,#x20      // space
  sub r1,r1,lr
  bmi end_skip_white_space
  lit lr,#1
  add r0,r0,lr
  bne next_white_space
end_skip_white_space:
  pop r1,sp
  pop pc,sp

//-------------------------------------------------------------------
// move_mem
// input:
//   r0:dest address
//   r1:src address
//   r2:len in words
//-------------------------------------------------------------------
#if DEBUG
s_move_mem:
."movemem\n"
#endif
move_mem:
  push lr,sp
  push r0,sp
  push r1,sp
  push r2,sp
  push r3,sp

#if DEBUG  
  push r0,sp
  jsr print_hex_number
  orr r0,r1,r1
  jsr print_hex_number
  orr r0,r2,r2
  jsr print_hex_number
  adr r0,s_move_mem
  jsr print_string
  pop r0,sp
#endif
  
  orr lr,r2,r2
  beq end_move_mem
  
  sub lr,r1,r0
  bmi backward_move_mem
  
  lit r3,#0
forward_move_mem:
  ldr lr,[r1,r3]
  str lr,[r0,r3] 
  lit lr,#1
  add r3,r3,lr
  eor lr,r3,r2
  bne forward_move_mem
  beq end_move_mem

backward_move_mem:
  lit lr,#1
  sub r2,r2,lr
next_backward_move_mem:
  ldr lr,[r1,r2]
  str lr,[r0,r2]
  lit lr,#1
  sub r2,r2,lr
  bpl next_backward_move_mem
  
end_move_mem:  
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop r0,sp
  pop pc,sp
  
