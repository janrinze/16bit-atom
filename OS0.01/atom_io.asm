  // IO handling keys and screen

  // ------------------------------------------------------------------------------------
	// 	25.5 Input/Output Port Allocations
	// 
	// The  8255  Programmable  Peripheral  Interface  Adapter  contains  three
	// 8-bit ports, and all but one of these lines is used by the ATOM.
	// 
	// Port A - #B000
	//        Output bits:      Function:
	//             O -- 3     Keyboard row
	//             4 -- 7     Graphics mode
	// 
	// Port B - #B001
	//        Input bits:       Function:
	//             O -- 5     Keyboard column
	//               6        CTRL key (low when pressed)
	//               7        SHIFT keys {low when pressed)
	// 
	// Port C - #B002
	//        Output bits:      Function:
	//             O          Tape output
	//             1          Enable 2.4 kHz to cassette output
	//             2          Loudspeaker
	//             3          Not used
	// 
	//        Input bits:       Function:
	//             4          2.4 kHz input
	//             5          Cassette input
	//             6          REPT key (low when pressed)
	//             7          60 Hz sync signal (low during flyback)
	// 
	// The port C output lines, bits O to 3, may be used for user
	// applications when the cassette interface is not being used.
  // 
  // key store: 0xe000
	// ------------------------------------------------------------------------------------

 
set_cursor:
  push lr,sp
  push r0,sp
  push r1,sp
  push r2,sp
  
  load r2,display
  load r1,cursor
  ldr lr,[r2,r1]
  
  shl lr,lr,#9
  shr lr,lr,#9

  shl r0,r0,#7

  orr lr,lr,r0

  str lr,[r2,r1]

  pop r2,sp
  pop r1,sp
  pop r0,sp
  pop pc,sp
    

last_key_row:
  .0000

key_store:
  .e000
  
key_board:
    push lr,sp
    push r1,sp
    push r2,sp
    push r3,sp
    push r4,sp
    
    
    lit r1,#1
    shl r0,r1,#12
    
key_delay:
    sub r0,r0,r1
    bne key_delay
    
    
    
    load r1,last_key_row
    shl r1,r1,#12
    shr r1,r1,#12
    
    lit r3,#xb0
    shl r3,r3,#8
    pop r3,r3              // fetch key column
    
    load r0,key_store
    ldr r2,[r0,r1]         // get saved value 
    str r3,[r0,r1]         // save key column
    
    eor r2,r2,r3           // find the key that changed
    
    lit r0,#255
    eor r0,r3,r0
    and r3,r2,r0           // this holds a 1 for each newly pressed key
    lit r0,#63
    and r0,r0,r3           // filter ctrl/shift
    beq end_key_board

    
    // free : r3,r4,r2,lr
    // inuse: r0 (pressed keys) r1 (key row)
    
    lit r2,#1
    lit r3,#0
    sub r3,r3,r2
    shl r0,r0,#1
next_key_bit:
    shr r0,r0,#1
    add r3,r3,r2
    and r4,r0,r2
    beq next_key_bit

    // r3 is index for bit
    lit r2,#xe1
    shl r4,r1,#3
    shl r2,r2,#8
    add r2,r2,r4
    ldr r0,[r2,r3]  // translated key.
    
    lit r2,#255
    and r0,r0,r2

    lit r3,#xb0
    shl r3,r3,#8
    pop r3,r3              // fetch key column

    lit r2,64
    and r2,r3,r2           // shift pressed?
    bne check_shift_pressed
    lit r2,#31
    and r0,r0,r2
    bpl end_key_board

check_shift_pressed:
    lit r2,128
    and r2,r3,r2           // shift pressed?
    bne end_key_board
    
    lit r2,#xe0
    and r2,r2,r0
    beq end_key_board     // ignore control chars
    
    lit r2,#x60
    sub r2,r2,r0
    bmi end_key_board     // only chars that we can type.
    
    lit r2,#xEF           // no shift for '0' and space
    and r2,r2,r0
    beq end_key_board
    
    lit r2,#x3f
    sub r2,r2,r0
    bmi to_uppercase_key
    
    lit r2,#x10
    eor r0,r0,r2
    bne end_key_board
    
to_uppercase_key:
    lit r2,#x20
    add r0,r0,r2
    
end_key_board:
    
    lit r2,#1             // advance key row.
    add r1,r1,r2
    lit r2,#9
    sub r2,r2,r1
    bpl store_key_row_val
    lit r1,#0
store_key_row_val:
    
    lit r2,#0
    adr r4,last_key_row
    str r1,[r4,r2]
    lit r3,#xb0
    shl r3,r3,#8
    str r1,[r3,r2]
    
    pop r4,sp
    pop r3,sp
    pop r2,sp
    pop r1,sp
    pop pc,sp
        
// print_string:
// print all characters read from address at r0 until char 0 is found.

print_string:
    push lr,sp
    push r0,sp
    push r1,sp
    mov r1,r0
next_char:
    lit r0,#0
    ldr r0,[r1,r0]
    lit lr,#255
    and lr,lr,r0
    beq end_print_string
    jsr print_char
    shr r0,r0,#8
    beq end_print_string
    jsr print_char
    lit lr,#1
    add r1,r1,lr
    bne next_char
end_print_string:
    pop r1,sp
    pop r0,sp
    pop pc,sp

    
to_vdu:
    .bfbf

backspace:
  push lr,sp
  push r0,sp
  
  load r0,cursor
  beq end_backspace
  
  lit lr,#1
  sub r0,r0,lr
  adr lr,cursor
  push r0,lr

end_backspace:
  pop r0,sp
  pop pc,sp

linefeed:
  push lr,sp
  push r0,sp
  
  // advance cursor to next line
  load r0,cursor
  
  lit lr,#32
  add r0,r0,lr
  shr r0,r0,#5
  shl r0,r0,#5
  
  // next line outside screen?
  shr lr,r0,#9

  beq no_scroll
  
  jsr scroll_up

  lit r0,#15
  shl r0,r0,#5

no_scroll:
  adr lr,cursor 
  push r0,lr
  
  pop r0,sp
  pop pc,sp

display:
    .8000
cursor:
    .0000
twospaces:
    .2020

cls:
  push lr,sp
  push r9,sp
  push r10,sp
  push r11,sp
  push r12,sp
  
  adr r9,cursor
  lit r10,#0
  push r10,r9
  load r9,display
  load r10,twospaces
  lit r11,#1
  shl r11,r11,#9
  lit r12,#1
cls_loop:
  sub r11,r11,r12
  str r10,[r9,r11]
  bne cls_loop
  pop r12,sp
  pop r11,sp
  pop r10,sp
  pop r9,sp
  pop pc,sp

scroll_up:
  push lr,sp
  push r0,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  
  load lr,display  // lr = display
  lit r0,#32
  add r0,lr,r0     // r2 = address of next line

  lit r1,#xf
  shl r1,r1,#5
  lit r2,#0      
  lit r3,#1
  
scroll_loop:
  ldr r4,[r0,r2]
  str r4,[lr,r2]
  add r2,r2,r3
  sub r4,r1,r2
  bne scroll_loop

  lit r0,#32       // space char
clear_bottom_loop:
  str r0,[lr,r2]
  add r2,r2,r3
  shr r4,r2,#9
  beq clear_bottom_loop

  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop r0,sp
  pop pc,sp


print_wstring:
    push lr,sp
    push r0,sp
    push r1,sp
    orr r1,r0,r0
next_print_wstring:
    lit r0,#0
    ldr r0,[r1,r0]
    beq end_print_string
    jsr print_char
    lit r0,#1
    add r1,r1,r0
    bne next_print_wstring
end_print_wstring:   
    pop r1,sp
    pop r0,sp
    pop pc,sp

print_char:
    push lr,sp
    push r0,sp
    push r1,sp
    
    lit lr,#255         // 8 bit only
    and r0,r0,lr
    
    beq end_print_char  // not NULL
    bmi end_print_char  // ascii is 7 bit.

    shr lr,r0,#5
    bne not_control_char // <32 then control char
    
    adr lr,end_print_char
    orr r0,r0,r0
    adr r1,control_char_handlers
    orr r0,r0,r0
    ldr pc,[r1,r0]        // jsr handler[char] 
    
not_control_char:
    lit r1,#x60
    sub r1,r0,r1
    bmi not_uppercase_print_char
    lit r1,#x20
    add r0,r0,r1
    bne vdu_converted_char
not_uppercase_print_char:
    lit lr,#xbf
    and r0,r0,lr        // convert to vdu.
vdu_converted_char:
    load r1,cursor  
    shr lr,r1,#9
    
    bne end_print_char  // invalid cursor pos.
    
    load lr,display
    str r0,[lr,r1]      // output to screen at cursor pos
    
    lit lr,#1
    add r1,lr,r1
    shr lr,r1,#9        // at end of screen? auto line feed.
    beq skip_linefeed
    jsr linefeed
    jmp end_print_char
    
skip_linefeed:
    store r1,cursor
    
end_print_char:
    pop r1,sp
    pop r0,sp
    pop pc,sp

back:
  

cursor_on:
cursor_off:
toggle_cursor:
htab:
back:
forward:
crreturn:
beep:
null:
  rts

//
// print_control_char:
// r0 has control character.
//

control_char_handlers:
.null
.cursor_on
.cursor_off
.toggle_cursor

.null
.null
.null
.beep

.backspace
.htab
.linefeed
.forward

.cls
.crreturn
.null
.null

.null
.null
.null
.null

.null
.null
.null
.null

.null
.null
.null
.null

.null
.null
.null
.null


@e100
// key trans table
/*
."  33--GGQQ!!XXX"
."  22,,FFPPZZXXX"
."  11;;EEOOYYXXX"
."  00::DDNNXXXXX"
."  \b\b99CCMMWWWWW"
."  CC88BBLLVVVVV"
."]]\n\n77AAKKUUUUU"
."\\\\  66@@JJTTTTT"
."[[  55//IISSSSS"
."    44..HHRRR"
*/
.w" 3-GQ!X"
.w" 2,FPZX"
.w" 1;EOYX"
.w" 0:DNXX"
.w" \b9CMWW"
.w"^C8BLVV"
.w"]\n7AKUU"
.w"\\ 6@JTT"
.w"[ 5/ISS"
.w"  4.HRR"

