

//-------------------------------------------------------------------
// Command lookup lists
//-------------------------------------------------------------------


print_command:
.do_print
.w"PRINT"

pdot_command:
.do_print
.w"P."

cls_command:
.cls
.w"CLS"

if_command:
.do_if
.w"IF"

run_command:
.do_run
.w"RUN"

ldot_command:
.list_basic
.w"L."
list_command:
.list_basic
.w"LIST"
goto_command:
.do_run
.w"GOTO"
gdot_command:
.do_run
.w"G."
for_command:
.do_for
.w"FOR"
dnext_command:
.do_next
.w"NEXT"
ndot_command:
.do_next
.w"N."
fdot_command:
.do_for
.w"F."
colour_command:
.do_color
.w"COLOR"
plot_command:
.do_gr_plot
.w"PLOT"
move_command:
.do_gr_move
.w"MOVE"
do_command:
.do_cmd
.w"DO"
until_command:
.do_until
.w"UNTIL"
duntil_comand:
.do_until
.w"U."
key_command:
.do_key
.w"KEY"
clear_command:
.do_clear
.w"CLEAR"
draw_command:
.drawline
.w"DRAW"

commands:
.ndot_command
.duntil_comand
.draw_command
.fdot_command
.ldot_command
.pdot_command
.gdot_command
.dnext_command
.until_command
.print_command
.cls_command
.if_command
.run_command
.plot_command
.move_command
.colour_command
.goto_command
.for_command
.do_command
.key_command
.clear_command
.0000

for_next_stack:
.2700

//-------------------------------------------------------------------
// do_run
// input:
//   r0: string  (ignored but can have <exp>,<exp>...)
//-------------------------------------------------------------------
#if DEBUG
debug_run:
."run"
#endif
err_run:
."LINE\n"
back_to_cmdline:
.restart_command_line_input
restore_stack:
.7fff
do_run:
  load sp,restore_stack
  load lr,back_to_cmdline
  push lr,sp
  
  jsr skip_white_space
  orr r2,r0,r0
  jsr eval_expression
  eor r2,r2,r0
  beq run_from_start
  orr r3,r1,r1
  orr r0,r1,r1
  jsr find_line
  eor r3,r3,r2
  beq do_next_line_run
  adr r0,err_run
  jsr print_string
  jsr end_do_run
run_from_start:
  lit r0,#xf0
  shl r0,r0,#8
  lit lr,#0
  ldr r0,[r0,lr]
  load r12,for_next_stack
do_next_line_run:
#if DEBUG
  push r0,sp
  lit lr,#0
  ldr r0,[r0,lr]
  jsr print_hex_number
  adr r0,debug_run
  jsr print_string
  pop r0,sp
#endif

  lit lr,#0
  ldr r1,[r0,lr]
  eor r1,r1,&ffff
  shl r1,r1,#1
  beq end_do_run
  
  lit lr,#1
  add r0,r0,lr
  
next_cmd_do_run:
  jsr interpret_cmd
  jsr skip_white_space
  lit lr,#0
  ldr r1,[r0,lr]
  beq prep_for_next_line
  lit lr,#10
  eor lr,lr,r1
  bne next_cmd_do_run
  lit lr,#1
  add r0,r0,lr  
prep_for_next_line:  
  lit lr,#1
  add r0,r0,lr
  bne  do_next_line_run
  
end_do_run:
  
  jsr restart_command_line_input 
  
perror:
."ERROR: "
  
basic_error:
  jsr print_string
  lit r0,#10
  jsr print_char
  load sp,restore_stack
  jsr restart_command_line_input 

//--------------------------------------------------------------------
// interpret_cmd:
// input:
//  r0 : string to commands
// output:
//  r0 : pointer to last unprocessed char in string
//  r1 : error code (0 if no error)
//--------------------------------------------------------------------
text_assignment:
."ASSIGNMENT\n"
text_mrcmds:
."morecmds\n"
interpret_cmd:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp

  // r0 is string to a command.

more_cmds:
#if DEBUG
  push r0,sp
  jsr print_hex_number
  adr r0,text_mrcmds
  jsr print_string
  pop r0,sp
#endif
  jsr skip_white_space
  
#if DEBUG
  push r0,sp
  jsr print_hex_number  
  pop r0,sp 
#endif

  lit lr,#0
  ldr r1,[r0,lr]
  beq end_interpret_cmd
  
#if DEBUG
  push r0,sp
  orr r0,r1,r1
  jsr print_hex_number  
  pop r0,sp
#endif

  lit r3,#10
  eor r3,r1,r3
  beq end_interpret_cmd
  
  lit r3,#'!'
  eor r3,r1,r3
  bne cmd_check_is_variable
  
#if DEBUG
  push r0,sp
  adr r0,text_assignment
  jsr print_string
  pop r0,sp
#endif
    
  lit lr,#1
  add r0,r0,lr
  
  orr r2,r0,r0
  jsr get_operand

  
#if DEBUG
  push r0,sp
  orr r0,r1,r1
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
#endif
  
  eor r2,r2,r0
  
  bne cmd_is_assignment
  
  beq cmd_exit_with_error
    
cmd_check_is_variable:  
  jsr get_variable_address
  orr r1,r1,r1
  bne cmd_is_assignment
  beq not_an_assignment


cmd_is_assignment:
  // assignment of variable or poke.
  jsr skip_white_space
  
  lit lr,#0
  ldr lr,[r0,lr]
  beq end_interpret_cmd
  lit r3,#'='
  eor lr,lr,r3
  bne end_interpret_cmd
  
  lit lr,#1
  add r0,r0,lr
  jsr skip_white_space
  orr r3,r1,r1
  orr r2,r0,r0
  jsr eval_expression
  eor r2,r2,r0
  beq end_interpret_cmd
  
  lit lr,#0
  str r1,[r3,lr]
  eor r1,r1,r1
  beq more_cmds
  
not_an_assignment: 

  lit lr,#0
  ldr lr,[r0,lr]
  beq end_interpret_cmd  // EOL then end
  lit r3,#';'
  eor lr,lr,r3
  bne check_for_command
  lit lr,#1
  add r0,r0,lr
  bne more_cmds
  beq end_interpret_cmd
  
check_for_command:  
  adr r2,commands
  lit r3,#0
check_next_command:
  ldr r1,[r2,r3]
  beq command_not_found
  
  lit lr,#0
  ldr r4,[r1,lr]         // get pointer to function
  lit lr,#1
  add r1,r1,lr           // pointer to command keyword
   
  jsr cmp_wstr_wstr
  
  orr r1,r1,r1
  bne command_found
  lit lr,#1
  add r3,r3,lr
  bne check_next_command

command_not_found:
  orr r1,r0,r0
  jsr do_assignment
  eor r1,r0,r1
  bne more_cmds

cmd_exit_with_error:
  adr r0,cmd_err_txt
  jsr basic_error
end_interpret_cmd:
#if DEBUG
  push r0,sp
  adr r0,txt_endintrp
  jsr print_string
  pop r0,sp
#endif  
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp

txt_endintrp:
."endinterp\n"

command_found:
  adr lr,more_cmds
  orr pc,r4,r4

do_assignment:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  
  jsr skip_white_space
  
  lit lr,#0
  ldr lr,[r0,lr]         // get first char
  beq end_do_assignment
  
  // test '!'
  lit r1,#'!'
  eor r1,r1,lr
  bne do_assignment_check_variable
  
  lit lr,#1
  add r0,r0,lr

  lit lr,#0
  ldr lr,[r0,lr]         // get first char
  beq end_do_assignment
  
  orr r3,r0,r0
  jsr eval_expression
  eor r3,r3,r0
  beq end_do_assignment
  bne destination_is_set
  
do_assignment_check_variable:  
  jsr get_variable_address // r0 text, r1 address.
  orr r1,r1,r1
  beq end_do_assignment
  
  // check if bracket etc..
  
  

  
destination_is_set: 
  orr r2,r1,r1          // keep first evaluation in r2
  
  jsr skip_white_space
  
  lit lr,#0
  ldr lr,[r0,lr]         // get first char
  beq end_do_assignment

  // test '='
  lit r1,#'='
  eor r1,r1,lr
  bne end_do_assignment
  
  lit lr,#1
  add r0,r0,lr
  lit lr,#0
  ldr lr,[r0,lr]         // get first char
  beq end_do_assignment
  
  orr r3,r0,r0
  jsr eval_expression
  eor r3,r3,r0
  beq end_do_assignment
  
  lit lr,#0
  str r1,[r2,lr]
  
end_do_assignment:
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp


  
cmd_err_txt:
."COMMAND NOT FOUND"


//--------------------------------------------------------------------
// find_line:
// input r0: line to search for.
//
// output:
//  r0 : address of line if found, else address of next line
//  r1 : line length
//  r2 : line number
//--------------------------------------------------------------------
find_line:
  push lr,sp
  push r3,sp
  
  orr r3,r0,r0
  load r0,basic_start
  
next_find_line:
  jsr wstr_len  
  lit lr,#0
  ldr r2,[r0,lr]
  sub lr,r2,r3
  bpl done_find_line
  

  add r0,r0,r1
  bne next_find_line
  
done_find_line:
  pop r3,sp
  pop pc,sp
  
//-------------------------------------------------------------------
// print_basic_line
// input:
//   r0: basic line
//-------------------------------------------------------------------

print_basic_line:
 push lr,sp
 push r0,sp
 push r0,sp
 
 lit lr,#0
 ldr r0,[r0,lr]
 jsr print_number
 pop r0,sp
 lit lr,#1
 add r0,r0,lr
 jsr print_wstring
 
end_print_basic_line:
 pop r0,sp
 pop pc,sp
 
//-------------------------------------------------------------------
// list_basic
// input:
//   r0: string  ( optional begin,end )
//-------------------------------------------------------------------

list_basic:
  push lr,sp
  push r0,sp
  push r1,sp
  push r2,sp
  push r3,sp
  
  lit r3,#0
  eor r3,r3,&ffff
  shr r3,r3,#1
  
  lit r2,#x29
  shl r2,r2,#8
  
next_list_line:
  lit lr,#0
  ldr r0,[r2,lr]
  eor lr,r3,r0
  beq end_list_basic
  jsr print_number
  lit lr,#1
  add r2,r2,lr
  orr r0,r2,r2
  jsr print_wstring
  jsr wstr_len
  lit r0,#10
  jsr print_char
  add r2,r2,r1
  //load lr,basic_top
  //sub lr,lr,r2
  bne next_list_line
  
end_list_basic:
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop r0,sp
  pop pc,sp
   
//-------------------------------------------------------------------
// insert_basic_line
// input:
//   r0: string
//-------------------------------------------------------------------



ins_bas:
."INSBAS\n"  
insert_basic_line:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  push r6,sp
  push r7,sp
  
  orr r7,r0,r0
  orr r5,r1,r1
  
  beq end_insert_basic_line
  bmi end_insert_basic_line
  

    
  // set line number.
  lit lr,#1
  sub r7,r7,lr
  lit lr,#0
  str r5,[r7,lr]
  lit lr,#1
  add r0,r7,lr
  jsr wstr_len
  lit lr,#1
  add r4,r1,lr



  
  // r7=string ,r5=lineno,  r4=strlen+1 (for lineno)
  
  orr r0,r5,r5
  jsr find_line    // r0=address, r1= len , r2=lineno
  
  // empty line?
  lit lr,#1
  ldr lr,[r7,lr]
  beq remove_basic_line
  
  eor lr,r2,r5
  beq replace_line
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif   
  // make room for inserted line.
  load r3,basic_top
  
  add r6,r3,r4
  store r6,basic_top
  

  
  sub r2,r3,r0
  
  orr r1,r0,r0
  
  add r0,r0,r4
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif  
  
  jsr move_mem
  orr r0,r1,r1
  orr r1,r7,r7
  orr r2,r4,r4
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif 
    
  jsr move_mem  
  jmp end_insert_basic_line


replace_line:
  // r7=string ,r5=lineno,  r4=strlen+1 (for lineno)
  // r0=address, r1= len , r2=lineno
  
  // line previously sized: r1
  // line now sized r4
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif  
  
  eor lr,r4,r1
  beq same_line_size
     
  push r0,sp

  sub r6,r4,r1
  add r1,r0,r1
  add r0,r0,r4
    
  load r2,basic_top
  sub r2,r2,r1
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif  
   
  jsr move_mem
  
  load r2,basic_top
  add r2,r2,r6
  store r2,basic_top
  pop r0,sp
same_line_size:  
  
  orr r1,r7,r7
  orr r2,r4,r4
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif  
  
  jsr move_mem
  jmp end_insert_basic_line
  
  
remove_basic_line:
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif  
  
  orr r3,r1,r1
  load r2,basic_top
  add r1,r1,r0
  sub r2,r2,r1
  
#if DEBUG  
  push r2,sp
  push r1,sp
  push r0,sp
  jsr print_hex_number
  mov r0,r1
  jsr print_hex_number
  mov r0,r2
  jsr print_hex_number
  lit r0,#10
  jsr print_char
  pop r0,sp
  pop r1,sp
  pop r2,sp
#endif   
  jsr move_mem
  load r2,basic_top
  sub r2,r2,r3
  store r2,basic_top
  
end_insert_basic_line:  
  pop r7,sp
  pop r6,sp
  pop r5,sp
  pop r4,sp
  pop r3,sp     
  pop r2,sp
  pop r1,sp
  pop pc,sp

bas_end_line:
.7fff
reset_basic:
  push lr,sp
  push r0,sp
  push r1,sp
  load r1,bas_end_line
  load r0,basic_start
  lit lr,#0
  str r1,[r0,lr]
  lit r1,#1
  str lr,[r0,r1]
  lit r1,#2
  add r0,r0,r1
  store r0,basic_top
  pop r1,sp
  pop r0,sp
  pop pc,sp
   
//--------------------------------------------------------------------
// eval_expression:
// input:
//   r0: string to expression
// output:
//   r0 : pointer to last unprocessed char
//   r1 : value computed from expression.
//--------------------------------------------------------------------
this_string_term:
.0000
get_comma:
  push lr,sp
  push r1,sp
  jsr skip_white_space
  
  lit lr,#0
  ldr r1,[r0,lr]
  lit lr,#','
  eor r1,r1,lr
  beq done_get_comma
  jsr basic_error

done_get_comma: 
  lit lr,#1
  add r0,r0,lr
  pop r1,sp
  pop pc,sp

get_three_operand:
  push lr,sp
  jsr get_operand
  mov r3,r1
  jsr get_comma
  jsr get_operand
  mov r2,r1
  jsr get_comma
  jsr get_operand
  pop pc,sp

get_two_operand:
  push lr,sp
  jsr get_operand
  orr r2,r1,r1
  jsr get_comma
  jsr get_operand
  pop pc,sp
//-------------------------------------------------------------------
// get_operand
// input:
//   r0:string to evaluate as an expression
// output:
//   r0:pointer to next unprocessed char in string
//   r1:value of expression.
//-------------------------------------------------------------------

get_operand:
  push lr,sp
  push r2,sp 
  
  jsr skip_white_space
  lit r2,#0
  ldr r2,[r0,r2]
  beq end_get_operand
  lit lr,#10
  eor lr,lr,r2
  beq end_get_operand
  
  lit lr,'#'
  eor lr,lr,r2
  beq term_is_hexval
  
  
  lit lr,'!'
  eor lr,lr,r2
  beq term_is_memref
  lit lr,'?'
  eor lr,lr,r2
  beq term_is_memref
  lit lr,'('
  eor lr,lr,r2
  beq term_is_nested
  
  jsr get_variable_address
  orr r1,r1,r1   
  bne term_get_memref
  
  orr r2,r0,r0
  jsr w_atoi
  eor lr,r0,r2
  
  bne end_get_operand
  
  /*
  beq check_nested_operand
  
  jsr get_variable_address
  orr r1,r1,r1
  
  lit lr,#0
  ldr r1,[r1,lr]
  eor lr,r1,r1
  beq end_get_operand
  */
term_is_hexval:
  lit lr,#1
  add r0,r0,lr
  jsr w_htoi
  jsr end_get_operand
  
term_is_nested:  
  lit lr,#1
  add r0,r0,lr
  jsr eval_expression
  jsr skip_white_space
  lit r2,#0
  ldr r2,[r0,r2]
  beq end_get_operand
  
  lit lr,')'
  eor lr,lr,r2
  bne end_get_operand
  
  lit lr,#1
  add r0,r0,lr
  bne end_get_operand
  beq end_get_operand
  
term_is_memref:

  
  // memoryref
  lit lr,#1
  add r0,r0,lr
  
  orr r2,r0,r0 
  jsr get_operand
  eor r2,r2,r0
  beq end_get_operand
  
term_get_memref:
  lit lr,#0
  ldr r1,[r1,lr]
  
end_get_operand:
  pop r2,sp
  pop pc,sp
  
//-------------------------------------------------------------------
// eval_expression
// input:
//   r0:string to evaluate as an expression
// output:
//   r0:pointer to next unprocessed char in string
//   r1:value of expression.
//-------------------------------------------------------------------

eval_expression:
  push lr,sp
  push r2,sp
  push r3,sp
  push r4,sp
 
  orr r2,r0,r0
  jsr get_operand
  eor r2,r2,r0

  bne skip_default_zero_eval_expression
  
  lit r1,#0
  
skip_default_zero_eval_expression:
  orr r4,r1,r1
  jsr skip_white_space

  lit lr,#0
  ldr r3,[r0,lr]
  beq end_this_eval_expression

  lit lr,#','
  eor lr,lr,r3
  beq end_this_eval_expression  
  lit lr,#';'
  eor lr,lr,r3
  beq end_this_eval_expression
  lit lr,#39
  eor lr,lr,r3
  beq end_this_eval_expression

  lit lr,#1
  add r0,r0,lr

  lit lr,#0
  ldr r1,[r0,lr]

  eor r1,r1,r3
  bne this_eval_no_double_sign

  lit lr,#128
  orr r3,r3,lr

this_eval_no_double_sign:
  orr r2,r0,r0
  jsr get_operand
  eor r2,r2,r0
  beq end_this_eval_expression

  lit lr,#'*'
  eor lr,lr,r3
  beq eval_does_mult

  lit lr,#'/'
  eor lr,lr,r3
  beq eval_does_div
   
  lit lr,#'+'
  eor lr,lr,r3
  beq eval_does_addition
  
  lit lr,#'-'
  eor lr,lr,r3
  beq eval_does_subtraction
  
  lit lr,#'&'
  eor lr,lr,r3
  beq eval_does_andop
  
  lit lr,#'|'
  eor lr,lr,r3
  beq eval_does_orrop
  
  lit lr,#'^'
  eor lr,lr,r3
  beq eval_does_eorop
  
  lit lr,#'='
  eor lr,lr,r3
  beq eval_does_cmpop
  
  lit lr,#'>'
  eor lr,lr,r3
  beq eval_does_lgcmpop
  
  lit lr,#'<'
  eor lr,lr,r3
  beq eval_does_smcmpop

  lit lr,#190
  eor lr,lr,r3
  beq eval_does_shright
  
  lit lr,#188
  eor lr,lr,r3
  beq eval_does_shleft
  
  bne end_this_eval_expression

eval_does_mult:
  mul r15,r4,r1
  orr r0,r0,r0
  mul r1,r0,r0
  eor r2,r2,r2
  beq end_this_eval_expression
  
eval_does_div:
  mul r15,r4,r1
  orr r0,r0,r0
  orr r0,r0,r0
  mul r1,r1,r0
  eor r2,r2,r2
  beq end_this_eval_expression
  
eval_does_andop:
  and r1,r4,r1
  eor r2,r2,r2
  beq end_this_eval_expression
eval_does_orrop:
  orr r1,r4,r1
  eor r2,r2,r2
  beq end_this_eval_expression
  
eval_does_eorop:
  eor r1,r4,r1
  eor r2,r2,r2
  beq end_this_eval_expression

eval_does_shright:
  shr r1,r4,r1  
  eor r2,r2,r2
  beq end_this_eval_expression

eval_does_shleft:
  shl r1,r4,r1  
  eor r2,r2,r2
  beq end_this_eval_expression
  
eval_does_cmpop:
  eor r1,r4,r1
  beq eval_ret_true
  bne eval_ret_false
  
eval_does_lgcmpop:
  sub r1,r1,r4
  bmi eval_ret_true
  bpl eval_ret_false

  
eval_does_smcmpop:
  sub r1,r4,r1
  bmi eval_ret_true
  bpl eval_ret_false
    
eval_ret_false:
  lit r1,#0
  beq end_this_eval_expression
eval_ret_true:
  lit r1,#1
  bne end_this_eval_expression
  
eval_does_subtraction:
  sub r1,r4,r1
  eor r2,r2,r2
  beq end_this_eval_expression

eval_does_addition:  
  add r1,r1,r4
  
end_this_eval_expression:
  
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop pc,sp

//-------------------------------------------------------------------
// get_variable_address
// input:
//   r0:string to evaluate as a variable
// output:
//   r0:pointer to next unprocessed char in string
//   r1:pointer to storage of variable.
//-------------------------------------------------------------------
text_check_variable:
."check var\n"
text_is_variable:
."is var\n"
text_var_step:
."stepvar\n"

get_variable_address:
  push lr,sp
  push r2,sp
  push r3,sp
  jsr skip_white_space
  
#if DEBUG  
  push r0,sp
  adr r0,text_check_variable
  jsr print_string
  pop r0,sp
#endif
  
  lit r1,#0
  lit lr,#1
  ldr r2,[r0,r1]
  ldr r3,[r0,lr]
  
  lit lr,#'.'
  eor lr,lr,r3
  beq end_get_variable_address
  
check_variable:
  lit lr,#'Z'
  sub lr,lr,r2
  bmi end_get_variable_address
  
#if DEBUG  
  push r0,sp
  adr r0,text_var_step
  jsr print_string
  pop r0,sp  
#endif
  
  lit lr,#'@'
  sub r2,r2,lr
  bmi end_get_variable_address

  lit lr,#'['
  eor lr,lr,r3
  bne var_address_not_indexed
  
  lit lr,#1
  add r0,r0,lr
  orr r3,r0,r0
  jsr eval_expression
  eor r3,r3,r0
  jeq basic_error
  jsr skip_white_space
  lit lr,#0
  ldr r3,[r0,lr]
  lit lr,#']'
  eor lr,lr,r3
  jne basic_error
  
  lit lr,#1
  add r0,r0,lr
  // get var val
  lit lr,#4
  shl lr,lr,#8
  ldr r2,[lr,r2]
  
  // compute indexed address
  add r1,r2,r1
  bne end_get_variable_address
  jeq basic_error
  
var_address_not_indexed: 



#if DEBUG  
  push r0,sp
  adr r0,text_var_step
  jsr print_string
  pop r0,sp  
#endif
  
  lit lr,#'@'
  sub lr,lr,r3
  bpl confirmed_variable
  
#if DEBUG  
  push r0,sp
  adr r0,text_var_step
  jsr print_string
  pop r0,sp  
#endif
  
  lit lr,#'Z'
  sub lr,lr,r3
  bmi confirmed_variable
  bpl end_get_variable_address
  
confirmed_variable:
#if DEBUG  
  push r0,sp
  adr r0,text_is_variable
  jsr print_string
  pop r0,sp
#endif
  
  lit lr,#1
  add r0,r0,lr
  shl lr,lr,#10    // 0x400 is base for basic variables.
  add r1,r2,lr
#if DEBUG  
  push r0,sp
  orr r0,r1,r1
  jsr print_hex_number
  pop r0,sp
#endif  
end_get_variable_address:
  pop r3,sp
  pop r2,sp
  pop pc,sp   


//-------------------------------------------------------------------
// get_variable_val
// input:
//   r0:string to evaluate as a variable
// output:
//   r0:pointer to next unprocessed char in string
//   r1:value of variable.
//-------------------------------------------------------------------
get_variable_val:
  push lr,sp
  jsr get_variable_address
  lit lr,#0
  ldr r1,[r1,lr]
  pop pc,sp

//-------------------------------------------------------------------
// do_print
// input:
//   r0:string following PRINT command
//-------------------------------------------------------------------
text_do_print:
."doprint\n"

do_print:
  push lr,sp
  push r1,sp
  push r2,sp

#if DEBUG
  push r0,sp
  adr r0,text_do_print
  jsr print_string
  pop r0,sp
#endif

next_item_do_print:
  
  jsr skip_white_space

  lit lr,#0
  ldr lr,[r0,lr]
  beq end_do_print      // zero terminate is end.

  lit r1,#39
  eor r1,r1,lr
  beq do_print_line_feed
  
  lit r1,#10
  eor r1,r1,lr
  beq end_do_print
  
  lit r1,#';'
  eor r1,r1,lr
  beq end_do_print
  
  lit r1,#','           // comma so next item
  eor r1,r1,lr
  bne test_quotes_do_print
  
skip_char_do_print:
  lit r1,#1
  add r0,r0,r1
  bne next_item_do_print
  beq end_do_print
  
test_quotes_do_print:  
  lit r1,#x22           // begin of a string ?
  eor r1,r1,lr
  bne get_expression_do_print
  
  lit r1,#1
  add r0,r0,r1 // skip first quote char
next_quoted_do_print:  
  lit r1,#0
  ldr lr,[r0,r1]
  
  lit r1,#1
  add r0,r0,r1
  
  lit r1,#x22
  eor r1,r1,lr
  beq next_item_do_print      // end of quoted string
  
  push r0,sp
  orr r0,lr,lr
  jsr print_char
  pop r0,sp
  bne next_quoted_do_print
  beq end_do_print

get_expression_do_print:
  lit r2,'$'
  eor r2,r2,lr
  beq do_string_print
  lit r2,'&'
  eor r2,r2,lr
  bne numerical_do_print
  lit lr,#1
  add r0,r0,lr
numerical_do_print:  
  jsr eval_expression
  
  push r0,sp
  orr r0,r1,r1
  orr r2,r2,r2
  beq hex_do_print
  jsr print_number
  pop r0,sp
  jmp next_item_do_print
hex_do_print:
  jsr print_hex_number
  pop r0,sp
  jmp next_item_do_print
do_string_print:
  lit lr,#1
  add r0,r0,lr
  jsr eval_expression
  push r0,sp
  orr r0,r1,r1
  shr r1,r1,#7
  beq do_print_this_char
  jsr print_wstring
  pop r0,sp
  jmp next_item_do_print

do_print_line_feed:
  lit lr,#1
  add r0,r0,lr
  push r0,sp
  lit r0,#10
do_print_this_char:
  jsr print_char
  pop r0,sp
  jmp next_item_do_print

end_do_print:
  pop r2,sp
  pop r1,sp
  pop pc,sp



do_cmd:
  push r0,r12
  orr pc,lr,lr

do_until:
  push lr,sp
  push r1,sp
  push r2,sp
  orr r2,r0,r0
  jsr eval_expression
  eor r2,r2,r0
  bne check_finished_until
  jsr basic_error
check_finished_until:
  // get return val
  pop r2,r12
  orr r1,r1,r1
  bne end_do_until
  orr r0,r2,r2
  push r0,r12
end_do_until:
  pop r2,sp
  pop r1,sp
  pop pc,sp

//-------------------------------------------------------------------
// do_for
// input:
//   r0: string  ( <var> <start> <end> )
//-------------------------------------------------------------------

assert_char:
  push lr,sp
  jsr skip_white_space
  
  ldi lr,[r0,#0]
  jeq basic_error
  eor r1,lr,r1
  jne basic_error
  adi r0,r0,#1
  pop pc,sp

step_keyword:
.w"STEP"
do_for:
  push lr,sp
  push r1,sp
  push r2,sp

  jsr get_variable_address
  orr r1,r1,r1
  jeq basic_error
  
  push r1,r12  //var 
  orr r2,r1,r1
  
  jsr skip_white_space
  
  ldi lr,[r0,#0]
  jeq basic_error
  
  lit r1,#'='
  jsr assert_char
  
  jsr get_operand
  
  sti r1,[r2,#0]
  
  lit r1,#'T'
  jsr assert_char
  lit r1,#'O'
  jsr assert_char
  
  
  jsr get_operand
  push r1,r12
   
  adr r1,step_keyword
  jsr cmp_wstr_wstr
  orr r1,r1,r1
  beq no_step_keyword

  push r0,sp
  adr r0,step_keyword
  jsr print_wstring
  pop r0,sp
  
  jsr eval_expression

  orr r1,r1,r1
  bne set_step_forcmd
no_step_keyword:
  lit r1,#1
set_step_forcmd:  
  push r1,r12



  jsr skip_white_space
  push r0,r12 
  
  
  
end_do_for:
  
  pop r2,sp
  pop r1,sp
  pop pc,sp

//-------------------------------------------------------------------
// do_next
// input:
//   r0: string  ( <expr> THEN <cmd> )
//-------------------------------------------------------------------

do_next:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  
  
  // stack checking required here!!!!
  
  pop r1,r12 // txt
  pop r2,r12 // step
  pop r5,r12 // to
  pop r4,r12 // var address
  
  
  ldi r3,[r4,#0]
  add r3,r3,r2
  orr r2,r2,r2
  bmi check_down_next
  sub lr,r5,r3
  bmi done_next
  bpl continue_next
  
check_down_next:
  sub lr,r3,r5 
  bmi done_next

continue_next:

  sti r3,[r4,#0]

  // basic back to previous text
  orr r0,r1,r1
  
  // rewind stack
  adi r12,r12,#-4
  
done_next:
  pop r5,sp 
  pop r4,sp
  pop r3,sp  
  pop r2,sp
  pop r1,sp
  pop pc,sp
  
//-------------------------------------------------------------------
// do_if
// input:
//   r0: string  ( <expr> THEN <cmd> )
//-------------------------------------------------------------------

do_if:
  push lr,sp
  push r1,sp
  jsr eval_expression
  orr r1,r1,r1
  beq not_do_if
next_do_if:
  jsr interpret_cmd
  jsr skip_white_space
  lit lr,#0
  ldr lr,[r0,lr]
  bne next_do_if
not_do_if:
  lit lr,#0
  ldr lr,[r0,lr]
  beq end_do_if
  lit lr,#1
  add r0,r0,lr
  bne not_do_if

end_do_if:
  pop r1,sp
  pop pc,sp


// KEY

do_key:
 push lr,sp
 push r1,sp
 push r2,sp
 orr r2,r0,r0
 jsr get_variable_address
 eor r2,r2,r0
 push r0,sp
 bne store_key_in_var
 jsr basic_error
store_key_in_var:
 jsr key_board
 push r0,r1
 pop r0,sp
 pop r2,sp
 pop r1,sp
 pop pc,sp
 
//-------------------------------------------------------------------
// GOTO
//-------------------------------------------------------------------
goto_errstr:
."ERR LINE"
do_goto:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp

  jsr eval_expression
  
  orr r0,r1,r1
  orr r3,r1,r1
  jsr find_line
  
  eor lr,r3,r2  // is line requested?
  beq end_goto
  
  lit lr,#1
  add r0,r0,lr
  jsr print_wstring
  
  adr r0,goto_errstr
  jsr basic_error
end_goto:
  lit lr,#1
  add r0,r0,lr
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp


