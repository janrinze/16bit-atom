


/*    
plotLine(x0,y0, x1,y1)
  if abs(y1 - y0) < abs(x1 - x0)
    if x0 > x1
      draw_low(x1, y1, x0, y0)
    else
      draw_low(x0, y0, x1, y1)
    end if
  else
    if y0 > y1
      draw_high(x1, y1, x0, y0)
    else
      draw_high(x0, y0, x1, y1)
    end if
  end if
*/




#define x0 r2
#define y0 r1
#define x1 r4
#define y1 r3
#define tmp lr

#define dx r7
#define dy r8
#define xend r10
#define yi r9
#define D r0
#define yend r10
#define xi r9


drawline:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  push r6,sp
  push r7,sp
  push r8,sp
  push r9,sp
  push r10,sp
    
  jsr get_two_operand
  push r0,sp

  load x1,x_old
  load y1,y_old
  
  store x0,x_old
  store y0,y_old
  
  jsr raw_draw_line

  pop r0,sp
    
  pop r10,sp
  pop r9,sp
  pop r8,sp
  pop r7,sp
  pop r6,sp
  pop r5,sp
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp

raw_draw_line:
  push lr,sp   
  sub dx,x0,x1
  bpl abs_one
  sub dx,x1,x0
abs_one:

  sub dy,y1,y0
  bpl abs_two
  sub dy,y0,y1
abs_two:

  
  sub tmp,dx,dy
  bmi draw_pt2
  // dx >= dy
  sub tmp,x1,x0
  bpl draw_low
  bmi draw_low_rev

draw_pt2:
  // dy >dx
  sub tmp,y1,y0
  bpl draw_high
  bmi draw_high_rev

draw_low_rev:
  push x0,sp
  push x1,sp
  push y0,sp
  push y1,sp
  pop y0,sp
  pop y1,sp
  pop x0,sp
  pop x1,sp


/*
draw_low(x0,y0, x1,y1)
  dx = x1 - x0
  dy = y1 - y0
  yi = 1
  if dy < 0
    yi = -1
    dy = -dy
  end if
  D = 2*dy - dx
  y = y0

  for x from x0 to x1
    plot(x,y)
    if D > 0
       y = y + yi
       D = D - 2*dx
    end if
    D = D + 2*dy
*/

draw_low:
  orr xend,x1,x1
  sub dx,x0,x1
  lit yi,#1

  sub dy,y1,y0
  bpl skip_draw_low_negdy
  
  adi yi,yi,#-2
  sub dy,y0,y1
  
skip_draw_low_negdy:
  shl dy,dy,#1
  orr D,dx,dx
  shl dx,dx,#1
  adi D,D,#1
draw_low_loop:
  add D,D,dy
  jsr plot_pixel_at
  adi x0,x0,#1
  sub tmp,xend,x0
  bmi end_draw_line
  orr D,D,D
  bmi draw_low_loop
  add y0,y0,yi
  add D,D,dx
  jsr draw_low_loop


draw_high_rev:
  push x0,sp
  push x1,sp
  push y0,sp
  push y1,sp
  pop y0,sp
  pop y1,sp
  pop x0,sp
  pop x1,sp

/*    
draw_high(x0,y0, x1,y1)
  dx = x1 - x0
  dy = y1 - y0
  xi = 1
  if dx < 0
    xi = -1
    dx = -dx
  end if
  D = 2*dx - dy
  x = x0

  for y from y0 to y1
    plot(x,y)
    if D > 0
       x = x + xi
       D = D - 2*dy
    end if
    D = D + 2*dx
*/

draw_high:
  orr yend,y1,y1
  lit xi,#1
  sub dy,y0,y1
  lit tmp,#0

  sub dx,x1,x0
  bpl skip_draw_high_negdy
  
  sub xi,tmp,xi
  sub dx,tmp,dx

skip_draw_high_negdy:
  shl dx,dx,#1
  orr D,dy,dy
  shl dy,dy,#1
  adi D,D,#-1
draw_high_loop:
  add D,D,dx
  jsr plot_pixel_at
  adi y0,y0,#1
  sub tmp,yend,y0
  bmi end_draw_line
  orr D,D,D
  bmi draw_high_loop
  add x0,x0,xi
  add D,D,dy
  jsr draw_high_loop

  
  
end_draw_line:    
  pop pc,sp
  

//----------------------------------------------------------------------
// do clear <int>
//----------------------------------------------------------------------

gr_mods:
.0000
.0000
.0000
.0000
.000f
first_pop:
.97ff
do_clear:
  push lr,sp
  push r1,sp
  push r2,sp
  jsr get_operand
  orr r1,r1,r1
  bmi end_do_color
  lit lr,#4
  sub lr,lr,r1
  bmi end_do_color
  adr lr,gr_mods
  ldr lr,[lr,r1]
  store lr,&bd00
  load r1,first_pop
  lit r2,#0
  
clear_loop:
  push r2,r1
  push r2,r1
  push r2,r1
  push r2,r1
  push r2,r1
  push r2,r1
  push r2,r1
  push r2,r1
  shr lr,r1,#15
  bne clear_loop
  pop r2,sp
  pop r1,sp
  pop pc,sp
x_old:
.0000
y_old:
.0000
color_val:
.0000
pixel_lookup:
.c000
.3000
.0c00
.0300
.00c0
.0030
.000c
.0003
pixel_mask_lookup:
.3fff
.cfff
.f3ff
.fcff
.ff3f
.ffcf
.fff3
.fffc
//----------------------------------------------------------------------
// do_color <int>
//----------------------------------------------------------------------
do_color:
  push lr,sp
  push r1,sp
  
  jsr get_operand
  lit lr,#3
  and r1,lr,r1
  orr lr,r1,r1
  shl r1,r1,#2
  orr lr,lr,r1
  shl r1,lr,#4
  orr lr,lr,r1
  shl r1,lr,#8
  orr r1,lr,r1 
  adr lr,color_val
  push r1,lr
end_do_color:
  pop r1,sp
  pop pc,sp


//----------------------------------------------------------------------
// pixel_at  r1:y r2:x trashes:r3-r6
//----------------------------------------------------------------------

plot_pixel_at:
  push lr,sp
  orr lr,r1,r2
  shr lr,lr,#8
  bne end_plot_pixel_at
  
  lit lr,#191
  sub lr,lr,r1
  bmi end_plot_pixel_at
  
  shr r4,r2,#3
  
  lit lr,#x80
  shl lr,lr,#8
  add r4,r4,lr

  shl r5,r1,#5
  
  lit lr,#7
  and r6,r2,lr
  adr lr,pixel_mask_lookup
  ldr r6,[lr,r6]
  load lr,color_val
  
  ldr r3,[r4,r5]
  and r3,r3,r6
  xor r6,r6,&ffff
  and lr,lr,r6
  orr r3,r3,lr
  str r3,[r4,r5]
  
end_plot_pixel_at:
  pop pc,sp
  
//----------------------------------------------------------------------
// do_gr_plot
//----------------------------------------------------------------------  
do_gr_plot:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  push r6,sp

  // get x,y in r2,r1
  jsr get_two_operand
  store r2,x_old
  store r1,y_old

  jsr plot_pixel_at

end_do_gr_plot:
  pop r6,sp
  pop r5,sp
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp
//----------------------------------------------------------------------
// do_gr_move
//----------------------------------------------------------------------  
do_gr_move:
  push lr,sp
  push r1,sp
  push r2,sp
  push r3,sp
  push r4,sp
  push r5,sp
  push r6,sp
  // get x,y in r2,r1
  jsr get_two_operand
  store r2,x_old
  store r1,y_old
  pop r6,sp
  pop r5,sp
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop r1,sp
  pop pc,sp

    
#undef x0
#undef y0
#undef x1
#undef y1
#undef tmp

#undef dx
#undef dy
#undef xend
#undef yi
#undef D
#undef yend
#undef xi
  
