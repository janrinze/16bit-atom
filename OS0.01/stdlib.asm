w_htoi:
  push lr,sp
  push r2,sp
  push r3,sp
  push r4,sp
 
  jsr skip_white_space
  
  lit r1,#0
  lit r4,#4
  // up to 4 chars 0..9,a..f

next_char_w_htoi:
  // get next char
  lit r3,#0
  ldr r3,[r0,r3]
  beq end_w_htoi
  
  lit r2,#':'
  sub r2,r3,r2
  bpl is_aplha_w_htoi
  
  lit lr,'0'
  sub r2,r3,lr
  bmi end_w_htoi
  bpl add_digit_w_htoi

is_aplha_w_htoi:  
  lit r2,#'G'
  sub r2,r3,r2
  bpl end_w_htoi
  lit r2,#'A'
  sub r2,r3,r2
  bmi end_w_htoi
  lit lr,#10
  add r2,r2,lr

add_digit_w_htoi:
  shl r1,r1,#4
  add r1,r1,r2
  
  lit r2,#1
  add r0,r0,r2
  
  sub r4,r4,r2
  
  bne next_char_w_htoi
  
end_w_htoi:
  pop r4,sp
  pop r3,sp
  pop r2,sp
  pop pc,sp
  
  
//
// w_atoi:
// input:
//   r0: wstring pointer
// output:
//   r0: wstring pointer to char after int
//   r1 value of int

w_atoi:
  push lr,sp
  push r2,sp
  push r3,sp

  jsr skip_white_space
  
  lit r3,#1
  lit r1,#0
  lit lr,#0
  ldr lr,[r0,lr]
  beq done_w_atoi
  
  lit r3,#x23
  eor r3,r3,lr
  bne w_atoi_check_minus
  lit lr,#1
  add r0,r0,lr
  jsr w_htoi
  jmp end_w_atoi
  
w_atoi_check_minus:  
  lit r3,'-'
  eor r3,r3,lr
  bne next_digit_w_atoi
  
  lit lr,#1
  add r0,r0,lr
  
next_digit_w_atoi:

  lit lr,#0
  ldr lr,[r0,lr]
  beq done_w_atoi
  lit r2,#x30
  sub lr,lr,r2
  bmi done_w_atoi
  lit r2,#9
  sub r2,r2,lr
  bmi done_w_atoi
  
  shl r2,r1,#3    // r3 = sum*8
  shl r1,r1,#1    // sum = sum *2
  add r1,r1,r2    // sum = sum * 10
  add r1,r1,lr
  
  lit r2,#1
  add r0,r0,r2
  bne next_digit_w_atoi

done_w_atoi:
  orr r3,r3,r3
  bne end_w_atoi
  sub r1,r3,r1
end_w_atoi:  
  pop r3,sp
  pop r2,sp
  pop pc,sp
