// boot code
//  for demo

@f000
program_store:
basic_start:
.2900
basic_top:
program_top:
.2902  

command_line_buffer:
.1000

my_prompt:
.w">"

prompt:
.my_prompt

cold_boot:
.0000

my_screen:
.0000

my_print_bla:
.w"HELLO WORLD"

end_screen:
.97ff
ref_bla:
.my_print_bla

my_pchar:
  push lr,sp
  push r0,sp
  load r1,my_screen
  
  lit lr,#31
  and r0,r0,lr
  store r0,&8000,r1
  adi r1,r1,#1
  store r1,my_screen
  pop r0,sp
  pop pc,sp
  


boot:
  lit r1,#4
  store r1,&8001
  orr r1,r1,#2
  store r1,&8002
  
  load sp,stack

  load r2,ref_bla
  
load r2,ref_bla
t_loop:
  //bne boot_loop
  //beq boot_loop
  ldi r0,[r2,#0]
  orr r0,r0,r0
  beq end_t_loop
  jsr my_pchar
  adi r2,r2,#1
  bne t_loop
end_t_loop:
  //jsr end_t_loop
  //jsr end_t_loop
boot_loop:
  //lit r1,#1
  //store r1,&8000
  load r0,cold_boot
  orr r0,r0,r0
  bne skip_reset_basic
  
  jsr reset_basic
    
skip_reset_basic:
    lit lr,#1

      
    store lr,cold_boot
    lit r0,boot_string
  orr r2,r0,r0
  

    lit r0,boot_string
    jsr print_string


restart_command_line_input:
    load sp,stack
    lit r2,#1 // default step for buffer
    lit r3,#0 // cursor flash counter
    lit r1,#0 // buffer index
    
    lit r4,#0 // always 0
    lit r5,#0 // line counter
    
    load r10,command_line_buffer // null terminate new buffer.
    str r4,[r10,r1]

    load r0,prompt
    jsr print_wstring

main_loop:

    add r3,r3,r2
    shl r0,r3,#5
    shr r0,r0,#15
    
    jsr set_cursor
    
    jsr key_board

    orr r6,r0,r0
    beq main_loop
    
    lit r0,#0
    jsr set_cursor

    orr r0,r6,r6

    
    lit lr,#10 // return?
    eor lr,lr,r0    
    beq end_main_loop
    
    lit lr,#8 // backspace?
    eor lr,lr,r0
    beq undo_char
    jsr print_char    
    str r0,[r10,r1]  // store char in buffer.
    lit r2,#1 // default step for buffer
    add r1,r1,r2     // advance buffer.
add_zero_termination:
    lit lr,#0
    str lr,[r10,r1]  // zero terminate
    beq  main_loop
    
undo_char:
    orr r1,r1,r1      // only if chars in buffer 
    beq main_loop
    lit r0,#8
    jsr print_char
    lit r0,#32
    jsr print_char
    lit r0,#8
    jsr print_char
    sub r1,r1,r2
    bpl add_zero_termination

end_main_loop:
    jsr print_char
    //load r0,command_line_buffer
    //jsr print_wstring
    load r0,command_line_buffer
    jsr parse_command_line
    jsr restart_command_line_input

stack:
.7fff
boot_string:
."\lACORN ATOM\nOS 0.01\n16 BIT RISC\n"

reply_text:
."OK: "


parse_command_line:
  push lr,sp
  push r0,sp
  push r1,sp
  push r2,sp
  
  jsr skip_white_space
  
  orr r2,r0,r0
  jsr w_atoi
  eor r2,r2,r0
  bne store_command_line_as_program
  
  jsr interpret_cmd
  

  
  jmp end_parse_command_line
store_command_line_as_program:

  jsr insert_basic_line
  
end_parse_command_line:
  pop r2,sp
  pop r1,sp
  pop r0,sp
  pop pc,sp
#include "atom_io.asm"
#include "lib.asm"
@ffff
.boot

