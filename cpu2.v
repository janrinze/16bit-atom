/*
    16bit RISC cpu
    (c) 2018 Jan Rinze Peterzon.
*/

`define  BRA   4'h0
`define  JSR   4'h1
`define  LDR   4'h2
`define  STR   4'h3
`define  LDI   4'h4
`define  STI   4'h5
`define  ADI   4'h6
`define  LIT   4'h7

`define  ADD   4'h8
`define  SUB   4'h9
`define  AND   4'ha
`define  ORR   4'hb
`define  EOR   4'hc
`define  SHL   4'hd
`define  SHR   4'he
`define  MUL   4'hf

`define LOADRD 2'b00
`define WRITE  2'b01
`define PSTALL 2'b10
`define DECODE 2'b11

`define PC 4'hf

`define  opLIT   constant
`define  opADI   (arg2==4'h0)?{15'h0000,(Ra==0)}:(Ra + sim7)

`define  opADD   Ra + Rb
`define  opSUB   Ra - Rb
`define  opSHL   Ra << Rb[3:0]
`define  opSHR   Ra >> Rb[3:0]
//`define  opSHL   (Rb[3]? {Ra[7:0]<< Rb[2:0],8'h00} :(Ra << Rb[2:0]))
//`define  opSHR   (Rb[3]? {8'h00,Ra[15:8]>> Rb[2:0]} :(Ra >> Rb[2:0]))
`define  opAND   Ra & Rb
`define  opORR   Ra | Rb
`define  opXOR   Ra ^ Rb
`define  opMUL   Ra * Rb

`define  DopADD   RiA + RiB
`define  DopSUB   RiA - RiB
`define  DopSHL   RiA << RiB[3:0]
`define  DopSHR   RiA >> RiB[3:0]
//`define  DopSHL   (RiB[3]? {RiA[7:0]<< RiB[2:0],8'h00} :(RiA << RiB[2:0]))
//`define  DopSHR   (RiB[3]? {8'h00,RiA[15:8]>> RiB[2:0]} :(RiA >> RiB[2:0]))
`define  DopAND   RiA & RiB
`define  DopORR   RiA | RiB
`define  DopXOR   RiA ^ RiB
`define  DopMUL   RiA * RiB



module cpu (
    input clk,                // clock
    input rst,                // reset
    input delay,              // Hold off cpu from bus
    output reg write,         // CPU write request
    output reg [15:0]address, // read/write address
    output reg [15:0]dout,    // write data
    input [15:0]din           // read data
  );

  reg [15:0] result = 16'h0000;
  reg [15:0] register[0:15];
  reg [3:0]  read_dest=0;
  reg [1:0]  cpu_state = 0;
  reg [15:0] inst =0;

  // clock instr

  
  reg [15:0] nextpc=0;
  //reg [15:0] dnextpc=0;
  `define to_pc (dest==`PC)
  
 
  
  wire [3:0] op = inst[15:12];
  wire [3:0] dest = inst[11:8];
  wire [3:0] arg1 = inst[7:4];
  wire [3:0] arg2 = inst[3:0];
  
  
  
  wire imm_alu = inst[15] & inst[0]& inst[1]& inst[2]& inst[3];
  
  reg [15:0] RiB=0;
  reg [15:0] RiA=0;
  reg [3:0] d_op=0;
  
  reg delayed_alu;
  
  reg [15:0] Ra;
  reg [15:0] Rb;
  
  //wire [15:0]Ra = register[inst[7:4]];
  //wire [15:0]Rb = register[inst[3:0]];
  always@* begin
    Ra <= register[inst[7:4]];
	Rb <= register[inst[3:0]];
  end
  //wire [15:0] RB = delayed_alu ? RiB:Rb;
  wire [15:0] constant = { 8'h00,inst[7:0]};
  //wire [15:0] branch_target = register[`PC] + {{7{inst[8]}},inst[8:0]};
  //wire [15:0] jump_target = register[`PC] + {{5{inst[10]}},inst[10:0]};
  wire [15:0] sim7 =  {{12{inst[3]}},inst[3:0]};
  
  // prepare for single branch instruction.
  wire branchbita = inst[10];
  wire branchbitb = inst[9];

  reg branch_taken=0;
  reg branch_imm=0;

  reg [15:0] branch_target;
   
  always@(negedge clk)
  begin
    nextpc <= register[`PC] + 16'h0001;
    branch_target <= register[`PC] + {{7{inst[8]}},inst[8:0]};
    //dnextpc <= register[`PC] + 16'h0002;
    branch_taken <= branchbita ?  ((result==16'h0000) ? ~branchbitb : branchbitb) : (result[15]? branchbitb:~branchbitb);
    branch_imm <= inst[11] ? (inst[8:0]==9'h00) : 0;
  end

  //always @*
  //  branch_taken = branchbita ?  ((result==16'h0000) ? ~branchbitb : branchbitb) : (result[15]? branchbitb:~branchbitb);

  wire [15:0] braddr = branch_taken ? (branch_imm ? din : branch_target) : nextpc;
  wire [1:0]  br_cpu_state = (branch_taken) ? ((inst[11]&&(inst[8:0]!=9'h00))?`LOADRD:`PSTALL):(inst[11]?`PSTALL:`DECODE);

  //wire [15:0] ldraddr = (arg2==`PC) ? ((arg1==`PC)? din : Ra + 1):Ra + Rb;
  
  reg [15:0] ALU=0;
  always@* begin
    case (op)
      `LIT: ALU = `opLIT ;
      `ADD: ALU = `opADD ;
      `SUB: ALU = `opSUB ;
      `SHL: ALU = `opSHL ;
      `SHR: ALU = `opSHR ;
      `AND: ALU = `opAND ;
      `ORR: ALU = `opORR ;
      `EOR: ALU = `opXOR ;
      `ADI: ALU = `opADI ;
      //`MUL: ALU = `opMUL ;
      default: ALU = 16'h0000;
    endcase
  end

reg [15:0] delayedALU=0;
  always@* begin
    case (d_op)
      `ADD: delayedALU = `DopADD ;
      `SUB: delayedALU = `DopSUB ;
      `SHL: delayedALU = `DopSHL ;
      `SHR: delayedALU = `DopSHR ;
      `AND: delayedALU = `DopAND ;
      `ORR: delayedALU = `DopORR ;
      `EOR: delayedALU = `DopXOR ;
      //`MUL: delayedALU = `DopMUL ;
      default: delayedALU = 16'h0000;
    endcase
  end

  
  //reg [15:0] co_pro_div;
  //reg [15:0] co_pro_rem;
  reg [15:0] co_pro_A;
  reg [15:0] co_pro_B;

  wire [31:0] co_pro_mul = co_pro_A * co_pro_B;
  wire [15:0] co_pro_div = co_pro_A / co_pro_B;
  
  reg imm_read;

  always@(posedge clk) begin
    if (rst) begin
      // jump to boot vector.
      cpu_state <= `LOADRD;
      address   <= 16'hffff;
      write     <= 0;
      read_dest <= `PC;
      // sane defaults.
      result <= 16'h0000;
      imm_read<=0;
      delayed_alu<=0;
      RiA <=16'h0000;
      RiB <=16'h0000;
      dout <=16'h0000;
      d_op <=4'h0;
      inst <=16'h0000;
    end
    else if (!delay) // bus busy
    begin
      
      // pipeline next instruction.
      if (cpu_state[1] && !(imm_alu&cpu_state[0])) begin
            inst <= din;

		end
      
      case(cpu_state)
      `DECODE: begin
                case (op)
                  `BRA: //if (branch_taken)
                          begin
                            read_dest <= `PC;
                            address   <= braddr;
                            register[`PC] <=braddr;
                            cpu_state <= br_cpu_state;
                            /*
                            // special case where the indirect branch address is our next instruction
                            if (branch_imm)
                             begin
                                register[`PC] <= din;
                                address <= din;
                                cpu_state <= `PSTALL;
                              end
                            else
                              begin
                                register[`PC] <= branch_target;
                                address <= branch_target;
                                if (inst[11]) // indirect target
                                  cpu_state <= `LOADRD;
                                else
                                  cpu_state <= `PSTALL;
                              end
                          end
                        else
                          begin
                            register[`PC] <= nextpc;
                            address <= nextpc;
                            if (inst[11]) cpu_state <= `PSTALL; // skip next word if intended as branch target
                          end*/
                        end
                  `LDR: begin
                            // ldr pc,pc,pc is JMP [IMM16]
                            // ldr rd,pc,pc is LDR rd[IMM16]
                            // ldr rd,rs,pc is POP rd,rs
                            
                            register[`PC] <= nextpc;
                            read_dest <= dest;
                            cpu_state <= `LOADRD;
                            /*
                            address <= ldraddr;
                            if (arg2==`PC)
                              begin
                                if (arg1==`PC)
                                    imm_read<=1;
                                else
                                    register[arg1] <= ldraddr; // stack operation 'POP' ascending.
                              end
                            */
                            if (arg2==`PC)
                              begin
                                if (arg1==`PC)
                                  begin
                                    address<=din;
                                    imm_read<=1;
                                  end
                                else
                                  begin
                                    register[arg1] <= Ra + 1; // stack operation 'POP' ascending.
                                    address <= Ra + 1;
                                  end
                              end
                            else
                              begin                              
                                address <= Ra + Rb;
                              end
                         end
                  `STR: begin
                            // str rd,pc,pc is str rd,[IMM16]
                            // str rd,rs,pc is PUSH rd,rs
                            register[`PC] <= nextpc;
                            cpu_state <= `WRITE;
                            write <=1;
                            if (arg2==`PC)
                              begin
                                if (arg1==`PC)
                                  begin // STR Rd,imm16
                                    address <= din;
                                    imm_read<=1;
                                  end
                                else
                                  begin // PUSH Rd,Ra
                                    address <= Ra;
                                    register[arg1] <= Ra-1;
                                  end
                              end
                            else // STR Rd,[Ra,Rb]
                              address <= Ra + Rb;
                            
                            if (dest==`PC)
                              dout <= nextpc;
                            else
                              dout <= register[dest];
                         end
                  `LDI: begin
                            // ldr pc,pc,pc is JMP IMM16
                            // ldr rd,pc,pc is LDR rd[IMM16]
                            // ldr rd,rs,pc is POP rd,rs
                            
                            register[`PC] <= nextpc;
                            read_dest <= dest;
                            cpu_state <= `LOADRD;
                            if (arg1==`PC)
                              begin
                                if (arg2==`PC)
                                  begin
                                    register[dest]<=din ;
                                    result <= din;
                                    cpu_state <= `PSTALL;
                                    if (dest==`PC)
                                      address <= din ;
                                    else
                                      address <= nextpc;
                                  end
                                else
                                  begin
                                    imm_read<=1;
                                    address <= din + Rb;
                                  end
                              end
                            else
                              begin                              
                                address <= Ra + sim7;
                              end
                         end
                  `STI: begin
                            // str rd,pc,pc is str rd,[IMM16]
                            // str rd,rs,pc is PUSH rd,rs
                            register[`PC] <= nextpc;
                            cpu_state <= `WRITE;
                            write <=1;
                            if (arg1==`PC)
                              begin
                                imm_read<=1;
                                if (arg2==`PC)
                                  begin // STR Rd,imm16
                                    address <= din;
                                  end
                                else
                                  begin // STR Rd,[imm16,Rb]
                                    address <= din + Rb;
                                  end
                              end
                            else // STR Rd,[Ra,Rb]
                              address <= Ra + sim7;
                            
                            if (dest==`PC)
                              dout <= nextpc;
                            else
                              dout <= register[dest];
                         end
                         
                  `JSR: begin
                            read_dest <= `PC;
                            // special case where the indirect address is our next instruction
                            if (inst[11:0]==12'h800)
                             begin
                                register[`PC] <= din;
                                address <= din;
                                cpu_state <= `PSTALL;
                                register[14]<=nextpc; // skip one because that contains our target address
                                //if (inst[11]) // indirect target
                                //  cpu_state <= `LOADRD;
                                //else
                                //  cpu_state <= `PSTALL;
                              end
                            else
                              begin
                                register[14]<=register[`PC];
                                register[`PC] <=  branch_target;
                                address <=  branch_target;
                                if (inst[11]) // indirect target
                                  cpu_state <= `LOADRD;
                                else
                                  cpu_state <= `PSTALL;
                              end
                         end
                  `MUL:  begin
                           register[`PC] <= nextpc;
                           address <= nextpc;
                           if(dest[3:0]==`PC) begin
                              co_pro_A <= register[arg1];
                              co_pro_B <= (imm_alu) ? din :register[arg2];
                              cpu_state <= (imm_alu) ? `PSTALL:`DECODE;
                           end else begin
                              register[dest]<=arg1[0] ? co_pro_div:co_pro_mul[15:0];
                           end
                         end
                  default: begin
                              if (imm_alu)
                                begin
                                  register[`PC] <= nextpc;
                                  address <= nextpc;
                                  delayed_alu<=1;
                                  RiB <= din;
                                  RiA <= Ra;
                                  d_op <= op;
                                  read_dest<=dest;
                                  cpu_state<=`PSTALL;
                                end
                              else
                               begin
                                 if (`to_pc)
                                   begin
                                    address <= ALU;
                                    register[`PC] <= ALU;
                                    cpu_state<=`PSTALL;
                                   end
                                 else
                                   begin
                                     register[dest] <= ALU;
                                     result <= ALU;
                                     register[`PC] <= nextpc;
                                     address <= nextpc;
                                   end
                               end  
                           end
                endcase
               end  
      `LOADRD: begin
                  imm_read<=0;
                  if (read_dest==`PC) 
                    begin
                      register[`PC]<= din;
                      address <= din;
                      cpu_state<=`PSTALL; // stall for pipeline
                    end
                  else
                    begin
                      address <= register[`PC];
                      result <= din;
                      register[read_dest] <= din;
                      cpu_state <= imm_read ?`PSTALL:`DECODE;
                    end
                end
      `WRITE:  begin
                  address <= register[`PC];
                  write<=0;
                  cpu_state<=imm_read ?`PSTALL:`DECODE;
                  imm_read<=0;
               end
      `PSTALL:  begin
                  // pipeline stall for new PC.
                  delayed_alu<=0;
                  write<=0;
                  if (delayed_alu)
                    begin
                      if (read_dest==`PC)
                        begin
                          cpu_state<=`PSTALL;
                          address <= delayedALU;
                          register[`PC]<=delayedALU;
                        end
                      else
                        begin
                         register[read_dest] <= delayedALU;
                         result <= delayedALU;
                         register[`PC]<=nextpc;
                         address <= nextpc;
                         cpu_state<=`DECODE;
                        end
                    end
                  else
                    begin
                      cpu_state<=`DECODE;
                      register[`PC]<=nextpc;
                      address <= nextpc;
                    end
               end
      endcase
    end
  end
endmodule
